# [1.11.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.10.0...develop-1.11.0) (2022-10-18)


### Features

* Add LICENSE file ([8c971ae](https://gitlab.com/xcapit/backend/on-off-ramps/commit/8c971aee5b72f37ae72ce4e26c1240f47ac9f5d6))
* Add README and update config examples ([61d3b65](https://gitlab.com/xcapit/backend/on-off-ramps/commit/61d3b65afdb5bef32b7355eda3aa4aae2081be31))

# [1.10.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.9.0...develop-1.10.0) (2022-10-14)


### Bug Fixes

* **kripton:** Get users only by email to avoid duplications ([f4b2715](https://gitlab.com/xcapit/backend/on-off-ramps/commit/f4b271589a4e0684f238c805c07393e661d36f98))


### Features

* **kripton:** Create kripton app and move users into it ([ead1244](https://gitlab.com/xcapit/backend/on-off-ramps/commit/ead12442034f13d839a190650b11dcef07e84962))

# [1.9.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.8.0...develop-1.9.0) (2022-10-11)


### Bug Fixes

* Uncomment sentry and change variable name ([dd86ccc](https://gitlab.com/xcapit/backend/on-off-ramps/commit/dd86ccc1aef71e02aa9139421f0fa4c796166931))


### Features

* **kripton:** Update registration status ([dd172da](https://gitlab.com/xcapit/backend/on-off-ramps/commit/dd172da80207a68ba59d364bb780e00a7c664bbe))

# [1.8.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.7.1...develop-1.8.0) (2022-10-11)


### Features

* **directa24:** Add network to json body on directa24 link ([93ad930](https://gitlab.com/xcapit/backend/on-off-ramps/commit/93ad930b3895711eedd3eeb4a935769aa2db6231))

# [1.6.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/v1.5.0...v1.6.0) (2022-09-22)


### Bug Fixes

* **core:** Remove unused method in Environment ([3339e79](https://gitlab.com/xcapit/backend/on-off-ramps/commit/3339e791e262f11f46552ac638b7543b26265742))
* **directa24/core:** Refactor objets based on MR feedback ([82370f5](https://gitlab.com/xcapit/backend/on-off-ramps/commit/82370f59a27e0ef45b6c484b6f895351f9fef114))
* **directa24:** Add environment in deposit request body constructor ([40ec03d](https://gitlab.com/xcapit/backend/on-off-ramps/commit/40ec03d29c2c55913dbeab3797757ae5b8b6f6fd))
* **kripton:** Fix user creation when user doesnt exist on provider ([9613ace](https://gitlab.com/xcapit/backend/on-off-ramps/commit/9613ace03bab70cf3fb5aaa03aae35765c3a6ba0))


### Features

* Create with registration status complete when kyc is approved ([6eea8bf](https://gitlab.com/xcapit/backend/on-off-ramps/commit/6eea8bfb38b657b3817b580a5b4dabcce79802c9))
* **directa24:** Add deposit link endpoint ([f791830](https://gitlab.com/xcapit/backend/on-off-ramps/commit/f791830d122516763310dc8ac18d1b55be537c36))
* **kripton:** Merge check user and create user into get or create user ([6449126](https://gitlab.com/xcapit/backend/on-off-ramps/commit/6449126a759e6fafdc1fc152cc1020f93ea2d600))

## [1.7.1](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.7.0...develop-1.7.1) (2022-09-19)


### Bug Fixes

* **kripton:** Fix user creation when user doesnt exist on provider ([9613ace](https://gitlab.com/xcapit/backend/on-off-ramps/commit/9613ace03bab70cf3fb5aaa03aae35765c3a6ba0))

# [1.7.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.6.0...develop-1.7.0) (2022-09-16)


### Features

* Create with registration status complete when kyc is approved ([6eea8bf](https://gitlab.com/xcapit/backend/on-off-ramps/commit/6eea8bfb38b657b3817b580a5b4dabcce79802c9))
* **kripton:** Merge check user and create user into get or create user ([6449126](https://gitlab.com/xcapit/backend/on-off-ramps/commit/6449126a759e6fafdc1fc152cc1020f93ea2d600))

# [1.6.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.5.0...develop-1.6.0) (2022-09-06)


### Bug Fixes

* **core:** Remove unused method in Environment ([3339e79](https://gitlab.com/xcapit/backend/on-off-ramps/commit/3339e791e262f11f46552ac638b7543b26265742))
* **directa24/core:** Refactor objets based on MR feedback ([82370f5](https://gitlab.com/xcapit/backend/on-off-ramps/commit/82370f59a27e0ef45b6c484b6f895351f9fef114))
* **directa24:** Add environment in deposit request body constructor ([40ec03d](https://gitlab.com/xcapit/backend/on-off-ramps/commit/40ec03d29c2c55913dbeab3797757ae5b8b6f6fd))


### Features

* **directa24:** Add deposit link endpoint ([f791830](https://gitlab.com/xcapit/backend/on-off-ramps/commit/f791830d122516763310dc8ac18d1b55be537c36))

# [1.5.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.4.0...develop-1.5.0) (2022-08-02)


### Bug Fixes

* Add mock to crypto exchange rate view test and remove wip ([1b7156e](https://gitlab.com/xcapit/backend/on-off-ramps/commit/1b7156eef78185cbda4be84edb71150edcbb2038))


### Features

* **directa24:** Add Directa24 app and crypto_exchange_rates endpoint ([4672f8f](https://gitlab.com/xcapit/backend/on-off-ramps/commit/4672f8f62bed4f5e064776f09e8beb543b2ccf17))

# [1.4.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.3.0...develop-1.4.0) (2022-07-13)


### Features

* **kripton:** TIC-266-344 Receive telephonee and send physical address ([2130ab5](https://gitlab.com/xcapit/backend/on-off-ramps/commit/2130ab54c0bf9aeeadd361203343fe11049d131a))

# [1.3.0](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.2.2...develop-1.3.0) (2022-05-30)


### Features

* SX-2048 - Add wallet address on get user operation response ([8d2afbc](https://gitlab.com/xcapit/backend/on-off-ramps/commit/8d2afbc0de81d0304644f03e872697309d6a37db))

## [1.2.2](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.2.1...develop-1.2.2) (2022-03-22)


### Bug Fixes

* **kripton:** TIC-225 - Update networks enum ([7988cd3](https://gitlab.com/xcapit/backend/on-off-ramps/commit/7988cd38a5a264486d85b815dc90fd93ede64da4))

## [1.2.1](https://gitlab.com/xcapit/backend/on-off-ramps/compare/develop-1.2.0...develop-1.2.1) (2022-01-07)


### Bug Fixes

* Calculate and encode signature properly ([9d86725](https://gitlab.com/xcapit/backend/on-off-ramps/commit/9d867257202d47087266460358e879d79871f26d))
