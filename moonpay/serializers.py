from rest_framework import serializers


class GetMoonpayLinkSerializer(serializers.Serializer):
    publishable_key = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, required=True)
    wallet_address = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, required=True)
    currency_code = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, required=True)
    base_currency_code = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, required=True)
    base_currency_amount = serializers.FloatField(allow_null=False, required=True)