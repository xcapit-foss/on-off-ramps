import hmac
from hashlib import sha256
from urllib.parse import urlencode
from on_off_ramps.settings import MOONPAY_URL, MOONPAY_API_SECRET
import base64


class MoonpayLink:
    def __init__(self, secret_key: str, publishable_key: str, currency_code: str, wallet_address: str,
                 base_currency_code: str, base_currency_amount: float):
        self._pk = publishable_key
        self._currency_code = currency_code
        self._wallet_address = wallet_address
        self._secret_key = secret_key
        self._base_currency_code = base_currency_code
        self._base_currency_amount = base_currency_amount

    @classmethod
    def create(cls, publishable_key: str, currency_code: str, wallet_address: str, base_currency_code: str,
               base_currency_amount: float):
        return cls(MOONPAY_API_SECRET, publishable_key, currency_code, wallet_address, base_currency_code,
                   base_currency_amount)

    def value(self):
        payload = self._payload()
        signature = self._signature(payload)
        return f'{MOONPAY_URL}{payload}{signature}'

    def _payload(self):
        result = urlencode([('apiKey', self._pk),
                            ('currencyCode', self._currency_code),
                            ('walletAddress', self._wallet_address),
                            ('baseCurrencyCode', self._base_currency_code),
                            ('baseCurrencyAmount', self._base_currency_amount)])
        return f"?{result}"

    def _signature(self, payload):
        result = urlencode([('signature', self._base64(self._hmac(payload).digest()))])
        return f"&{result}"

    def _hmac(self, payload):
        return hmac.new(self._secret_key.encode(), payload.encode(), sha256)

    @staticmethod
    def _base64(value):
        return base64.b64encode(value)
