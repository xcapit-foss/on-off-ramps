from django.urls import path
from moonpay.views import GetMoonpayLinkAPIView

app_name = "moonpay"

urlpatterns = [
    path('link', GetMoonpayLinkAPIView.as_view(), name='moonpay-link'),
]
