from django.apps import AppConfig


class MoonpayConfig(AppConfig):
    name = 'moonpay'
