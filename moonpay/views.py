from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from moonpay.serializers import GetMoonpayLinkSerializer
from moonpay.moonpay_link import MoonpayLink


class GetMoonpayLinkAPIView(APIView):
    serializer_class = GetMoonpayLinkSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            publishable_key, wallet_address, currency_code, base_currency_code, base_currency_amount = serializer.data.values()
            link = {"url": MoonpayLink.create(publishable_key, currency_code, wallet_address, base_currency_code,
                                              base_currency_amount).value()}
            response = Response(link, status=status.HTTP_200_OK)
        return response
