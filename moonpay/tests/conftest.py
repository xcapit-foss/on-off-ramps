import pytest


@pytest.fixture
def expected_link():
    return (
        'https://buy-staging.moonpay.com/'
        '?apiKey=pk_test_some_code_of_publishable_key'
        '&currencyCode=eth'
        '&walletAddress=0xwalletAdress00000'
        '&baseCurrencyCode=usd'
        '&baseCurrencyAmount=10.5'
        '&signature=OO7LLvScFXe8Pc%2FLYvWA8Pux%2F7P6u0KlANaxbcpepds%3D'
    )
