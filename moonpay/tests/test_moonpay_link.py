import pytest
from unittest.mock import Mock
from moonpay.moonpay_link import MoonpayLink


def test_moonpay_link():
    assert MoonpayLink(Mock(), Mock(), Mock(), Mock(), Mock(), Mock())


def test_moonpay_link_create():
    assert MoonpayLink.create(Mock(), Mock(), Mock(), Mock(), Mock())


def test_moonpay_link_value(expected_link):
    assert MoonpayLink('test_secret_key',
                       'pk_test_some_code_of_publishable_key',
                       'eth',
                       '0xwalletAdress00000',
                       'usd',
                       10.5).value() == expected_link
