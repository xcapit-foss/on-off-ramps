import pytest
from unittest.mock import patch
from django.urls import reverse
from rest_framework import status


@patch('moonpay.moonpay_link.MoonpayLink.value')
def test_moonpay_link_view_200(moonpay_value_mock, client, expected_link):
    data = {
        "wallet_address": '0xwalletAdress00000',
        "currency_code": 'eth',
        "publishable_key": 'pk_test_some_code_of_publishable_key',
        "base_currency_code": 'usd',
        "base_currency_amount": 10.50
    }
    moonpay_value_mock.return_value = expected_link

    response = client.post(reverse('moonpay:moonpay-link'), data=data, content_type='application/json')
    assert response.status_code == 200
    assert response.json() == {'url': expected_link}


@pytest.mark.parametrize('wallet_address, currency_code, publishable_key, base_currency_code, base_currency_amount, '
                         'expected_status, expected_response', [
    ['', 'eth', 'pk_some_publishable_key', 'aus', 55.31, status.HTTP_400_BAD_REQUEST,
     {'wallet_address': ['This field may not be blank.']}],
    ['walletValida', '', 'pk_some_publishable_key', 'usd', 10.91, status.HTTP_400_BAD_REQUEST,
     {'currency_code': ['This field may not be blank.']}],
    ['walletValida', 'eth', '', 'aus', 37.32, status.HTTP_400_BAD_REQUEST,
     {'publishable_key': ['This field may not be blank.']}],
    ['walletValida', 'eth', 'pk_some_publishable_key', '', 10.91, status.HTTP_400_BAD_REQUEST,
     {'base_currency_code': ['This field may not be blank.']}],
    ['walletValida', 'eth', 'pk_some_publishable_key', 'usd', None, status.HTTP_400_BAD_REQUEST,
     {'base_currency_amount': ['This field may not be null.']}],
])
def test_moonpay_link_view_missing_data(client, wallet_address, currency_code,
                                        publishable_key, base_currency_code, base_currency_amount, expected_status,
                                        expected_response):
    data = {
        "wallet_address": wallet_address,
        "currency_code": currency_code,
        "publishable_key": publishable_key,
        "base_currency_code": base_currency_code,
        "base_currency_amount": base_currency_amount
    }
    response = client.post(reverse('moonpay:moonpay-link'), data=data, content_type='application/json')

    assert response.status_code == expected_status
    assert response.json() == expected_response
