import requests

from core.enviroment import DefaultEnvironment, Environment
from directa24.deposit_headers import DefaultDepositHeaders
from directa24.deposit_request_body import DepositRequestBody
from directa24.deposit_request_error_response import DepositRequestErrorResponse
from directa24.deposit_request_response import DepositRequestResponse
from directa24.deposit_request_success_response import DepositRequestSuccessResponse


class DepositRequest:
    def __init__(self,
                 deposit_request_body: DepositRequestBody,
                 environment: Environment = DefaultEnvironment()):
        self._deposit_request_body = deposit_request_body
        self._env = environment

    def _api_url(self) -> str:
        return self._env.by_key('DIRECTA_API_URL')

    def _deposit_url(self) -> str:
        return f'{self._api_url()}deposits'

    def _headers(self):
        return DefaultDepositHeaders.create(self._deposit_request_body, self._env).value()

    def _request(self):
        return requests.post(self._deposit_url(), headers=self._headers(), data=self._deposit_request_body.json())

    def response(self) -> DepositRequestResponse:
        response = self._request()
        return DepositRequestSuccessResponse(response) if response.status_code == 201 \
            else DepositRequestErrorResponse(response)
