from rest_framework.response import Response
from rest_framework.views import APIView
import logging
from directa24.client import Directa24Client
from directa24.deposit import Deposit
from directa24.deposit_request import DepositRequest
from directa24.deposit_request_body import DefaultDepositRequestBody


class CryptoExchangeRateAPIView(APIView):
    directa_client = Directa24Client()

    def get(self, request, *args, **kwargs):
        query_params = dict(self.request.query_params)
        response = Directa24Client().crypto_exchange_rates(**query_params)
        return Response(response.json(), response.status_code)


class DepositLinkAPIView(APIView):

    def post(self, request, *args, **kwargs):
        response = DepositRequest(DefaultDepositRequestBody.create(self.request.data)).response()
        if response.status_code == 201:
            Deposit.create(self.request.data, response.deposit_id)
        return Response(response.json(), status=response.status_code)
