import datetime
from abc import ABC, abstractmethod
from hashlib import sha256
import hmac

from core.enviroment import DefaultEnvironment, Environment
from directa24.deposit_request_body import DepositRequestBody


class DepositHeaders(ABC):

    @abstractmethod
    def create(self, *args):
        """"""

    @abstractmethod
    def value(self):
        """"""


class FakeDepositHeaders(DepositHeaders):
    def __init__(self, headers={}):
        self.headers = headers

    @classmethod
    def create(cls, headers={}):
        return cls(headers)

    def value(self):
        return self.headers


class DefaultDepositHeaders(DepositHeaders):
    def __init__(self, deposit_request_body: DepositRequestBody, x_date: str, x_login: str, api_signature: str):
        self._body = deposit_request_body
        self._x_login = x_login
        self._x_date = x_date
        self._api_signature = api_signature

    @classmethod
    def create(cls, deposit_request_body: DepositRequestBody, env: Environment = DefaultEnvironment()):
        x_date = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat().replace("+00:00", "Z")
        return cls(deposit_request_body, x_date, env.by_key('X_LOGIN'), env.by_key('API_SIGNATURE'))

    def _encode(self, value: str):
        return value.encode('utf8')

    def _message(self):
        return b"".join(map(self._encode, [self._x_date, self._x_login, self._body.json()]))

    def _signature(self):
        return hmac.new(self._api_signature.encode(), self._message(), sha256).hexdigest()

    def value(self):
        return {"X-Login": self._x_login,
                "X-Date": self._x_date,
                "Authorization": f'D24 {self._signature()}',
                "Content-Type": 'application/json'}
