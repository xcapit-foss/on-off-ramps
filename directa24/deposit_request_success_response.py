from directa24.deposit_request_response import DepositRequestResponse


class DepositRequestSuccessResponse(DepositRequestResponse):

    def __init__(self, raw_response):
        self._raw_response = raw_response

    @property
    def deposit_id(self):
        return self._raw_response.json()['deposit_id']

    def json(self):
        return {"link": self._raw_response.json()['redirect_url']}

    @property
    def status_code(self):
        return self._raw_response.status_code
