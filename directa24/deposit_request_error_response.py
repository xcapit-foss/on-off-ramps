from operator import itemgetter

from directa24.deposit_request_response import DepositRequestResponse
import logging


class DepositRequestErrorResponse(DepositRequestResponse):

    def __init__(self, raw_response):
        self._raw_response = raw_response

    def json(self):
        self._log_error()
        return {"error": 'An error occurred when generating the link'}

    @property
    def status_code(self):
        return self._raw_response.status_code

    def _log_error(self):
        logger = logging.getLogger(__name__)
        code, description, type = itemgetter('code', 'description', 'type')(self._raw_response.json())
        logger.error(f"Directa24 Link Error: code: {code}, description: {description}, type: {type}")
