from unittest.mock import patch

import pytest

from directa24.deposit import Deposit
from directa24.models import DirectaDeposit


@pytest.mark.django_db
@patch('requests.post')
def test_deposit_create(mock_post, request_input_data, expected_deposit_request_success_response):
    mock_post.return_value = expected_deposit_request_success_response
    deposit = Deposit.create(request_input_data, 1)

    assert DirectaDeposit.objects.get(deposit_id=deposit.id)


@pytest.mark.django_db
def test_deposit_deposit_id(created_deposit):
    assert Deposit(created_deposit).id == 1


@pytest.mark.django_db
def test_deposit_status(created_deposit):
    assert Deposit(created_deposit).status == 'CREATED'
