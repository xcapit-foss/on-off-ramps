from unittest.mock import Mock
from directa24.deposit_headers import DefaultDepositHeaders
from directa24.deposit_request_body import DefaultDepositRequestBody, FakeDepositRequestBody


def test_deposit_headers_new():
    assert DefaultDepositHeaders(Mock(spec=DefaultDepositRequestBody), '', '', '')


def test_deposit_headers_create():
    assert DefaultDepositHeaders.create(Mock(spec=DefaultDepositRequestBody))


def test_deposit_headers_value(expected_deposit_headers, expected_deposit_request_body):
    deposit_headers = DefaultDepositHeaders(FakeDepositRequestBody(expected_deposit_request_body),
                                            '2022-08-31T00:00:00Z',
                                            'testKey',
                                            'test_api_signature')
    assert deposit_headers.value() == expected_deposit_headers
