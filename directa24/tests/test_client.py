import responses
from directa24.client import Directa24Client


def test_new():
    assert Directa24Client()


@responses.activate
def test_crypto_exchange_rates():
    expected_url = 'http://test-endpoint/exchange_rates/crypto?currency=USD&amount=100&crypto=USDT'
    responses.add(responses.GET, expected_url, json={}, status=200)
    client = Directa24Client('http://test-endpoint/')
    response = client.crypto_exchange_rates('USD', 100, 'USDT')
    assert response
    assert responses.calls[0].request.url == expected_url
