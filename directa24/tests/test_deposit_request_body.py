import json
from directa24.deposit_request_body import DefaultDepositRequestBody


def test_deposit_request_body_new(request_input_data, expected_deposit_request_body):
    assert DefaultDepositRequestBody(request_input_data, environment='PREPROD')


def test_deposit_request_body_create(request_input_data, expected_deposit_request_body):
    assert DefaultDepositRequestBody.create(request_input_data)


def test_deposit_request_body_value(request_input_data, expected_deposit_request_body):
    body = DefaultDepositRequestBody(request_input_data, environment='PREPROD')
    assert body.value() == expected_deposit_request_body


def test_deposit_request_body_json(request_input_data, expected_deposit_request_body):
    body = DefaultDepositRequestBody(request_input_data, environment='PREPROD')
    assert body.json() == json.dumps(expected_deposit_request_body)
