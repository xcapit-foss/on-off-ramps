from unittest.mock import Mock

from directa24.models import DirectaDeposit
import pytest


@pytest.fixture
def request_input_data():
    return {
        "amount": 1000,
        "fiat_token": "ARS",
        "crypto_token": "USDC",
        "wallet": "0xsome_test_wallet",
        "country": "AR",
        "payment_method": "UU",
        "mobile": True,
        "language": "es",
        "back_url": "https://test-page.com/initial_page",
        "success_url": "https://test-page.com/success",
        "error_url": "https://test-page.com/error",
        "notification_url": "https://test-page.com/webhook",
        "logo": "https://test-page.com/logo.png",
    }


@pytest.fixture
def expected_deposit_request_body():
    return {
        "amount": 1000,
        "country": "AR",
        "currency": "ARS",
        "payment_method": "UU",
        "test": True,
        "mobile": True,
        "language": "es",
        "payer": {
            "id": "0xsome_test_wallet"
        },
        "fee_on_payer": True,
        "request_payer_data_on_validation_failure": True,
        "crypto": {
            "currency": "USDC",
            "wallet": "0xsome_test_wallet",
            "network": "MATIC"
        },
        "back_url": "https://test-page.com/initial_page",
        "success_url": "https://test-page.com/success",
        "error_url": "https://test-page.com/error",
        "notification_url": "https://test-page.com/webhook",
        "logo": "https://test-page.com/logo.png",
    }


@pytest.fixture
def expected_deposit_headers():
    return {'X-Login': 'testKey',
            'X-Date': '2022-08-31T00:00:00Z',
            'Authorization': 'D24 739a69c0cc0e2129b4f2ba399cdfd3e7790ad72801a67804764ac7d80b2fcc27',
            'Content-Type': 'application/json'}


@pytest.fixture
def deposit_request_success_response_json():
    return {'checkout_type': 'HOSTED',
            'redirect_url': 'https://test-url.com/validate/this_is_a_test_hash',
            'deposit_id': 1,
            'user_id': '0xsome_test_wallet',
            'merchant_invoice_id': '1'}


@pytest.fixture
def deposit_request_error_response_json():
    return {
        "code": 304,
        "description": "The user limit has been exceeded: TRANSACTION",
        "type": "USER_LIMIT_EXCEEDED"
    }


@pytest.fixture
def expected_deposit_request_success_response(deposit_request_success_response_json):
    return Mock(json=lambda: deposit_request_success_response_json, status_code=201)


@pytest.fixture
def expected_deposit_request_error_response(deposit_request_error_response_json):
    return Mock(json=lambda: deposit_request_error_response_json, status_code=400)


@pytest.fixture
def created_deposit():
    data = {
        "amount": 1000,
        "fiat_token": "ARS",
        "crypto_token": "USDC",
        "wallet": "0xsome_test_wallet",
        "country": "AR",
        "payment_method": "UU",
        "deposit_id": 1
    }
    return DirectaDeposit.objects.create(**data)
