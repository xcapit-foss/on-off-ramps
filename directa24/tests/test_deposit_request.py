from unittest.mock import patch
from core.enviroment import FakeEnvironment
from directa24.deposit_request import DepositRequest
from directa24.deposit_request_body import FakeDepositRequestBody
from directa24.deposit_request_success_response import DepositRequestSuccessResponse


def test_deposit_request_new():
    assert DepositRequest(FakeDepositRequestBody(), FakeEnvironment(['api_url']))


@patch('requests.post')
def test_deposit_request_response(mock_post, expected_deposit_request_success_response, expected_deposit_request_body):
    mock_post.return_value = expected_deposit_request_success_response
    deposit = DepositRequest(FakeDepositRequestBody(), FakeEnvironment(['api_url', 'testKey', 'test_api_signature']))
    assert deposit.response().json() == DepositRequestSuccessResponse(expected_deposit_request_success_response).json()
