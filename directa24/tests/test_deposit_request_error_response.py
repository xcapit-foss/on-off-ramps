from directa24.deposit_request_error_response import DepositRequestErrorResponse


def test_deposit_request_error_response_create(expected_deposit_request_error_response):
    assert DepositRequestErrorResponse(expected_deposit_request_error_response)


def test_deposit_request_error_response_json(expected_deposit_request_error_response):
    assert DepositRequestErrorResponse(expected_deposit_request_error_response).json() == {
        "error": 'An error occurred when generating the link'}


def test_deposit_request_error_response_status_code(expected_deposit_request_error_response):
    assert DepositRequestErrorResponse(expected_deposit_request_error_response).status_code == 400
