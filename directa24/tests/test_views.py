from unittest.mock import patch, Mock
from django.urls import reverse
from requests import Response
import pytest


@patch('directa24.client.Directa24Client.crypto_exchange_rates')
def test_crypto_exchange_rates_api_view(mock_crypto_exchange_rates, client):
    mock_crypto_exchange_rates.return_value = Mock(spec=Response, json=lambda: {}, status_code=200)
    data = {'base': 'USD', 'quote': 'USDT', 'amount': 100}
    response = client.get(reverse('directa:crypto-exchange-rate'), data=data)
    assert response


@patch('requests.post')
@pytest.mark.django_db
def test_deposit_link_api_view(mock_post, client, request_input_data, expected_deposit_request_success_response):
    mock_post.return_value = expected_deposit_request_success_response
    response = client.post(reverse('directa:deposit-link'), data=request_input_data)
    assert response.json() == {'link': 'https://test-url.com/validate/this_is_a_test_hash'}
    assert response.status_code == 201
