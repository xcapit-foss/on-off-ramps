from directa24.deposit_request_success_response import DepositRequestSuccessResponse


def test_success_response_create(expected_deposit_request_success_response):
    assert DepositRequestSuccessResponse(expected_deposit_request_success_response)


def test_success_response_json(expected_deposit_request_success_response):
    assert DepositRequestSuccessResponse(expected_deposit_request_success_response).json() == {
        "link": 'https://test-url.com/validate/this_is_a_test_hash'}


def test_success_response_status_code(expected_deposit_request_success_response):
    assert DepositRequestSuccessResponse(expected_deposit_request_success_response).status_code == 201


def test_success_response_deposit_id(expected_deposit_request_success_response):
    assert DepositRequestSuccessResponse(expected_deposit_request_success_response).deposit_id == 1
