from abc import ABC, abstractmethod


class DepositRequestResponse(ABC):

    @abstractmethod
    def json(self):
        """"""

    @abstractmethod
    def status_code(self):
        """"""
