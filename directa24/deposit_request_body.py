import json
from abc import ABC, abstractmethod

from core.enviroment import DefaultEnvironment, Environment


class DepositRequestBody(ABC):
    @abstractmethod
    def value(self):
        """"""

    @abstractmethod
    def json(self):
        """"""


class FakeDepositRequestBody(DepositRequestBody):

    def __init__(self, value_result={}):
        self.value_result = value_result

    def value(self):
        return self.value_result

    def json(self):
        return json.dumps(self.value())


class DefaultDepositRequestBody(DepositRequestBody):
    def __init__(self, deposit_raw_data, environment: str):
        self._deposit_raw_data = deposit_raw_data
        self._environment = environment

    @classmethod
    def create(cls, deposit_raw_data, env: Environment = DefaultEnvironment()):
        return cls(deposit_raw_data, env.by_key('ENTORNO'))

    def _is_test_env(self):
        return self._environment != 'PRODUCCION'

    def value(self):
        return {
            "amount": self._deposit_raw_data['amount'],
            "country": self._deposit_raw_data['country'],
            "currency": self._deposit_raw_data['fiat_token'],
            "payment_method": self._deposit_raw_data['payment_method'],
            "test": self._is_test_env(),
            "mobile": self._deposit_raw_data['mobile'],
            "language": self._deposit_raw_data['language'],
            "payer": {
                "id": self._deposit_raw_data['wallet']
            },
            "fee_on_payer": True,
            "request_payer_data_on_validation_failure": True,
            "crypto": {
                "currency": self._deposit_raw_data['crypto_token'],
                "wallet": self._deposit_raw_data['wallet'],
                "network": "MATIC"
            },
            "back_url": self._deposit_raw_data['back_url'],
            "success_url": self._deposit_raw_data['success_url'],
            "error_url": self._deposit_raw_data['error_url'],
            "notification_url": self._deposit_raw_data['notification_url'],
            "logo": self._deposit_raw_data['logo'],
        }

    def json(self):
        return json.dumps(self.value())
