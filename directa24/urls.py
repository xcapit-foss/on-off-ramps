from django.urls import path

from directa24.views import CryptoExchangeRateAPIView, DepositLinkAPIView

app_name = "directa"

urlpatterns = [
    path('crypto_exchange_rate', CryptoExchangeRateAPIView.as_view(), name='crypto-exchange-rate'),
    path('deposit_link', DepositLinkAPIView.as_view(), name='deposit-link')
]
