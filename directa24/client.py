from core.clients import ApiClient
from on_off_ramps.settings import DIRECTA_API_URL, DIRECTA_API_KEY


class Directa24Client(ApiClient):
    def __init__(self, api_url: str = DIRECTA_API_URL):
        super().__init__(api_url)

    def _headers(self):
        return {'Authorization': f'Bearer {DIRECTA_API_KEY}'}

    def crypto_exchange_rates(self, base: str, amount: float, quote: str):
        params = {"currency": base, "amount": amount, "crypto": quote}
        return self._http.get(self._generate_endpoint('exchange_rates/crypto'), params=params, headers=self._headers())
