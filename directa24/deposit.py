from directa24.models import DirectaDeposit


class Deposit:
    def __init__(self, deposit_data):
        self.data = deposit_data

    @classmethod
    def create(cls, raw_deposit_data, deposit_id):
        deposit_data = {field: raw_deposit_data[field] for field in DirectaDeposit.fields() if
                        raw_deposit_data.get(field)}
        deposit_data['deposit_id'] = deposit_id
        deposit = DirectaDeposit.objects.create(**deposit_data)
        return cls(deposit)

    @property
    def id(self):
        return self.data.deposit_id

    @property
    def status(self):
        return self.data.status
