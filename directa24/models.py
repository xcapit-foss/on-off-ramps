from django.db import models

DIRECTA_STATUSES = [
    ('CREATED', 'CREATED'),
    ('INITIATED', 'INITIATED'),
    ('PENDING', 'PENDING'),
    ('FOR_REVIEW', 'FOR_REVIEW'),
    ('EXPIRED', 'EXPIRED'),
    ('CANCELLED', 'CANCELLED'),
    ('APPROVED', 'APPROVED'),
    ('COMPLETED', 'COMPLETED')
]


class DirectaDeposit(models.Model):
    wallet = models.CharField(max_length=100)
    deposit_id = models.IntegerField()
    amount = models.FloatField()
    fiat_token = models.CharField(max_length=20)
    crypto_token = models.CharField(max_length=20)
    payment_method = models.CharField(max_length=10)
    country = models.CharField(max_length=10)
    status = models.CharField(max_length=20, choices=DIRECTA_STATUSES, default='CREATED')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def fields(cls):
        return [field.get_attname() for field in DirectaDeposit._meta.get_fields()]
