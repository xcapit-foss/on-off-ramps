"""on_off_ramps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('kripton.urls', namespace='kripton')),
    path('api/currencies/', include('currencies.urls', namespace='currencies')),
    path('api/operations/', include('operations.urls', namespace='operations')),
    path('api/paxful/', include('paxful.urls', namespace='paxful')),
    path('api/moonpay/', include('moonpay.urls', namespace='moonpay')),
    path('api/directa/', include('directa24.urls', namespace='directa')),
    path('api/notifications/', include('notifications.urls', namespace='notifications'))
]
