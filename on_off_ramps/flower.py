from on_off_ramps.settings import CELERY_BROKER_URL


address = '0.0.0.0'
port = 5555
basic_auth = ['xcapit:xcapit1234']
broker = CELERY_BROKER_URL
