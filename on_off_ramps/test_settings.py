from on_off_ramps.settings import *


SECRET_KEY = 'DJANGO_SECRET_KEY'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
        'TIME_ZONE': 'UTC',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
