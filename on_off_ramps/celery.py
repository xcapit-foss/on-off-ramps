import os
from celery import Celery
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'on_off_ramps.settings')
app = Celery('on_off_ramps')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'update-kripton-purchases-status-0100am': {
        'task': 'operations.tasks.update_kripton_purchases_status',
        'schedule': crontab(minute=0, hour=1)
    },
}
app.conf.timezone = 'UTC'
