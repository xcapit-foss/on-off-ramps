class CustomError(Exception):
    DEFAULT_MESSAGE = 'Error en el proveedor'

    def __init__(self, message=''):
        self.message = message or self.DEFAULT_MESSAGE
        super().__init__(self.message)
