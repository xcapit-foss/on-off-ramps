import requests
from enum import Enum
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


class RequestsSessionBuilder:

    class Prefix(Enum):
        http = 'http://'
        https = 'https://'

    def __init__(self):
        self._session = requests.Session()

    @property
    def session(self):
        return self._session

    def set_retry(self):
        retry_strategy = Retry(
            total=3,
            status_forcelist=[400, 401, 429, 500, 502, 503, 504],
            method_whitelist=["HEAD", "GET", "PUT", "DELETE", "OPTIONS", "TRACE"]
        )

        self._mount(
            prefixes=[self.Prefix.http.value, self.Prefix.https.value],
            adapter=HTTPAdapter(max_retries=retry_strategy)
        )
        return self

    def _mount(self, prefixes: list, adapter: HTTPAdapter):
        for prefix in prefixes:
            self._session.mount(prefix, adapter)
