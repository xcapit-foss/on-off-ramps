import os
from abc import ABC, abstractmethod


class Environment(ABC):
    @abstractmethod
    def by_key(self, key: str, default: str):
        """"""


class DefaultEnvironment(Environment):

    def __init__(self, env=os.environ):
        self._env = env

    def by_key(self, key: str, default: str = None):
        return self._env.get(key, default)


class FakeEnvironment(Environment):

    def __init__(self, by_key_results=[]):
        self._by_key_results = by_key_results

    def by_key(self, key: str, default: str = None):
        return self._by_key_results.pop(0)
