import os
from unittest.mock import Mock
from core.enviroment import DefaultEnvironment


def test_environment_create():
    assert DefaultEnvironment()


def test_environment_by_key():
    env_mock = Mock(spec=os.environ, get=lambda key, default: 'variable_value')
    assert DefaultEnvironment(env_mock).by_key('SOME_VARIABLE') == 'variable_value'
