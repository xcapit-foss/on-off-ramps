from abc import ABC
from urllib.parse import urljoin
from core.requests_session_builder import RequestsSessionBuilder


class ApiClient(ABC):

    def __init__(self, api_url: str):
        self._api_url = api_url
        self._http = RequestsSessionBuilder().set_retry().session

    def _generate_endpoint(self, path: str) -> str:
        return urljoin(self._api_url, path)



