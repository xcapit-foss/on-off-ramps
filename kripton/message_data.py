from abc import ABC


class MessageData(ABC):
    def title(self):
        """"""

    def body(self):
        """"""


class DefaultMessageData(MessageData):
    def __init__(self, a_title: str, a_message: str):
        self._a_title = a_title
        self._a_message = a_message

    def title(self):
        return self._a_title

    def body(self):
        return self._a_message


class NullMessageData(MessageData):

    def title(self):
        return None

    def body(self):
        return None
