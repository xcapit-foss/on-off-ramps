from django.apps import AppConfig


class KriptonConfig(AppConfig):
    name = 'kripton'
