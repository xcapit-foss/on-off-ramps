from kripton.exchange_rate_of import ExchangeRateOf
from kripton.kripton_fee_request import KriptonFeeRequest
from on_off_ramps.settings import KRIPTON_MINIMUM_CASH_IN, KRIPTON_MINIMUM_CASH_OUT
from providers.kripton.clients import KriptonClient, DefaultKriptonClient
from providers.kripton.kripton_user import KriptonUser


class KriptonMinimumAmount:
    def __init__(self, operation_type: str, currency_in: str, currency_out: str, network: str,
                 a_kripton_user: KriptonUser, a_kripton_client: KriptonClient = DefaultKriptonClient(),
                 a_cash_in_minimum: str = KRIPTON_MINIMUM_CASH_IN,
                 a_cash_out_minimum: str = KRIPTON_MINIMUM_CASH_OUT):
        self._operation_type = operation_type
        self._currency_in = currency_in
        self._currency_out = currency_out
        self._network = network
        self._client = a_kripton_client
        self._kripton_user = a_kripton_user
        self._cash_in_minimum = float(a_cash_in_minimum)
        self._cash_out_minimum = float(a_cash_out_minimum)

    def value(self) -> float:
        minimum = 0.0
        if self._operation_type == 'cash-in':
            kripton_fee = KriptonFeeRequest(self._operation_type, self._currency_in, self._currency_out,
                                            self._network, self._client).response()
            service_cost = (self._cash_in_minimum * (kripton_fee.service_fee_percentage() / 100))
            minimum = (self._cash_in_minimum + service_cost + kripton_fee.network_fee()) * self._exchange_rate()
            minimum = round(minimum, 2)
        elif self._operation_type == 'cash-out':
            minimum = float(self._cash_out_minimum)

        return minimum

    def _exchange_rate(self):
        crypto_exchange_rate = 0.0
        if self._operation_type == 'cash-in':
            crypto_exchange_rate = ExchangeRateOf(self._currency_out, self._currency_in, self._client).value()
        elif self._operation_type == 'cash-out':
            crypto_exchange_rate = ExchangeRateOf(self._currency_in, self._currency_out, self._client).value()
        return crypto_exchange_rate
