from django.db import models


class UserModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    app_id = models.IntegerField()
    provider = models.IntegerField()
    provider_user_id = models.IntegerField()
    email = models.CharField(max_length=100, default=None, null=False)
    registration_status = models.CharField(max_length=50, default='USER_INFORMATION', null=True)
    user_id = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'users_usermodel'

    @classmethod
    def create(cls, app_id, provider, provider_user_id, email, user_id):
        user = UserModel.objects.create(
            app_id=app_id,
            provider=provider,
            provider_user_id=provider_user_id,
            email=email,
            user_id=user_id
        )
        return user