class KriptonExchangeRateNotFound(Exception):
    def __init__(self, a_crypto_currency):
        self._crypto_currency = a_crypto_currency
        self.message = f"Currency not found in Kripton Quotations({a_crypto_currency.upper()})"
        super().__init__(self.message)
