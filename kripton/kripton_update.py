from kripton.models import UserModel


class KriptonUpdate:
    def __init__(self, raw_data, user_model=UserModel):
        self._raw_data = raw_data
        self._user_model = user_model

    def user(self) -> UserModel:
        return self._user_model.objects.filter(provider_user_id=self.request_user_id()).last()

    def request_user_id(self) -> int:
        return self._raw_data.get('data', {}).get('user_id')

    def kyc_approval(self):
        return self._raw_data.get('kyc_approved')

    def is_update(self):
        return self._raw_data.get('action') == 'update'

    def is_cancel(self):
        return self._raw_data.get('action') == 'cancel'

    def is_buy(self):
        return self._operation_type() == 'CashIn'

    def is_sell(self):
        return self._operation_type() == 'CashOut'

    def is_kyc(self):
        return self._raw_data.get('resource') in ['domicile', 'personal_information', 'fiscal_information']

    def is_operation(self):
        return self._raw_data.get('resource') == 'operation'

    def _operation_type(self):
        return self._raw_data.get('data').get('type').split('::')[1]
