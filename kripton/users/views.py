from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from kripton.kripton_available_currencies_of import KriptonAvailableCurrenciesOf
from kripton.kripton_login import KriptonLogin
from kripton.kripton_access_token_response import KriptonAccessTokenResponse
from kripton.parsed_network import ParsedNetwork
from kripton.tests.test_kripton_minimum_amount import KriptonMinimumAmount
from providers.kripton.clients import DefaultKriptonClient
from providers.kripton.kripton_users import KriptonUsers
from providers.service import ProvidersService, GetProviderError
from providers.kripton.service import CustomError
from kripton.users.services import UsersService
from requests.exceptions import RetryError


class GetOrCreateView(APIView):

    def post(self, request, *args, **kwargs):
        try:
            kripton_user = KriptonUsers().get_or_create_by(request.data.get('email'), request.data.get('user_id'))

            response = Response({'kyc_approved': kripton_user.kyc_approved,
                                 'registration_status': kripton_user.registration_status},
                                status=status.HTTP_200_OK)

        except (GetProviderError, CustomError, RetryError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.user.create.errorProvider'

        finally:
            return response


class SaveUserInfoView(APIView):
    provider_service = ProvidersService()

    def post(self, request, provider_id, *args, **kwargs):
        try:
            data = request.data.copy()
            data['birthday'] = self._formatted_birthday(data['birthday'])
            user_id = data.get('user_id')
            email = data.get('email')
            auth_token = data.get('auth_token')
            data.pop('user_id')
            data.pop('email')
            data.pop('auth_token')

            kripton_user = KriptonUsers().get_or_create_by(email, user_id)
            provider = self.provider_service.get_provider_client(provider_id=provider_id)

            result = provider.save_user_information(provider_user_id=kripton_user.id, data=data, auth_token=auth_token)
            response = Response(result, status=status.HTTP_200_OK)

        except (GetProviderError, CustomError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.user.save_info.errorProvider'
        except RetryError:
            response = Response({"error": 'Error al guardar la informacion, token de acceso invalido',
                                 "error_code": "on_off_ramps.user.save_info.errorProvider"},
                                status.HTTP_401_UNAUTHORIZED)
        finally:
            return response

    def _formatted_birthday(self, raw_birthday: str):
        month, day, year = raw_birthday.split('/')
        return "/".join([year, month, day])


class SaveUserBankView(APIView):
    users_service = UsersService()
    provider_service = ProvidersService()

    def post(self, request, provider_id, *args, **kwargs):
        try:
            data = request.data.copy()
            user_id = data.get('user_id')
            email = data.get('email')
            auth_token = data.get('auth_token')
            data.pop('user_id')
            data.pop('email')
            data.pop('auth_token')

            kripton_user = KriptonUsers().get_or_create_by(email, user_id)
            provider = self.provider_service.get_provider_client(provider_id=provider_id)

            result = provider.save_user_bank(provider_user_id=kripton_user.id, data=data, auth_token=auth_token)
            response = Response(result, status=status.HTTP_200_OK)

        except (GetProviderError, CustomError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.user.save_user_bank.errorProvider'
        except RetryError:
            response = Response({"error": 'Error al guardar la informacion, token de acceso invalido',
                                 "error_code": "on_off_ramps.user.save_user_bank.errorProvider"},
                                status.HTTP_401_UNAUTHORIZED)
        return response


class GetUserBankView(APIView):
    users_service = UsersService()
    provider_service = ProvidersService()

    def post(self, request, provider_id, *args, **kwargs):
        try:
            data = request.data.copy()
            user_id = data.get('user_id')
            email = data.get('email')
            auth_token = data.get('auth_token')
            payment_method_id = data.get('payment_method_id')
            data.pop('user_id')
            data.pop('email')
            data.pop('auth_token')
            data.pop('payment_method_id')

            user = KriptonUsers().get_or_create_by(email, user_id)

            provider = self.provider_service.get_provider_client(provider_id=provider_id)

            result = provider.get_user_bank(provider_user_id=user.id, payment_method_id=payment_method_id,
                                            auth_token=auth_token)
            response = Response(result, status=status.HTTP_200_OK)

            if not result:
                response = Response({"error": "No se ha encontrado el metodo de pago solicitado"},
                                    status.HTTP_400_BAD_REQUEST)

        except (GetProviderError, CustomError) as e:
            response = Response({"error": str(e),
                                 'error_code': 'on_off_ramps.user.get_user_bank.errorProvider'},
                                status.HTTP_400_BAD_REQUEST)
        except RetryError:
            response = Response(
                {"error": 'Error al obtener la información bancaria del usuario, token de accesso invalido',
                 "error_code": "on_off_ramps.user.get_user_bank.errorProvider"},
                status.HTTP_401_UNAUTHORIZED)

        return response


class SaveUserImage(APIView):
    users_service = UsersService()
    provider_service = ProvidersService()

    def post(self, request, provider_id, *args, **kwargs):
        try:
            data = request.data.copy()
            user_id = data.get('user_id')
            email = data.get('email')
            auth_token = data.get('auth_token')
            data.pop('user_id')
            data.pop('auth_token')
            data.pop('email')
            user = KriptonUsers().get_or_create_by(email, user_id)

            provider = self.provider_service.get_provider_client(provider_id=provider_id)

            result = provider.upload_user_image(provider_user_id=user.id, data=data, auth_token=auth_token)
            response = Response(result, status=status.HTTP_200_OK)

        except (GetProviderError, CustomError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.user.save_images.errorProvider'
        except RetryError:
            response = Response({"error": 'Error al guardar las imagenes, token de accesso invalido',
                                 "error_code": "on_off_ramps.user.save_images.errorProvider"},
                                status.HTTP_401_UNAUTHORIZED)

        finally:
            return response


class RequestAccessTokenView(APIView):

    def post(self, request, *args, **kwargs):
        response = KriptonAccessTokenResponse(request.data.get('email')).value()
        data = response.json() if response.status_code != 200 else None
        return Response(data=data, status=response.status_code)


class LoginView(APIView):

    def post(self, request, *args, **kwargs):
        response = KriptonLogin(**request.data.copy()).value()
        return Response(response.json(), status=response.status_code)


class RefreshTokenView(APIView):
    kripton_client = DefaultKriptonClient()

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        access_token = data.pop('access_token', None)
        refresh_token = data.pop('refresh_token', None)
        response = Response('Error: bad access or refresh token', status.HTTP_400_BAD_REQUEST)
        if access_token is not None and refresh_token is not None:
            result = self.kripton_client.refresh_token(access_token, refresh_token)
            response = Response(result.json(), result.status_code)
        return response


class GetUserLimitsLegacyView(APIView):
    kripton_client = DefaultKriptonClient()

    def post(self, request, mode, user_id, currency, *args, **kwargs):
        if not request.data.get('email'):
            response = Response({"error": 'Email is required',
                                 'error_code': 'on_off_ramps.kripton.user_limit.error'},
                                status.HTTP_400_BAD_REQUEST)
        else:
            email = request.data.get('email')
            user = KriptonUsers().get_or_create_by(email, user_id)
            limits_response = self.kripton_client.get_user_limits(mode, user.id, currency)
            if limits_response.status_code != 200:
                response = Response({"error": 'Error al obtener los limites desde proveedor',
                                     'error_code': 'on_off_ramps.kripton.user_limit.errorProvider'},
                                    status.HTTP_400_BAD_REQUEST)
            else:
                response = Response(limits_response.json().get("data"), status=status.HTTP_200_OK)
        return response


class GetUserLimitsView(APIView):
    kripton_client = DefaultKriptonClient()

    def post(self, request, *args, **kwargs):
        request_data = request.data.copy()
        email = request_data.get('email')
        operation_type = request_data.get('operation_type')
        user_id = request_data.get('user_id')
        currency_in = request_data.get('currency_in')
        currency_out = request_data.get('currency_out')
        network = request_data.get('network_out')

        if not email:
            response = Response({"error": 'Email is required',
                                 'error_code': 'on_off_ramps.kripton.user_limit.error'},
                                status.HTTP_400_BAD_REQUEST)
        else:
            kripton_user = KriptonUsers().get_or_create_by(email, user_id)
            user_limits_response = self.kripton_client.get_user_limits(operation_type,
                                                                       kripton_user.id,
                                                                       currency_in)
            if user_limits_response.status_code != 200:
                response = Response({"error": 'Error al obtener los limites desde proveedor',
                                     'error_code': 'on_off_ramps.kripton.user_limit.errorProvider'},
                                    status.HTTP_400_BAD_REQUEST)
            else:
                kripton_minimum = KriptonMinimumAmount(operation_type, currency_in, currency_out,
                                                       ParsedNetwork(network).value(), kripton_user)
                user_limits = user_limits_response.json().get("data")
                user_limits["minimum_general"] = max(float(user_limits["minimun_general"]), kripton_minimum.value())
                del user_limits["minimun_general"]
                response = Response(user_limits, status=status.HTTP_200_OK)
        return response


class AvailableCurrenciesView(APIView):
    def get(self, *args, **kwargs):
        try:
            response = Response(KriptonAvailableCurrenciesOf().value(), status=status.HTTP_200_OK)

        except (GetProviderError, CustomError, RetryError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.available_currencies'

        finally:
            return response
