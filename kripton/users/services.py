from kripton.models import UserModel
from providers.enums import ProvidersEnum


class UsersService:

    def __init__(self):
        self._model = UserModel

    def get_user_id_by_provider(self, provider_id, user_id):
        if provider_id == "1":
            user = self._model.objects.filter(user_id=user_id, provider=provider_id)

            id_for_provider = user[0].provider_user_id if user.exists() else None
        else:
            id_for_provider = user_id

        return id_for_provider

    def get_user_email_by_id(self, provider_id, user_id):
        if provider_id == "1":
            user_email = user_id
        else:
            user = self._model.objects.filter(user_id=user_id, provider=provider_id)
            user_email = user[0].email if user.exists() else None
        return user_email

    def get_all_user_ids_from_active_providers(self, user_id):
        all_user_ids = []
        for provider in ProvidersEnum:
            user_id_by_provider = self.get_user_id_by_provider(provider_id=provider.value, user_id=user_id)
            if user_id_by_provider is not None:
                all_user_ids.append(user_id_by_provider)
        return all_user_ids
