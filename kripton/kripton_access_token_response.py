from providers.kripton.clients import KriptonClient, DefaultKriptonClient
import requests


class KriptonAccessTokenResponse:

    def __init__(self, email: str, client: KriptonClient = DefaultKriptonClient()):
        self._email = email
        self._client = client

    def value(self) -> requests.Response:
        return self._client.request_login_token(self._email)
