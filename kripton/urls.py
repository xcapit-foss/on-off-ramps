from django.urls import path
from kripton.users.views import GetOrCreateView, SaveUserInfoView, SaveUserBankView, GetUserBankView, \
    SaveUserImage, RequestAccessTokenView, LoginView, RefreshTokenView, GetUserLimitsView, \
    AvailableCurrenciesView, GetUserLimitsLegacyView
from kripton.views import NotificationsView

app_name = "kripton"

urlpatterns = [
    path('users/provider/<provider_id>/get_or_create_user', GetOrCreateView.as_view(), name='get-or-create-users'),
    path('users/provider/<provider_id>/save_user_info', SaveUserInfoView.as_view(), name='save-user-info'),
    path('users/provider/<provider_id>/save_user_bank', SaveUserBankView.as_view(), name='save-user-bank'),
    path('users/provider/<provider_id>/get_user_bank', GetUserBankView.as_view(), name='get-user-bank'),
    path('users/provider/<provider_id>/save_user_image', SaveUserImage.as_view(), name='save-user-image'),
    path('users/request_access_token', RequestAccessTokenView.as_view(), name='request-access-token'),
    path('users/login', LoginView.as_view(), name='login'),
    path('users/refresh_token', RefreshTokenView.as_view(), name='refresh-token'),
    path('users/get_user_limits', GetUserLimitsView.as_view(), name='get-user-limits'),
    path('users/get_user_limits/<mode>/<user_id>/currency/<currency>', GetUserLimitsLegacyView.as_view(),
         name='get-user-limits-legacy'),
    path('available_currencies', AvailableCurrenciesView.as_view(), name='available-currencies'),
    path('notifications', NotificationsView.as_view(), name='notifications'),
]
