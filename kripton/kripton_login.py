import requests

from providers.kripton.clients import DefaultKriptonClient, KriptonClient


class KriptonLogin:
    def __init__(self, email: str, token: str, client: KriptonClient = DefaultKriptonClient()):
        self._email = email
        self._token = token
        self._client = client

    def value(self) -> requests.Response:
        return self._client.login(self._email, self._token)
