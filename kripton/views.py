import datetime
import os

from rest_framework.views import APIView
from rest_framework.response import Response
from kripton.delivery import Delivery
from kripton.kripton_update import KriptonUpdate
from kripton.message_of import MessageOf
from notifications.notifications import PushNotification


class NotificationsView(APIView):

    def post(self, request, *args, **kwargs):
        Delivery(MessageOf(KriptonUpdate(request.data)), PushNotification).push_notification().send()
        self.log_data(request.data)
        return Response()

    @staticmethod
    def log_data(data):
        with open('notifications.txt', 'a') as file:
            file.write('\n--------------------\n')
            file.write(f'[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]\n')
            file.write(f'{data}\n')
