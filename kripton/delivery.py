from kripton.message_of import MessageOf
from notifications.channels import PushChannel
from notifications.devices import FCMDevice
from push_notifications.models import GCMDevice

from notifications.notifications import Notification, NullNotification


class Delivery:

    def __init__(self, a_message_of: MessageOf, a_push_notification_class):
        self._a_message_of = a_message_of
        self._a_push_notification_class = a_push_notification_class

    def push_notification(self) -> Notification:
        result = NullNotification()
        if self._a_message_of.title() or self._a_message_of.body():
            result = self._a_push_notification_class(
                title=self._a_message_of.title(),
                message=self._a_message_of.body(),
                channel=PushChannel(FCMDevice(GCMDevice.objects.filter(name=self._a_message_of.receiver().user_id))),
            )
        return result

