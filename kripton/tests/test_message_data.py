from kripton.message_data import DefaultMessageData, NullMessageData


def test_message_data():
    assert DefaultMessageData('a_title', 'a_message')


def test_message_data_title():
    assert DefaultMessageData('a_title', 'a_message').title() == 'a_title'


def test_message_data_body():
    assert DefaultMessageData('a_title', 'a_message').body() == 'a_message'


def test_null_message_data():
    assert NullMessageData()


def test_null_message_data_title():
    assert NullMessageData().title() is None


def test_null_message_data_body():
    assert NullMessageData().body() is None
