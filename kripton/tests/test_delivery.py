import pytest

from kripton.delivery import Delivery
from kripton.kripton_update import KriptonUpdate
from kripton.message_of import MessageOf
from notifications.notifications import FakePushNotification
from providers.kripton.clients import FakeKriptonClient


def test_delivery(kripton_update_operation_data):
    assert Delivery(MessageOf(KriptonUpdate(kripton_update_operation_data()), FakeKriptonClient()),
                    FakePushNotification)


@pytest.mark.django_db
def test_delivery_push_notification(kripton_update_operation_data, create_kripton_user):
    create_kripton_user()
    assert Delivery(
        MessageOf(KriptonUpdate(kripton_update_operation_data()), FakeKriptonClient()),
        FakePushNotification
    ).push_notification()


@pytest.mark.django_db
def test_delivery_null_notification(kripton_update_invalid_data, create_kripton_user):
    create_kripton_user()

    assert Delivery(
        MessageOf(
            KriptonUpdate(kripton_update_invalid_data),
            FakeKriptonClient()
        ),
        FakePushNotification
    ).push_notification().send()

