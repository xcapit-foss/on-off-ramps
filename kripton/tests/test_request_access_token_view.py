from unittest.mock import patch
from django.urls import reverse


@patch('kripton.kripton_access_token_response.KriptonAccessTokenResponse.value')
def test_request_access_token_view_ok(mock_send_token_value, client, request_response):
    mock_send_token_value.return_value = request_response(json_data=None)
    response = client.post(reverse('kripton:request-access-token'), data={'email': 'test@test.com'})
    assert response.data is None
    assert response.status_code == 200


@patch('kripton.kripton_access_token_response.KriptonAccessTokenResponse.value')
def test_request_access_token_view_error(mock_send_token_value, client, request_response,
                                         expected_request_access_token_error_body):
    mock_send_token_value.return_value = request_response(json_data=expected_request_access_token_error_body,
                                                          status_code=400)
    response = client.post(reverse('kripton:request-access-token'), data={'email': 'test@test.com'})
    assert response.data == expected_request_access_token_error_body
    assert response.status_code == 400
