import pytest
from django.urls import reverse
from unittest.mock import patch


@pytest.fixture
def user_limits_response():
    return {
        'minimun_general': '1',
        'maximum_month': '10',
        'maximum_year': '100',
        'exception_max_limit': 0
    }


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_success(mock_get_user_limits, client, create_kripton_user, request_response,
                                      user_limits_response):
    create_kripton_user()
    params = {'user_id': 1, 'currency': 'ars', 'mode': 'cash-in'}
    mock_get_user_limits.return_value = request_response({'data': user_limits_response})
    response = client.post(reverse('kripton:get-user-limits-legacy', kwargs=params), data={'email': 'test@test.com'})
    assert response.json() == user_limits_response
    assert response.status_code == 200


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_missing_data(mock_get_user_limits, client, create_kripton_user, request_response,
                                           user_limits_response):
    create_kripton_user()
    params = {'user_id': 1, 'currency': 'ars', 'mode': 'cash-in'}
    mock_get_user_limits.return_value = request_response({'data': user_limits_response})
    response = client.post(reverse('kripton:get-user-limits-legacy', kwargs=params), data={})
    assert response.json() == {"error": 'Email is required', 'error_code': 'on_off_ramps.kripton.user_limit.error'}
    assert response.status_code == 400


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_invalid(mock_get_user_limits, client, create_kripton_user, request_response):
    create_kripton_user()
    params = {'user_id': 1, 'currency': 'ars', 'mode': 'cash-in'}
    mock_get_user_limits.return_value = request_response({}, 400)
    response = client.post(reverse('kripton:get-user-limits-legacy', kwargs=params), data={'email': 'test@test.com'})
    assert response.status_code == 400
    assert response.json() == {"error": 'Error al obtener los limites desde proveedor',
                               'error_code': 'on_off_ramps.kripton.user_limit.errorProvider'}
