from unittest.mock import Mock

from providers.kripton.clients import FakeKriptonClient
from providers.kripton.kripton_user import KriptonUser
import pytest

from kripton.models import UserModel


@pytest.fixture
def kripton_check_user_response():
    return Mock(status_code=200, json=lambda: {"id": 10000408, "kyc_approved": False})


@pytest.fixture
def test_kripton_user(create_kripton_user, kripton_check_user_response):
    user = create_kripton_user(registration_status='USER_INFORMATION')
    return KriptonUser(user, FakeKriptonClient(check_user_response=kripton_check_user_response))


@pytest.fixture
def expected_get_or_create_view_response():
    return {
        "kyc_approved": False,
        "registration_status": "USER_INFORMATION"
    }


@pytest.fixture
def create_kripton_user():
    def ckug(email='test@test.com', registration_status='COMPLETE'):
        return UserModel.objects.create(app_id='1', provider='1', provider_user_id=9999,
                                        email=email, user_id=47, registration_status=registration_status)

    return ckug


@pytest.fixture
def request_response():
    def rr(json_data={}, status_code=200):
        return Mock(json=lambda: json_data, status_code=status_code)

    return rr


@pytest.fixture
def expected_login_response(request_response):
    data = {
        "token": "eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoidG9rZW4iLCJjbGllbnRfaWQiOjk5OSwiY2xpZW50X3R5cGUiOiJVc2Vyczo6Q2xpZW50IiwidXNlcm5hbWUiOiJ7a20tY2xpZW50fVJpY2t5QGdtYWlsLmNvbSIsImV4cGlyZWRfYXQiOiIyMDIyLTEwLTE5IDIyOjI2OjE1IFVUQyIsImxvZ2luX2J5IjoxMDEwN30._p8-Hfo7EuJroNdz85scb9AHu_qIPa9HmGYNwKIGTr4",
        "refresh_token": "eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicmVmcmVzaCIsImNsaWVudF9pZCI6OTk5LCJjbGllbnRfdHlwZSI6IlVzZXJzOjpDbGllbnQiLCJ1c2VybmFtZSI6IntrbS1jbGllbnR9Umlja3lAZ21haWwuY29tIiwiZXhwaXJlZF9hdCI6IjIwMjItMTAtMjEgMjI6MjY6MTUgVVRDIiwibG9naW5fYnkiOjEwMTA3fQ.Vbvn90arImXDMLraUNdqD_70vASVncO00x4KayR8mok"
    }
    return request_response(data)


@pytest.fixture
def expected_request_access_token_error_body():
    return {
        "error": {
            "error_code": 28,
            "message": "Exist an action token valid to this operation. Please wait that expired to request other."
        }
    }


@pytest.fixture
def expected_available_currencies_response():
    return [
        {
            "currency_id": "doc",
            "crypto_network": "rsk"
        },
        {
            "currency_id": "rbtc",
            "crypto_network": "rsk"
        },
        {
            "currency_id": "usdt",
            "crypto_network": "eth"
        },
        {
            "currency_id": "dai",
            "crypto_network": "eth"
        },
        {
            "currency_id": "eth",
            "crypto_network": "eth"
        },
        {
            "currency_id": "usdt",
            "crypto_network": "tron"
        },
        {
            "currency_id": "usdt",
            "crypto_network": "bsc"
        },
        {
            "currency_id": "dai",
            "crypto_network": "bsc"
        },
        {
            "currency_id": "btc",
            "crypto_network": "btc"
        },
        {
            "currency_id": "btc",
            "crypto_network": "Lightning Network"
        },
        {
            "currency_id": "usdc",
            "crypto_network": "polygon"
        },
        {
            "currency_id": "matic",
            "crypto_network": "polygon"
        },
        {
            "currency_id": "dai",
            "crypto_network": "polygon"
        },
        {
            "currency_id": "usdc",
            "crypto_network": "bsc"
        },
        {
            "currency_id": "busd",
            "crypto_network": "bsc"
        },
        {
            "currency_id": "usdt",
            "crypto_network": "bsc"
        }
    ]


@pytest.fixture()
def expected_kripton_available_currencies_of_value_response():
    return [
        {
            'network': 'RSK',
            'currencies': ['DOC', 'RBTC']
        },
        {
            'network': 'ERC20',
            'currencies': ['USDT', 'DAI', 'ETH']
        },
        {
            'network': 'BSC',
            'currencies': ['USDT', 'DAI', 'USDC', 'BUSD']
        },
        {
            'network': 'MATIC',
            'currencies': ['USDC', 'MATIC', 'DAI']
        },
    ]


@pytest.fixture
def raw_bank_info():
    return {
        "country": "COL",
        "currency": "USDC",
        "name": "Test",
        "account_type": "test",
        "type_of_document": "DU",
        "number_of_document": "12345678",
        "account_number": 123,
        "email": "test@test.com",
        "auth_token": "test",
        "user_id": 1
    }


@pytest.fixture
def kripton_update_data():
    return {
        'action': 'update',
        'resource': 'domicile',
        'data': {'id': 1, 'user_id': 9999}
    }


@pytest.fixture
def kripton_update_kyc_data():
    def kukyc(action: str = 'update', resource: str = 'domicile'):
        return {
            'action': action,
            'resource': resource,
            'data': {'id': 1, 'user_id': 9999}
        }
    return kukyc


@pytest.fixture
def kripton_update_operation_data():
    def kuop(action: str = 'update', operation_type: str = 'CashIn'):
        return {
            'action': action,
            'resource': 'operation',
            'data': {'id': 1, 'user_id': 9999, 'type': f'Operations::{operation_type}'}
        }

    return kuop


@pytest.fixture
def kripton_update_invalid_data():
    return {
        'action': 'xxx',
        'resource': 'xxx',
        'data': {'id': 1, 'user_id': 9999}
    }


@pytest.fixture
def get_quotations_response_body():
    return {
        "data": [
            {
                "currency": "usdc",
                "market_price": {
                    "usd": 1.0,
                    "ars": 509.0,
                    "ves": 27.4,
                    "uyu": 39.0,
                    "pen": 0.0,
                    "cop": 4020.0,
                    "crc": 526.5
                },
                "quotations": {
                    "ars": {
                        "sell": 524.27,
                        "buy": 493.73,
                        "swap_sell": 521.72,
                        "swap_buy": 496.28
                    },
                    "usd": {
                        "sell": 1.03,
                        "buy": 0.96,
                        "swap_sell": 1.03,
                        "swap_buy": 0.98
                    },
                    "ves": {
                        "sell": 28.3,
                        "buy": 26.36,
                        "swap_sell": 28.08,
                        "swap_buy": 26.71
                    },
                    "uyu": {
                        "sell": 40.29,
                        "buy": 37.71,
                        "swap_sell": 39.97,
                        "swap_buy": 38.03
                    },
                    "pen": {
                        "sell": 0.0,
                        "buy": 0.0,
                        "swap_sell": 0.0,
                        "swap_buy": 0.0
                    },
                    "cop": {
                        "sell": 4152.66,
                        "buy": 3939.6,
                        "swap_sell": 4120.5,
                        "swap_buy": 3919.5
                    },
                    "crc": {
                        "sell": 543.87,
                        "buy": 506.49,
                        "swap_sell": 539.66,
                        "swap_buy": 513.34
                    }
                },
                "name": "USDC",
                "icons": {
                    "small": "https://public-kriptonmarket.s3.amazonaws.com/img/tokens/small/usdc.png",
                    "medium": "https://public-kriptonmarket-staging.s3.amazonaws.com/img/tokens/medium/usdc.png"
                }
            },
        ]}


@pytest.fixture
def calculate_amount_out_response_body_cash_in():
    return {
        "data": {
            "amount_in": "10000.0",
            "amount_out": "18.16",
            "costs": "1.48",
            "commissions": {
                "percentage": 3.0,
                "amount": 0.58939096
            },
            "taxes": {
                "percentage": 1.5,
                "amount": 0.29469548
            },
            "fee_of_network": "0.6"
        }
    }


@pytest.fixture
def calculate_amount_out_response_body_cash_out():
    return {
        "data": {
            "amount_in": "20.0",
            "amount_out": "9653.28",
            "costs": "486.72",
            "commissions": {
                "percentage": 3.0,
                "amount": 0.6
            },
            "taxes": {
                "percentage": 1.7999999999999998,
                "amount": 0.36
            },
            "fee_of_network": "0.0"
        }
    }


@pytest.fixture
def get_user_limits_response_body_cash_in():
    return {
        "data": {
            "minimun_general": "6740.0",
            "maximum_month": "674000.0",
            "maximum_year": "1305612.14",
            "exception_max_limit": 0
        }
    }


@pytest.fixture
def get_user_limits_response_body_cash_out():
    return {
        "data": {
            "minimun_general": 20.0,
            "maximum_month": 2000.0,
            "maximum_year": 3980.6,
            "exception_max_limit": 0
        }
    }

@pytest.fixture
def expected_user_limits_with_local_cash_in_minimum(get_user_limits_response_body_cash_in):
    user_limits = get_user_limits_response_body_cash_in.copy().get("data")
    user_limits["minimum_general"] = 6740.0
    return user_limits


@pytest.fixture
def expected_user_limits_with_local_cash_out_minimum(get_user_limits_response_body_cash_out):
    user_limits = get_user_limits_response_body_cash_out.copy().get("data")
    user_limits["minimum_general"] = 21.0
    return user_limits


@pytest.fixture
def set_cash_in_minimum(monkeypatch):
    monkeypatch.setenv('KRIPTON_MINIMUM_CASH_IN', '25')


@pytest.fixture
def set_cash_out_minimum(monkeypatch):
    monkeypatch.setenv('KRIPTON_MINIMUM_CASH_out', '21')
