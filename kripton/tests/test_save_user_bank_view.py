import pytest
from unittest.mock import patch
from django.urls import reverse
from rest_framework import status
from requests.exceptions import RetryError


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.register_user_bank_account')
def test_save_user_bank_view(mock_register_user_bank_account, client, raw_bank_info, create_kripton_user,
                             request_response):
    create_kripton_user()
    mock_register_user_bank_account.return_value = request_response({'data': {'id': 101}})

    response = client.post(reverse('kripton:save-user-bank', kwargs={'provider_id': 1}), data=raw_bank_info)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'status': 'Ok', 'id': 101}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.register_user_bank_account')
def test_save_user_bank_view_unauthorized(mock_register_user_bank_account, client, raw_bank_info,
                                          create_kripton_user):
    create_kripton_user()
    mock_register_user_bank_account.side_effect = RetryError

    response = client.post(reverse('kripton:save-user-bank', kwargs={'provider_id': 1}), data=raw_bank_info)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {"error": 'Error al guardar la informacion, token de acceso invalido',
                               "error_code": "on_off_ramps.user.save_user_bank.errorProvider"}
#
