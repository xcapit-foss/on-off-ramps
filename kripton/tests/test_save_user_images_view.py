from unittest.mock import patch, Mock
from django.urls import reverse
from kripton.models import UserModel
from providers.kripton.clients import FakeKriptonClient
from providers.kripton.service import KriptonService
import pytest
from requests.exceptions import RetryError


@pytest.fixture
def raw_user_images():
    return {
        'front_document': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==',
        'back_document': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==',
        'dni_selfie': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==',
        'email': 'test@test.com',
        'user_id': '1',
        'auth_token': 'test'
    }


@pytest.mark.django_db
@patch('providers.service.ProvidersService.get_provider_client')
def test_save_user_images_view(mock_get_provider, raw_user_images, client, create_kripton_user):
    mock_get_provider.return_value = KriptonService(
        client=FakeKriptonClient(upload_image_response=Mock(status_code=200)))
    create_kripton_user(registration_status='USER_IMAGES')
    response = client.post(reverse('kripton:save-user-image', kwargs={'provider_id': '1'}), data=raw_user_images)
    assert response.status_code == 200
    assert response.json() == {'status': 'Ok', 'message': 'Imagen de usuario guardada con éxito'}
    assert UserModel.objects.get(email='test@test.com').registration_status == 'COMPLETE'


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.upload_image')
def test_save_user_images_view_unauthorized(mock_upload_image, request_response, raw_user_images, client,
                                            create_kripton_user):
    mock_upload_image.side_effect = [request_response(), request_response(), RetryError]
    create_kripton_user(registration_status='USER_IMAGES')
    response = client.post(reverse('kripton:save-user-image', kwargs={'provider_id': '1'}), data=raw_user_images)
    assert response.status_code == 401
    assert response.json() == {"error": 'Error al guardar las imagenes, token de accesso invalido',
                               "error_code": "on_off_ramps.user.save_images.errorProvider"}
