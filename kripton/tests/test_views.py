from unittest.mock import patch, mock_open
from django.urls import reverse
import pytest


@pytest.mark.django_db
@patch('kripton.views.PushNotification.send')
def test_kripton_notifications(
        mock_notification_send,
        client,
        kripton_update_operation_data,
        create_kripton_user,
):
    create_kripton_user()
    with patch('builtins.open', mock_open()) as mock_file:
        client.post(
            reverse('kripton:notifications'),
            data=kripton_update_operation_data(),
            content_type='application/json'
        )

        mock_file.assert_called_with('notifications.txt', 'a')
        mock_notification_send.assert_called()
