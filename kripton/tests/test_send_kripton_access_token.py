from kripton.kripton_access_token_response import KriptonAccessTokenResponse
from providers.kripton.clients import FakeKriptonClient
from rest_framework import status


def test_send_kripton_access_token(request_response):
    send_kripton_access_token = KriptonAccessTokenResponse(
        'test@test.com',
        FakeKriptonClient(request_login_token_response=request_response())
    )
    assert send_kripton_access_token


def test_send_kripton_access_token_value(request_response):
    send_kripton_access_token = KriptonAccessTokenResponse(
        'test@test.com',
        FakeKriptonClient(request_login_token_response=request_response())
    )
    response = send_kripton_access_token.value()
    assert response.json() == {}
    assert response.status_code == status.HTTP_200_OK
