from kripton.parsed_network import ParsedNetwork


def test_parsed_network():
    assert ParsedNetwork('MATIC')


def test_parsed_network_value():
    assert ParsedNetwork('MATIC').value() == 'Polygon'
