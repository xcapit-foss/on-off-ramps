from unittest.mock import patch
import pytest
from django.urls import reverse
from rest_framework import status


@pytest.fixture()
def refresh_token_data():
    return {'access_token': 'an_access_token',
            'refresh_token': 'a_refresh_token'}


@pytest.fixture()
def expected_refresh_token_data():
    return {'access_token': 'a_new_access_token',
            'refresh_token': 'a_new_refresh_token'}


@patch('providers.kripton.clients.DefaultKriptonClient.refresh_token')
def test_refresh_token_success(mock_kripton_refresh_token, client, request_response,
                               refresh_token_data, expected_refresh_token_data):
    mock_kripton_refresh_token.return_value = request_response(expected_refresh_token_data)
    response = client.post(reverse('kripton:refresh-token'), data=refresh_token_data)
    assert response.json() == expected_refresh_token_data
    assert response.status_code == status.HTTP_200_OK


@patch('providers.kripton.clients.DefaultKriptonClient.refresh_token')
def test_refresh_token_expired(mock_kripton_refresh_token, client, request_response,
                               refresh_token_data, expected_refresh_token_data):
    error_json = {"error": {
        "error_code": 101,
        "message": "Token invalid or expired"
    }
    }
    mock_kripton_refresh_token.return_value = request_response(
        error_json, status.HTTP_400_BAD_REQUEST)
    response = client.post(reverse('kripton:refresh-token'), data=refresh_token_data)
    assert response.json() == error_json
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_refresh_token_error(client, request_response, refresh_token_data, expected_refresh_token_data):
    response = client.post(reverse('kripton:refresh-token'), data={})
    assert response.json() == 'Error: bad access or refresh token'
    assert response.status_code == status.HTTP_400_BAD_REQUEST
