from kripton.kripton_available_currencies_of import KriptonAvailableCurrenciesOf
from providers.kripton.clients import FakeKriptonClient


def test_kripton_available_currencies_of():
    assert KriptonAvailableCurrenciesOf(FakeKriptonClient())


def test_kripton_available_currencies_of_value(expected_available_currencies_response,
                                               expected_kripton_available_currencies_of_value_response,
                                               request_response):
    kripton_available_currencies_of = KriptonAvailableCurrenciesOf(
        FakeKriptonClient(
            available_currencies_response=request_response({'data': expected_available_currencies_response})))
    expected_kripton_available_currencies_of_value_response.sort(key=lambda x: x['network'])
    value = kripton_available_currencies_of.value()
    value.sort(key=lambda x: x['network'])
    assert value == expected_kripton_available_currencies_of_value_response
