import pytest

from kripton.kripton_update import KriptonUpdate


def test_kripton_update(kripton_update_data):
    assert KriptonUpdate(kripton_update_data)


@pytest.mark.django_db
def test_kripton_update_user(kripton_update_data, create_kripton_user):
    kripton_user = create_kripton_user()
    assert KriptonUpdate(kripton_update_data).user() == kripton_user


@pytest.mark.django_db
def test_kripton_update_user_not_exists(kripton_update_data):
    assert KriptonUpdate(kripton_update_data).user() is None


@pytest.mark.django_db
def test_kripton_update_request_user_id(kripton_update_data, create_kripton_user):
    kripton_user = create_kripton_user()
    assert KriptonUpdate(kripton_update_data).request_user_id() == kripton_user.provider_user_id


@pytest.mark.django_db
def test_kripton_update_is_update(kripton_update_data, create_kripton_user):
    create_kripton_user()
    assert KriptonUpdate(kripton_update_data).is_update()


@pytest.mark.django_db
def test_kripton_update_is_cancel(kripton_update_operation_data, create_kripton_user):
    create_kripton_user()
    assert KriptonUpdate(kripton_update_operation_data(action='cancel')).is_cancel()


@pytest.mark.django_db
def test_kripton_update_is_buy(kripton_update_operation_data, create_kripton_user):
    create_kripton_user()
    assert KriptonUpdate(kripton_update_operation_data()).is_buy()


@pytest.mark.django_db
def test_kripton_update_is_sell(kripton_update_operation_data, create_kripton_user):
    create_kripton_user()
    assert KriptonUpdate(kripton_update_operation_data(operation_type='CashOut')).is_sell()


@pytest.mark.django_db
def test_kripton_update_is_operation(kripton_update_operation_data, create_kripton_user):
    create_kripton_user()
    assert KriptonUpdate(kripton_update_operation_data()).is_operation()


@pytest.mark.parametrize('resource', ['domicile', 'personal_information', 'fiscal_information'])
@pytest.mark.django_db
def test_kripton_update_is_kyc(resource, kripton_update_kyc_data, create_kripton_user):
    create_kripton_user()
    assert KriptonUpdate(kripton_update_kyc_data(resource=resource)).is_kyc()
