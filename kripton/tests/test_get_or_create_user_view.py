import pytest
from django.urls import reverse
from unittest.mock import patch
from rest_framework import status


@pytest.mark.django_db
@patch('providers.kripton.kripton_users.KriptonUsers.get_or_create_by')
def test_get_or_create_api_view(mock_get_or_create, client, test_kripton_user, expected_get_or_create_view_response):
    mock_get_or_create.return_value = test_kripton_user
    data = {'email': 'test@test.com', 'user_id': '4'}
    response = client.post(reverse('kripton:get-or-create-users', kwargs={'provider_id': '1'}), data=data)
    assert response.json() == expected_get_or_create_view_response
    assert response.status_code == status.HTTP_200_OK
