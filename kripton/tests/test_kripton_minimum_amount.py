import pytest
from kripton.kripton_minimum_amount import KriptonMinimumAmount
from providers.kripton.clients import FakeKriptonClient


@pytest.mark.django_db
def test_kripton_minimum_amount(test_kripton_user):
    assert KriptonMinimumAmount('cash-in', 'ars', 'USDC', 'MATIC', test_kripton_user, FakeKriptonClient())


@pytest.mark.django_db
def test_kripton_minimum_amount_value_fiat(get_user_limits_response_body_cash_in, request_response, test_kripton_user,
                                           calculate_amount_out_response_body_cash_in, get_quotations_response_body):
    get_user_limits_response = request_response(get_user_limits_response_body_cash_in)
    calculate_amount_out_response = request_response(calculate_amount_out_response_body_cash_in)
    get_quotations_response = request_response(get_quotations_response_body)

    assert KriptonMinimumAmount('cash-in', 'ars', 'USDC', 'MATIC', test_kripton_user,
                                FakeKriptonClient(get_user_limits_response=get_user_limits_response,
                                                  calculate_amount_out_response=calculate_amount_out_response,
                                                  get_quotations_response=get_quotations_response),
                                25.1, 0.0
                                ).value() == 13656.22


@pytest.mark.django_db
def test_kripton_minimum_amount_value_crypto(
        get_user_limits_response_body_cash_out,
        request_response, test_kripton_user,
        calculate_amount_out_response_body_cash_out, get_quotations_response_body):
    get_user_limits_response = request_response(get_user_limits_response_body_cash_out)
    calculate_amount_out_response = request_response(calculate_amount_out_response_body_cash_out)
    get_quotations_response = request_response(get_quotations_response_body)

    assert KriptonMinimumAmount('cash-out', 'USDC', 'ars', 'MATIC', test_kripton_user,
                                FakeKriptonClient(get_user_limits_response=get_user_limits_response,
                                                  calculate_amount_out_response=calculate_amount_out_response,
                                                  get_quotations_response=get_quotations_response),
                                0.0, 25.1
                                ).value() == 25.1
