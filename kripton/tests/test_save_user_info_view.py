import pytest
from unittest.mock import patch, Mock
from django.http import QueryDict
from django.urls import reverse
from rest_framework import status
from requests.exceptions import RetryError


@pytest.fixture
def raw_user_info():
    return {
        'provider': 1,
        'birthday': '02/25/1970',
        'first_name': 'Test',
        'last_name': 'Test',
        'gender': 'Hombre',
        'marital_status': 'single',
        'nationality': 'Argentino',
        'politically_exposed': False,
        'document_type': 'dni',
        'document_number': '33333333',
        'postal_code': '5000',
        'city': 'Córdoba',
        'street_address': 'Bv. Chacabuco',
        'street_number': '500',
        'telephone_number': '3333444444',
        'email': 'test@test.com',
        'user_id': 1,
        'auth_token': 'test'
    }


@pytest.fixture
def expected_parsed_data():
    raw_data = {
        'provider': '1',
        'birthday': '1970/02/25',
        'first_name': 'Test',
        'last_name': 'Test',
        'gender': 'Hombre',
        'marital_status': 'single',
        'nationality': 'Argentino',
        'politically_exposed': 'False',
        'document_type': 'dni',
        'document_number': '33333333',
        'postal_code': '5000',
        'city': 'Córdoba',
        'street_address': 'Bv. Chacabuco',
        'street_number': '500',
        'telephone_number': '3333444444',
    }
    query_dict = QueryDict('', mutable=True)
    query_dict.update(raw_data)
    return query_dict


@pytest.mark.django_db
@patch('providers.service.ProvidersService.get_provider_client')
def test_save_user_info_view(mock_get_provider_client, client, raw_user_info, create_kripton_user,
                             expected_parsed_data):
    create_kripton_user()
    mock_kripton_service = Mock()
    mock_kripton_service.save_user_information.return_value = {'status': 'Ok',
                                                               'message': 'Información de usuario guardada con éxito'}
    mock_get_provider_client.return_value = mock_kripton_service

    response = client.post(reverse('kripton:save-user-info', kwargs={'provider_id': 1}), data=raw_user_info)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'status': 'Ok', 'message': 'Información de usuario guardada con éxito'}

    mock_kripton_service.save_user_information.assert_called_with(provider_user_id=9999, data=expected_parsed_data,
                                                                  auth_token='test')


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.register_user_information')
def test_save_user_info_view_user_information_unauthorized(mock_register_user_information, client, raw_user_info,
                                                           create_kripton_user,
                                                           expected_parsed_data):
    create_kripton_user()
    mock_register_user_information.side_effect = RetryError

    response = client.post(reverse('kripton:save-user-info', kwargs={'provider_id': 1}), data=raw_user_info)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {"error": 'Error al guardar la informacion, token de acceso invalido',
                               "error_code": "on_off_ramps.user.save_info.errorProvider"}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.register_user_information')
@patch('providers.kripton.clients.DefaultKriptonClient.save_user_physical_address')
def test_save_user_info_view_physical_address_unauthorized(mock_save_user_physical_address,
                                                           mock_register_user_information, client, raw_user_info,
                                                           create_kripton_user,
                                                           expected_parsed_data, request_response):
    create_kripton_user()
    mock_register_user_information.return_value = request_response()
    mock_save_user_physical_address.side_effect = RetryError

    response = client.post(reverse('kripton:save-user-info', kwargs={'provider_id': 1}), data=raw_user_info)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {"error": 'Error al guardar la informacion, token de acceso invalido',
                               "error_code": "on_off_ramps.user.save_info.errorProvider"}
