import pytest
from django.urls import reverse
from unittest.mock import patch


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_quotations')
@patch('providers.kripton.clients.DefaultKriptonClient.calculate_amount_out')
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_success_cash_in(mock_get_user_limits, mock_calculate_amount_out, mock_get_quotations,
                                              client, create_kripton_user, request_response,
                                              get_user_limits_response_body_cash_in,
                                              calculate_amount_out_response_body_cash_in, get_quotations_response_body,
                                              set_cash_out_minimum, expected_user_limits_with_local_cash_in_minimum):
    create_kripton_user()
    mock_get_user_limits.return_value = request_response(get_user_limits_response_body_cash_in)
    mock_calculate_amount_out.return_value = request_response(calculate_amount_out_response_body_cash_in)
    mock_get_quotations.return_value = request_response(get_quotations_response_body)
    data = {'email': 'test@test.com',
            'operation_type': 'cash-in',
            'user_id': '1',
            'currency_in': 'ars',
            'currency_out': 'USDC',
            'network_out': 'MATIC'}
    response = client.post(reverse('kripton:get-user-limits'), data=data)
    assert response.json() == expected_user_limits_with_local_cash_in_minimum
    assert response.status_code == 200


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_quotations')
@patch('providers.kripton.clients.DefaultKriptonClient.calculate_amount_out')
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_success_cash_out(mock_get_user_limits, mock_calculate_amount_out, mock_get_quotations,
                                               client, create_kripton_user, request_response,
                                               get_user_limits_response_body_cash_out, get_quotations_response_body,
                                               calculate_amount_out_response_body_cash_out,
                                               expected_user_limits_with_local_cash_out_minimum, set_cash_out_minimum):
    create_kripton_user()
    mock_get_user_limits.return_value = request_response(get_user_limits_response_body_cash_out)
    mock_calculate_amount_out.return_value = request_response(calculate_amount_out_response_body_cash_out)
    mock_get_quotations.return_value = request_response(get_quotations_response_body)
    data = {'email': 'test@test.com',
            'operation_type': 'cash-out',
            'user_id': '1',
            'currency_in': 'USDC',
            'currency_out': 'ars',
            'network_out': 'MATIC'}
    response = client.post(reverse('kripton:get-user-limits'), data=data)
    assert response.json() == expected_user_limits_with_local_cash_out_minimum
    assert response.status_code == 200


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_missing_data(mock_get_user_limits, client, create_kripton_user, request_response,
                                           get_user_limits_response_body_cash_in):
    create_kripton_user()
    mock_get_user_limits.return_value = request_response(get_user_limits_response_body_cash_in)
    response = client.post(reverse('kripton:get-user-limits'), data={})
    assert response.json() == {"error": 'Email is required', 'error_code': 'on_off_ramps.kripton.user_limit.error'}
    assert response.status_code == 400


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_limits')
def test_get_user_limits_view_invalid(mock_get_user_limits, client, create_kripton_user, request_response):
    create_kripton_user()
    mock_get_user_limits.return_value = request_response({}, 400)
    response = client.post(reverse('kripton:get-user-limits'), data={'email': 'test@test.com'})
    assert response.status_code == 400
    assert response.json() == {"error": 'Error al obtener los limites desde proveedor',
                               'error_code': 'on_off_ramps.kripton.user_limit.errorProvider'}
