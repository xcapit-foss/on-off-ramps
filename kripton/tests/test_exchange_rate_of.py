from kripton.exceptions import KriptonExchangeRateNotFound
from providers.kripton.clients import FakeKriptonClient
from kripton.exchange_rate_of import ExchangeRateOf
import pytest


def test_exchange_rate_of_new():
    assert ExchangeRateOf('USDC', 'ARS', FakeKriptonClient())


def test_exchange_rate_of_value(get_quotations_response_body, request_response):
    get_quotations_response = request_response(get_quotations_response_body)
    exchange_rate_of = ExchangeRateOf('USDC',
                                      'ARS',
                                      FakeKriptonClient(get_quotations_response=get_quotations_response))
    assert exchange_rate_of.value() == 509.0


def test_exchange_rate_of_throw_error_when_currency_is_not_found(get_quotations_response_body, request_response):
    get_quotations_response = request_response(get_quotations_response_body)
    with pytest.raises(KriptonExchangeRateNotFound):
        ExchangeRateOf('MANA',
                       'ARS',
                       FakeKriptonClient(get_quotations_response=get_quotations_response)).value()
