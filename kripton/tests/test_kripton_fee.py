from kripton.kripton_fee import KriptonFee


def test_kripton_fee_new():
    assert KriptonFee({})


def test_kripton_fee_network_fee(calculate_amount_out_response_body_cash_in):
    kripton_fee = KriptonFee(calculate_amount_out_response_body_cash_in)
    assert kripton_fee.network_fee() == 0.6


def test_kripton_fee_service_fee_percentage(calculate_amount_out_response_body_cash_in):
    kripton_fee = KriptonFee(calculate_amount_out_response_body_cash_in)
    assert kripton_fee.service_fee_percentage() == 4.5
