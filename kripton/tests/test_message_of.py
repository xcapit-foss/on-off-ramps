import pytest

from unittest.mock import Mock
from kripton.kripton_update import KriptonUpdate
from kripton.message_of import MessageOf
from providers.kripton.clients import FakeKriptonClient


def test_message_of(kripton_update_data):
    assert MessageOf(KriptonUpdate(kripton_update_data), FakeKriptonClient())


def test_message_of_title_kyc_cancel(kripton_update_kyc_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_kyc_data('cancel')),
        FakeKriptonClient()
    ).title() == 'No pudimos validar tu identidad 😔'


def test_message_of_title_kyc(kripton_update_kyc_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_kyc_data()),
        FakeKriptonClient(check_user_with_id_response=Mock(status_code=200, json=lambda: {"kyc_approved": True}))
    ).title() == '¡Tu validación de identidad está aprobada!'


def test_message_of_title_operation_cash_in(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data()),
        FakeKriptonClient()
    ).title() == '¡Se acreditó tu compra!'


def test_message_of_title_operation_cash_in_cancel(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data(action='cancel')),
        FakeKriptonClient()
    ).title() == 'No pudimos procesar tu compra 😔'


def test_message_of_title_operation_cash_out(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data(operation_type='CashOut')),
        FakeKriptonClient()
    ).title() == '¡Se acreditó tu venta!'


def test_message_of_title_operation_cash_out_cancel(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data(action='cancel', operation_type='CashOut')),
        FakeKriptonClient()
    ).title() == 'No pudimos procesar tu venta 😔'


def test_message_of_body_operation_cash_in_cancel(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data('cancel')),
        FakeKriptonClient()
    ).body() == 'Tu compra no pudo ser completada\nSi necesitas ayuda, puedes contactarnos de inmediato'


def test_message_of_body_operation_cash_out_cancel(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data(action='cancel', operation_type='CashOut')),
        FakeKriptonClient()
    ).body() == 'Tu venta no pudo ser completada\nSi necesitas ayuda, puedes contactarnos de inmediato'


def test_message_of_body_operation_cash_in_update(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data()),
        FakeKriptonClient()
    ).body() == '¡Tu compra ha sido aprobada ahora podes confeccionar tu garantía!'


def test_message_of_body_operation_cash_out_update(kripton_update_operation_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data(operation_type='CashOut')),
        FakeKriptonClient()
    ).body() == '¡Tu venta ha sido aprobada!'


def test_message_of_body_kyc_cancel(kripton_update_kyc_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_kyc_data('cancel')),
        FakeKriptonClient()
    ).body() == 'Tu validación de identidad no pudo completarse\nSi necesitas ayuda, puedes contactarnos de inmediato'


def test_message_of_body_kyc_update(kripton_update_kyc_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_kyc_data('update')),
        FakeKriptonClient(check_user_with_id_response=Mock(status_code=200, json=lambda: {"kyc_approved": True}))
    ).body() == '¡Ya podes comprar criptomonedas para confeccionar tu garantía de respaldo!\nSi necesitas ayuda, no dudes en contactarnos'


def test_message_of_body_kyc_update_not_approve(kripton_update_kyc_data):
    assert MessageOf(
        KriptonUpdate(kripton_update_kyc_data('update')),
        FakeKriptonClient(check_user_with_id_response=Mock(status_code=200, json=lambda: {"kyc_approved": False}))
    ).body() is None


@pytest.mark.django_db
def test_message_of_receiver(kripton_update_operation_data, create_kripton_user):
    kripton_user = create_kripton_user()
    assert MessageOf(
        KriptonUpdate(kripton_update_operation_data()),
        FakeKriptonClient()
    ).receiver() == kripton_user
