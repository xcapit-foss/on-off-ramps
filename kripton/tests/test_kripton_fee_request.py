from kripton.kripton_fee import KriptonFee
from kripton.kripton_fee_request import KriptonFeeRequest
from providers.kripton.clients import FakeKriptonClient


def test_kripton_fee_request_new():
    assert KriptonFeeRequest('cash-in', 'ars', 'USDC', 'polygon', FakeKriptonClient())


def test_kripton_fee_request_response(calculate_amount_out_response_body_cash_in, request_response):
    response = request_response(calculate_amount_out_response_body_cash_in)
    kripton_fee = KriptonFeeRequest('cash-in', 'ars', 'USDC', 'polygon',
                                    FakeKriptonClient(calculate_amount_out_response=response)).response()
    assert isinstance(kripton_fee, KriptonFee)
