from unittest.mock import patch
from django.urls import reverse
from rest_framework import status


@patch('kripton.kripton_login.KriptonLogin.value')
def test_login_view(mock_kripton_login, client, request_response, expected_login_response):
    mock_kripton_login.return_value = expected_login_response
    response = client.post(reverse('kripton:login'), data={'email': 'test@test.com', 'token': '123456'})
    assert response.json() == expected_login_response.json()
    assert response.status_code == status.HTTP_200_OK
