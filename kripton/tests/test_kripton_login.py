from kripton.kripton_login import KriptonLogin
from providers.kripton.clients import FakeKriptonClient


def test_kripton_login():
    assert KriptonLogin('test@test.com', '123456', FakeKriptonClient())


def test_kripton_login_value(expected_login_response):
    kripton_login = KriptonLogin('test@test.com', '123456', FakeKriptonClient(login_response=expected_login_response))
    assert ['token', 'refresh_token'] == list(kripton_login.value().json().keys())
