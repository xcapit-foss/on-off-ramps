import pytest
from django.urls import reverse
from unittest.mock import patch
from requests.exceptions import RetryError
from rest_framework import status


@pytest.fixture
def raw_data():
    return {
        'auth_token': 'test',
        'email': 'test@test.com',
        'user_id': '1',
        'payment_method_id': 100,
    }


@pytest.fixture
def expected_response():
    return {
        "id": 100,
        "country": "ARG",
        "currency_id": "ars",
        "name": "Santander",
        "account_type": "Ahorro",
        "account_number": "**************0990"
    }


@pytest.fixture
def kripton_banks_response():
    return {
        "data": [
            {
                "id": 100,
                "country": "ARG",
                "currency_id": "ars",
                "name": "Santander",
                "account_type": "Ahorro",
                "account_number": "**************0990"
            },
            {
                "id": 101,
                "country": "ARG",
                "currency_id": "ars",
                "name": "Santander",
                "account_type": "Ahorro",
                "account_number": "**************1234"
            }, {
                "id": 102,
                "country": "ARG",
                "currency_id": "ars",
                "name": "Santander",
                "account_type": "Ahorro",
                "account_number": "**************9876"
            }
        ]
    }


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_banks')
def test_get_user_bank_view_success(
        mock_get_user_banks, request_response, client,
        raw_data, expected_response, kripton_banks_response, create_kripton_user):
    create_kripton_user()
    mock_get_user_banks.return_value = request_response(json_data=kripton_banks_response)
    response = client.post(reverse('kripton:get-user-bank', kwargs={'provider_id': '1'}), data=raw_data)
    assert response.json() == expected_response
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_banks')
def test_get_user_bank_view_unauthorized(mock_get_user_banks, client, raw_data,
                                         create_kripton_user):
    create_kripton_user()
    mock_get_user_banks.side_effect = RetryError
    response = client.post(reverse('kripton:get-user-bank', kwargs={'provider_id': '1'}), data=raw_data)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {
        "error": 'Error al obtener la información bancaria del usuario, token de accesso invalido',
        "error_code": "on_off_ramps.user.get_user_bank.errorProvider"}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_user_banks')
def test_get_user_bank_view_incorrect_payment_id(mock_get_user_banks, client, raw_data, request_response,
                                                 kripton_banks_response, create_kripton_user):
    create_kripton_user()
    mock_get_user_banks.return_value = request_response(json_data=kripton_banks_response)
    response = client.post(reverse('kripton:get-user-bank', kwargs={'provider_id': '1'}), data={
        'auth_token': 'test',
        'email': 'test@test.com',
        'user_id': '1',
        'payment_method_id': 2
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"error": "No se ha encontrado el metodo de pago solicitado"}
