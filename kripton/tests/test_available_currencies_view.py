import pytest
from django.urls import reverse
from unittest.mock import patch
from rest_framework import status


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.available_currencies')
def test_available_currencies_view(mock_client_available_currencies, client, expected_available_currencies_response,
                                   expected_kripton_available_currencies_of_value_response,
                                   request_response):
    mock_client_available_currencies.return_value = request_response({"data": expected_available_currencies_response})
    response = client.get(reverse('kripton:available-currencies'))
    expected_kripton_available_currencies_of_value_response.sort(key=lambda x: x['network'])
    json_response = response.json()
    json_response.sort(key=lambda x: x['network'])
    assert json_response == expected_kripton_available_currencies_of_value_response
    assert response.status_code == status.HTTP_200_OK
