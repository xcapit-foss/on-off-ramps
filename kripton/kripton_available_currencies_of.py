from providers.kripton.clients import DefaultKriptonClient, KriptonClient


class KriptonAvailableCurrenciesOf:
    def __init__(self, client: KriptonClient = DefaultKriptonClient()):
        self._client = client

    def value(self):
        available_networks = ['RSK', 'BSC', 'ETH', 'POLYGON']
        data = self._client.available_currencies().json().get('data')
        network = set([item.get('crypto_network') for item in data])
        result = [{'network': item.upper(), 'currencies': []} for item in network if item.upper() in available_networks]
        for result_item in result:
            for item in data:
                if self._is_equal_network(result_item['network'],
                                          item['crypto_network'].upper()) and self._currency_not_added(
                        item['currency_id'].upper(), result_item['currencies']):
                    result_item['currencies'].append(item['currency_id'].upper())

        result = self._replace_network(result, "POLYGON", "MATIC")
        result = self._replace_network(result, "ETH", "ERC20")
        return result

    @staticmethod
    def _replace_network(result, network_to_replace, replacement_network):
        for item in result:
            item.update((network, replacement_network) for network, network_name in item.items() if
                        network_name == network_to_replace)
        return result

    @staticmethod
    def _currency_not_added(currency, currencies) -> bool:
        return currency not in currencies

    @staticmethod
    def _is_equal_network(network_in_result, network_in_data) -> bool:
        return network_in_result == network_in_data
