class KriptonFee:
    def __init__(self, a_calculate_amount_out_response: dict):
        self._raw_data = a_calculate_amount_out_response

    def network_fee(self) -> float:
        return float(self._raw_data.get('data').get('fee_of_network'))

    def service_fee_percentage(self) -> float:
        return self._commissions_percentage() + self._taxes_percentage()

    def _commissions_percentage(self) -> float:
        return self._raw_data.get('data').get('commissions').get('percentage')

    def _taxes_percentage(self) -> float:
        return self._raw_data.get('data').get('taxes').get('percentage')
