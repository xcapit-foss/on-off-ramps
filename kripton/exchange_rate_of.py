from kripton.exceptions import KriptonExchangeRateNotFound
from providers.kripton.clients import KriptonClient


class ExchangeRateOf:
    def __init__(self, a_crypto_currency: str, a_fiat_currency: str, a_client: KriptonClient):
        self._crypto_currency = a_crypto_currency
        self._fiat_currency = a_fiat_currency
        self._client = a_client

    def value(self) -> float:
        return self._crypto_exchange_rates().get("market_price").get(self._fiat_currency.lower())

    def _all_exchanges_rates(self) -> dict:
        return self._client.get_quotations().json().get("data")

    def _crypto_exchange_rates(self) -> dict:
        crypto_exchange_rates = list(
            filter(lambda x: x.get("currency") == self._crypto_currency.lower(), self._all_exchanges_rates()))
        if len(crypto_exchange_rates) == 0:
            raise KriptonExchangeRateNotFound(self._crypto_currency)
        return crypto_exchange_rates[0]
