from functools import cached_property

from kripton.kripton_update import KriptonUpdate
from kripton.message_data import DefaultMessageData, NullMessageData, MessageData
from kripton.models import UserModel
from providers.kripton.clients import KriptonClient, DefaultKriptonClient


class MessageOf:
    def __init__(self, kripton_update: KriptonUpdate, kripton_client: KriptonClient = DefaultKriptonClient()):
        self._kripton_update = kripton_update
        self._kripton_client = kripton_client

    def title(self) -> str:
        return self._message_data().title()

    def body(self) -> str:
        return self._message_data().body()

    def receiver(self) -> UserModel:
        return self._kripton_update.user()

    def _message_data(self) -> MessageData:
        result = NullMessageData()
        if self._kripton_update.is_operation() and self._kripton_update.is_cancel() and self._kripton_update.is_buy():
            result = DefaultMessageData(
                'No pudimos procesar tu compra 😔',
                'Tu compra no pudo ser completada\nSi necesitas ayuda, puedes contactarnos de inmediato'
            )
        elif self._kripton_update.is_operation() and self._kripton_update.is_update() and self._kripton_update.is_buy():
            result = DefaultMessageData(
                '¡Se acreditó tu compra!',
                '¡Tu compra ha sido aprobada ahora podes confeccionar tu garantía!'
            )
        elif self._kripton_update.is_operation() and self._kripton_update.is_cancel() and self._kripton_update.is_sell():
            result = DefaultMessageData(
                'No pudimos procesar tu venta 😔',
                'Tu venta no pudo ser completada\nSi necesitas ayuda, puedes contactarnos de inmediato'
            )
        elif self._kripton_update.is_operation() and self._kripton_update.is_update() and self._kripton_update.is_sell():
            result = DefaultMessageData(
                '¡Se acreditó tu venta!',
                '¡Tu venta ha sido aprobada!'
            )
        elif self._kripton_update.is_kyc() and self._kripton_update.is_cancel():
            result = DefaultMessageData(
                'No pudimos validar tu identidad 😔',
                'Tu validación de identidad no pudo completarse\nSi necesitas ayuda, puedes contactarnos de inmediato'
            )
        elif self._kripton_update.is_kyc() and self._kripton_update.is_update() and self._is_kyc_approved:
            result = DefaultMessageData(
                '¡Tu validación de identidad está aprobada!',
                '¡Ya podes comprar criptomonedas para confeccionar tu garantía de respaldo!\nSi necesitas ayuda, no dudes en contactarnos'
            )
        return result

    @cached_property
    def _is_kyc_approved(self):
        return self._kripton_client.check_user_with_id(self._kripton_update.request_user_id()).json().get(
            'kyc_approved', False)
