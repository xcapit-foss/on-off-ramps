class ParsedNetwork:

    def __init__(self, network: str):
        self._network = network

    def value(self):
        networks = {
            'ERC20': 'ETH',
            'MATIC': 'Polygon',
            'BSC_BEP20': 'BSC',
            'RSK': 'RSK'
        }
        return networks[self._network]
