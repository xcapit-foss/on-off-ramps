from kripton.kripton_fee import KriptonFee
from providers.kripton.clients import KriptonClient


class KriptonFeeRequest:
    def __init__(self, an_operation_type: str, a_currency_in: str, a_currency_out: str,
                 a_network: str, a_kripton_client: KriptonClient):
        self._operation_type = an_operation_type
        self._currency_in = a_currency_in
        self._currency_out = a_currency_out
        self._network = a_network
        self._client = a_kripton_client

    def response(self) -> KriptonFee:
        response = self._client.calculate_amount_out(self._operation_type, self._currency_in,
                                                     self._currency_out, self._network)
        return KriptonFee(response.json())
