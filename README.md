# Xcapit On-Off-Ramps Service

The Xcapit On-Off-Ramps service is a rest API that manages cryptocurrency purchases from various third-party providers.

## Community

- [Discord](https://discord.com/invite/2VRMzMhvjX)
- [Project Charter](https://xcapit-foss.gitlab.io/documentation/docs/project_charter)
- [Code of Conduct](https://xcapit-foss.gitlab.io/documentation/docs/CODE_OF_CONDUCT)
- [Contribution Guideline](https://xcapit-foss.gitlab.io/documentation/docs/contribution_guidelines)

## Getting Started

[Getting started](https://xcapit-foss.gitlab.io/documentation/docs/on-off-ramps-service/getting_started)
