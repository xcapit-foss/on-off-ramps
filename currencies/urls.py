from django.urls import path
from currencies.views import GetCurrenciesView, GetQuotationsView


app_name = "currencies"

urlpatterns = [
    path('provider/<provider_id>', GetCurrenciesView.as_view(), name='get-all-currencies'),
    path('provider/<provider_id>/quotations', GetQuotationsView.as_view(), name='get-quotations')
]
