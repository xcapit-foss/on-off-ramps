import pytest
from django.urls import reverse
from unittest.mock import patch, Mock
from providers.service import ProvidersService, GetProviderError

ALL_CURRENCIES = {
    "data": [
        {
            "id": "usd",
            "name": "usd",
            "created_at": "2021-02-07T00:04:29.549Z",
            "updated_at": "2021-02-07T00:04:29.549Z",
            "fiat": True
        },
        {
            "id": "dai",
            "name": "dai",
            "created_at": "2021-02-08T00:30:06.773Z",
            "updated_at": "2021-02-08T00:30:06.773Z",
            "fiat": False
        },
        {
            "id": "usdt",
            "name": "usdt",
            "created_at": "2021-02-08T00:30:08.224Z",
            "updated_at": "2021-02-08T00:30:08.224Z",
            "fiat": False
        }
    ]
}

ALL_QUOTATIONS = [
    {
        "currency": "dai",
        "market_price": {
            "ars": "156.00",
            "usd": "1.00"
        },
        "quotation": {
            "ars": {
                "buy": "151.32",
                "sell": "160.68"
            },
            "usd": {
                "buy": "0.97",
                "sell": "1.03"
            }
        }
    },
    {
        "currency": "usdt",
        "market_price": {
            "ars": "156.00",
            "usd": "1.00"
        },
        "quotation": {
            "ars": {
                "buy": "151.32",
                "sell": "160.68"
            },
            "usd": {
                "buy": "0.97",
                "sell": "1.03"
            }
        }
    }
]

RESP_QUOTATIONS = {
    "status": "ok",
    "data": ALL_QUOTATIONS
}

USDT_QUOTATION = [
    {
        "currency": "usdt",
        "market_price": {
            "ars": "156.00",
            "usd": "1.00"
        },
        "quotation": {
            "ars": {
                "buy": "151.32",
                "sell": "160.68"
            },
            "usd": {
                "buy": "0.97",
                "sell": "1.03"
            }
        }
    }
]


@pytest.fixture
def get_request(client):
    def p(url, reverse_kargs=(), params=()):
        return client.get(f'{reverse(url, kwargs=reverse_kargs)}', params=params)

    return p


@patch('providers.kripton.service.KriptonService.get_currencies')
def test_get_all_currencies(get_mock, get_request):
    provider_id = {'provider_id': "1"}
    get_mock.return_value = ALL_CURRENCIES

    url = 'currencies:get-all-currencies'

    response = get_request(url, reverse_kargs=provider_id)

    assert response
    assert response.json() == ALL_CURRENCIES


@patch('providers.kripton.service.KriptonService.get_currencies')
def test_get_currencies_should_a_400_error_when_provider_doesnot_exist(get_mock, get_request):
    provider_id = {'provider_id': "3123"}
    get_mock.return_value.json.return_value = {}
    get_mock.return_value.status_code = 400

    url = 'currencies:get-all-currencies'

    response = get_request(url, reverse_kargs=provider_id)

    assert response
    assert response.status_code == 400


@pytest.mark.parametrize('currency, expected_result', [
    [None, ALL_QUOTATIONS],
    ['usdt', USDT_QUOTATION]
])
@patch('providers.kripton.service.KriptonService.get_quotations')
def test_get_quotations(get_mock, currency, expected_result, get_request):
    provider_id = {'provider_id': "1"}
    params = {
        'currency': currency
    }

    get_mock.return_value = expected_result

    url = 'currencies:get-quotations'

    response = get_request(url, reverse_kargs=provider_id, params=params)

    assert response
    assert response.json() == expected_result


@patch('providers.kripton.service.KriptonService.get_quotations')
def test_get_quotations_should_a_400_error_when_provider_doesnot_exist(get_mock, get_request):
    provider_id = {'provider_id': "3123"}
    get_mock.return_value.json.return_value = {}
    get_mock.return_value.status_code = 400

    url = 'currencies:get-quotations'

    response = get_request(url, reverse_kargs=provider_id)

    assert response
    assert response.status_code == 400
