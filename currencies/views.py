from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from providers.service import ProvidersService, GetProviderError
from providers.kripton.service import CustomError
from requests.exceptions import RetryError


class GetCurrenciesView(APIView):

    def get(self, request, *args, **kwargs):
        try:
            provider_service = ProvidersService()
            provider = provider_service.get_provider_client(provider_id=kwargs['provider_id'])

            currencies = provider.get_currencies()

            response = Response(currencies, status=status.HTTP_200_OK)

        except (GetProviderError, CustomError, RetryError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.currencies.errorProvider'
        finally:
            return response


class GetQuotationsView(APIView):

    def get(self, request, *args, **kwargs):
        try:
            provider_service = ProvidersService()
            provider = provider_service.get_provider_client(provider_id=kwargs['provider_id'])

            currency = self.request.query_params.get('currency', None)

            quotations = provider.get_quotations(currency=currency)

            response = Response(quotations, status=status.HTTP_200_OK)

        except (GetProviderError, CustomError, RetryError) as e:
            response = Response({"error": str(e)}, status.HTTP_400_BAD_REQUEST)
            response.data['error_code'] = 'on_off_ramps.currencies.errorProvider'
        finally:
            return response
