from rest_framework import serializers


class PushNotificationsSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100, required=True, allow_blank=False, allow_null=False)
    message = serializers.CharField(max_length=400, required=True, allow_blank=False, allow_null=False)

    def create(self, validated_data):
        """"""

    def update(self, instance, validated_data):
        """"""