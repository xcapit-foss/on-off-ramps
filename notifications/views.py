from push_notifications.api.rest_framework import DeviceViewSetMixin, GCMDeviceSerializer
from push_notifications.models import GCMDevice
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status


class GCMDeviceViewSet(DeviceViewSetMixin, ModelViewSet):
    queryset = GCMDevice.objects.all()
    serializer_class = GCMDeviceSerializer


class ToggleUserNotificationsView(APIView):
    def put(self, request, user_id: int, active: bool):
        GCMDevice.objects.filter(name=str(user_id)).update(active=active)
        return Response({'user_id': user_id, 'active': active}, status=status.HTTP_200_OK)
