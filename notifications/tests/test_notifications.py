from unittest.mock import Mock
from notifications.channels import Channel, PushChannel
from notifications.notifications import PushNotification, FakePushNotification, NullNotification
from notifications.devices import FakeDevice


def test_new_push_notification():
    assert PushNotification(message="", channel=Mock(spec=Channel))


def test_push_notification_send():
    notification = PushNotification(
        message="some text",
        channel=PushChannel(FakeDevice())
    )

    assert notification.send()


def test_fake_push_notification():
    assert FakePushNotification(
        title='some title',
        message="some text",
        channel=PushChannel(FakeDevice())
    )


def test_fake_push_notification_send():
    assert FakePushNotification(
        title='some title',
        message="some text",
        channel=PushChannel(FakeDevice())
    ).send() is None


def test_null_notification():
    assert NullNotification()


def test_null_notification_send():
    assert NullNotification().send()
