import pytest
from push_notifications.models import GCMDevice


@pytest.fixture
def user_gcm_devices():
    def ugd(user_id):
        GCMDevice.objects.create(name=user_id, active=True, registration_id='test1')
        GCMDevice.objects.create(name=user_id, active=False, registration_id='test2')
        GCMDevice.objects.create(name=user_id, active=True, registration_id='test3')
        GCMDevice.objects.create(name=user_id, active=False, registration_id='test4')

    return ugd
