from unittest.mock import Mock
from push_notifications.models import GCMDevice as GCMDeviceModel
from notifications.devices import FCMDevice, FakeDevice


def test_new_default_device():
    assert FCMDevice(Mock(spec=GCMDeviceModel))


def test_new_default_device_send_message():
    message = 'some text'
    title = 'a title'
    mock_device_model = Mock(spec=GCMDeviceModel)

    FCMDevice(mock_device_model).send_message(message=message, title=title)

    mock_device_model.send_message.assert_called_with(message=message, title=title)


def test_fake_device():
    assert FakeDevice()
    assert FakeDevice().send_message(message="asdf")
