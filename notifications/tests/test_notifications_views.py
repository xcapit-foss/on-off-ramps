import pytest
from django.urls import reverse
from push_notifications.models import GCMDevice


@pytest.fixture
def device_data():
    return {
        "id": '1',
        "name": 'a_user_id',
        "registration_id": '1',
        "device_id": '1',
        "active": True,
        "cloud_message_type": 'FCM',
        "application_id": '1',
    }


@pytest.mark.django_db
def test_create_fcm_view(client, device_data):
    response = client.post(reverse('notifications:create-fcm-device'), data=device_data)
    assert response.status_code == 201


@pytest.mark.django_db
def test_toggle_user_notifications_enable(client, user_gcm_devices):
    user_gcm_devices(user_id=1)
    client.put(reverse('notifications:toggle-user-notifications', kwargs={'user_id': 1, 'active': True}))
    for active in GCMDevice.objects.filter(name='1').values_list('active', flat=True):
        assert active


@pytest.mark.django_db
def test_toggle_user_notifications_disable(client, user_gcm_devices):
    user_gcm_devices(user_id=1)
    client.put(reverse('notifications:toggle-user-notifications', kwargs={'user_id': 1, 'active': False}))
    for active in GCMDevice.objects.filter(name='1').values_list('active', flat=True):
        assert active is False
