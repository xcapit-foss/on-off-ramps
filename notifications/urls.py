from django.urls import path
from notifications.views import GCMDeviceViewSet, ToggleUserNotificationsView

app_name = "notifications"

urlpatterns = [
    path('device/fcm', GCMDeviceViewSet.as_view({'post': 'create'}), name='create-fcm-device'),
    path('user/<user_id>/toggle/<active>', ToggleUserNotificationsView.as_view(), name='toggle-user-notifications'),
]
