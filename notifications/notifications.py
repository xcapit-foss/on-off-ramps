from abc import ABC, abstractmethod
from notifications.channels import Channel


class Notification(ABC):

    @abstractmethod
    def send(self, *args, **kwargs):
        """"""


class FakePushNotification(Notification):
    def __init__(self, message: str, channel: Channel, **kwargs):
        """"""

    def send(self):
        """"""


class PushNotification(Notification):

    def __init__(self, message: str, channel: Channel, **kwargs):
        self._channel = channel
        self._message = message
        self._kwargs = kwargs

    def send(self, *args, **kwargs):
        return self._channel.send(message=self._message, **self._kwargs)


class NullNotification(Notification):

    def send(self):
        return True