from rest_framework import serializers


class GetPaxfulLinkSerializer(serializers.Serializer):
    wallet = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, required=True)
    user = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, required=True)