import pytest
from django.urls import reverse
from rest_framework import status


@pytest.mark.parametrize('wallet, user_id, expected_status, expected_response', [
    ['estaesunawallet', "150", status.HTTP_200_OK, None],
    ['estaotratambien', "175", status.HTTP_200_OK, None],
    ['', "231", status.HTTP_400_BAD_REQUEST, {'wallet': ['This field may not be blank.']}],
    ['otrawalletvalida', '', status.HTTP_400_BAD_REQUEST, {'user': ['This field may not be blank.']}]
])
def test_get_paxful_link(client, wallet, user_id, expected_status, expected_response):
    data = {
        "wallet": wallet,
        "user": user_id
    }
    response = client.post(reverse('paxful:get-paxful-link'), data=data)
    data = response.json()

    assert response.status_code == expected_status
    if response.status_code == 200:
        assert 'url' in data
    else:
        assert data == expected_response
