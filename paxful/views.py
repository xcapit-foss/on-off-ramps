from rest_framework.views import APIView
from providers.service import ProvidersService
from rest_framework.response import Response
from rest_framework import status
from .serializers import GetPaxfulLinkSerializer
from providers.enums import ProvidersEnum


class GetPaxfulLinkAPIView(APIView):

    provider_service = ProvidersService()
    serializer_class = GetPaxfulLinkSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        wallet = serializer.data.get('wallet')
        user_id = serializer.data.get('user')

        paxful_provider_id = ProvidersEnum.paxful.value
        provider_client = self.provider_service.get_provider_client(provider_id=paxful_provider_id)
        result = provider_client.get_link(wallet, user_id)

        response = Response(result, status=status.HTTP_200_OK)
        return response
