from django.apps import AppConfig


class PaxfulConfig(AppConfig):
    name = 'paxful'
