from django.urls import path
from .views import GetPaxfulLinkAPIView

app_name = "paxful"

urlpatterns = [
    path('get_paxful_link', GetPaxfulLinkAPIView.as_view(), name='get-paxful-link')
]
