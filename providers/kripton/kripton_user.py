from core.exceptions import CustomError
from providers.kripton.clients import DefaultKriptonClient, KriptonClient
from kripton.models import UserModel


class KriptonUser:
    def __init__(self,
                 model_data: UserModel = UserModel.objects.none(),
                 a_client: KriptonClient = DefaultKriptonClient()):
        self._model_data = model_data
        self._provider_id = '1'
        self._client = a_client

    def _get_user(self, email: str):
        response = self._client.check_user(email)
        return response.json() if response.status_code == 200 else {}

    def _get_user_with_id(self, provider_user_id: int):
        response = self._client.check_user_with_id(provider_user_id)
        return response.json() if response.status_code == 200 else {}

    def _create_user(self, email: str):
        response = self._client.create_user(email)
        if response.status_code != 200:
            raise CustomError('Error al crear nuevo usuario en el proveedor')
        return response.json()['data']['user']

    def _registration_status(self, user_info):
        user_has_no_loaded_information = user_info['kyc']['information'] == 'pending' and \
                                         user_info['kyc']['domicile'] == 'pending'
        status = 'USER_INFORMATION' if user_has_no_loaded_information else 'USER_IMAGES'
        required_documents = ['dni_selfie', 'front_document', 'back_document']
        uploaded_images = all(document in user_info['digital_documents'] for document in required_documents)
        if uploaded_images or user_info.get('kyc_approved', False):
            status = 'COMPLETE'
        return status

    def create(self, an_email: str, a_user_id: str):
        user = self._get_user(an_email)
        provider_user_id = user['id'] if user else self._create_user(an_email)['id']
        user_info = self._get_user_with_id(provider_user_id)
        registration_status = self._registration_status(user_info)
        created_user = UserModel.objects.create(app_id=1, provider=1, provider_user_id=provider_user_id,
                                                email=an_email, user_id=a_user_id,
                                                registration_status=registration_status)
        return KriptonUser(created_user)

    @property
    def kyc_approved(self):
        response = self._get_user(self._model_data.email)
        return response['kyc_approved'] if response['kyc_approved'] is not None else False

    @property
    def registration_status(self):
        return self._model_data.registration_status

    @property
    def id(self):
        return self._model_data.provider_user_id
