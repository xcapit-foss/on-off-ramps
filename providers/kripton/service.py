from providers.kripton.clients import DefaultKriptonClient, KriptonClient
from rest_framework import status
from kripton.models import UserModel
from kripton.users.services import UsersService
from providers.enums import ProvidersEnum
from core.exceptions import CustomError
from providers.serializers import KriptonOperationReturnSerializer
from requests.exceptions import RetryError

class KriptonService:

    def __init__(self, client: KriptonClient = DefaultKriptonClient()):
        self._client = client
        self._user_service = UsersService()
        self._provider_id = ProvidersEnum.kripton.value

    def get_currencies(self):
        currencies = self._client.get_currencies()

        if currencies.status_code == status.HTTP_200_OK:
            return currencies.json()
        else:
            self._raise_custom_error('Error al obtener las monedas del proveedor')

    def get_quotations(self, currency=None):
        quotations = self._client.get_quotations()

        if quotations.status_code != 200:
            self._raise_custom_error('Error al obtener las cotizaciones del proveedor')

        if currency is None:
            return quotations.json()

        data = quotations.json().get("data")
        quotation = self._filter_currencie(data, currency)

        if len(quotation) == 0:
            self._raise_custom_error('Error al obtener la cotizacion de la moneda seleccionada desde proveedor')

        return quotation

    def save_user_information(self, provider_user_id, auth_token, data=()):
        user_info = self._client.register_user_information(provider_user_id, data, auth_token)
        physical_address_data = self._client.save_user_physical_address(provider_user_id, data, auth_token)

        if user_info.status_code == 401 or physical_address_data.status_code == 401:
            raise RetryError('')
        if user_info.status_code != 200 or physical_address_data.status_code != 200:
            self._raise_custom_error('Error al intentar guardar los datos del usuario en el proveedor')

        user_db = UserModel.objects.filter(provider_user_id=provider_user_id, provider=1)[0]
        user_db.registration_status = 'USER_IMAGES'
        user_db.save()

        return {'status': 'Ok', 'message': 'Información de usuario guardada con éxito'}

    def save_user_bank(self, provider_user_id, auth_token, data=()):
        user_bank = self._client.register_user_bank_account(provider_user_id, data, auth_token)

        if user_bank.status_code == 401:
            raise RetryError('')
        if user_bank.status_code != 200:
            self._raise_custom_error('Error al intentar guardar los datos bancarios del usuario en el proveedor')

        result = user_bank.json()
        return {"status": "Ok", "id": result['data']['id']}

    def upload_user_image(self, provider_user_id, auth_token, data=()):
        for document_name, image in data.items():
            uploaded_image = self._client.upload_image(provider_user_id, document_name, image, auth_token)
            if uploaded_image.status_code == 401:
                raise RetryError('')
            if uploaded_image.status_code != 200:
                self._raise_custom_error('Error al intentar guardar la imagen en el proveedor')

        user_db = UserModel.objects.filter(provider_user_id=provider_user_id, provider=1)[0]
        user_db.registration_status = 'COMPLETE'
        user_db.save()

        return {'status': 'Ok', 'message': 'Imagen de usuario guardada con éxito'}

    def get_operations(self, provider_user_id, operation_id=None):
        message = 'Error al obtener las operaciones desde proveedor'
        params = {'user_id': provider_user_id}

        if operation_id is not None:
            params['operation_id'] = operation_id
            message = 'Error al obtener la operacion desde proveedor'

        operations = self._client.get_operations(**params)

        if operations.status_code != status.HTTP_200_OK:
            self._raise_custom_error(message)
        serialized_operations = self._serialize_output(operations.json().get("data"))

        return serialized_operations

    def get_user_bank(self, provider_user_id, payment_method_id, auth_token):
        user_banks = self._client.get_user_banks(provider_user_id, auth_token)

        if user_banks.status_code == 401:
            raise RetryError('')
        if user_banks.status_code != 200:
            self._raise_custom_error('Error al obtener la información bancaria del usuario')

        result = None
        for user_bank in user_banks.json()['data']:
            if int(user_bank['id']) == int(payment_method_id):
                result = user_bank
                break
        return result

    @staticmethod
    def _raise_custom_error(message=''):
        raise CustomError(f'{message}')

    @staticmethod
    def _filter_currencie(quotations, currency):
        return [x for x in quotations if x['currency'] == currency]

    @staticmethod
    def _save_user(app_id, provider, provider_user_id, email, user_id):
        return UserModel.create(
            app_id,
            provider,
            provider_user_id,
            email,
            user_id
        )

    @staticmethod
    def _serialize_output(ops):
        serialized_output = []
        _ops = [ops] if not isinstance(ops, list) else ops
        serializer_class = KriptonOperationReturnSerializer
        for op in _ops:
            op['operation_id'] = op['id']
            serializer = serializer_class(data=op)
            if serializer.is_valid(raise_exception=False):
                serialized_output.append(serializer.data)
        return serialized_output
