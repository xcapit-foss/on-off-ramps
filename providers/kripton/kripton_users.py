from providers.kripton.clients import KriptonClient, DefaultKriptonClient
from providers.kripton.kripton_user import KriptonUser
from kripton.models import UserModel


class KriptonUsers:
    def __init__(self, a_model: UserModel = UserModel, a_client: KriptonClient = DefaultKriptonClient()):
        self._model = a_model
        self._client = a_client

    def _get_user_by(self, an_email: str) -> UserModel:
        user = self._model.objects.filter(email=an_email)
        return user.first() if len(user) > 0 else None

    def get_or_create_by(self, an_email: str, a_user_id: str) -> KriptonUser:
        user = self._get_user_by(an_email)
        return KriptonUser(user, self._client) if user else \
            KriptonUser(a_client=self._client).create(an_email, a_user_id)
