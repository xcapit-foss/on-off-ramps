import os
from abc import abstractmethod, ABC

import requests

from core.clients import ApiClient
from on_off_ramps.settings import KRIPTON_PUBLIC_URL
from providers.helpers import decode_base64

KRIPTON_API = os.environ.get("KRIPTON_API")
KRIPTON_QUOTATIONS = os.environ.get("KRIPTON_QUOTATIONS")
KRIPTON_TOKEN = os.environ.get("KRIPTON_TOKEN")


class KriptonClient(ABC):
    @abstractmethod
    def create_user(self, *args):
        """"""

    @abstractmethod
    def check_user(self, *args):
        """"""

    @abstractmethod
    def check_user_with_id(self, *args):
        """"""

    @abstractmethod
    def register_user_information(self, *args):
        """"""

    @abstractmethod
    def save_user_physical_address(self, *args):
        """"""

    @abstractmethod
    def get_currencies(self, *args):
        """"""

    @abstractmethod
    def get_quotations(self, *args):
        """"""

    @abstractmethod
    def register_user_bank_account(self, *args):
        """"""

    @abstractmethod
    def upload_image(self, *args):
        """"""

    @abstractmethod
    def get_operations(self, *args, **kwargs):
        """"""

    @abstractmethod
    def save_wallet(self, *args, **kwargs):
        """"""

    @abstractmethod
    def create_operation(self, *args, **kwargs):
        """"""

    @abstractmethod
    def upload_voucher(self, *args, **kwargs):
        """"""

    @abstractmethod
    def send_tx_hash(self, *args):
        """"""

    @abstractmethod
    def request_login_token(self, *args):
        """"""

    @abstractmethod
    def login(self, *args):
        """"""

    @abstractmethod
    def get_user_limits(self, *args):
        """"""

    @abstractmethod
    def available_currencies(self, *args):
        """"""

    @abstractmethod
    def get_user_banks(self, *args, user_id, auth_token):
        """"""

    @abstractmethod
    def calculate_amount_out(self, mode: str, currency_in: str, currency_out: str, network_out: str):
        """"""


class FakeKriptonClient(KriptonClient):
    def __init__(
            self,
            create_user_response={},
            check_user_response={},
            check_user_with_id_response={},
            register_user_information_response={},
            save_user_physical_address_response={},
            upload_image_response={},
            request_login_token_response={},
            login_response={},
            save_wallet_response={},
            create_operation_response={},
            upload_voucher_response={},
            get_operations_response={},
            available_currencies_response={},
            register_user_bank_account_response={},
            get_quotations_response={},
            calculate_amount_out_response={},
            get_user_limits_response={}
    ):
        self._create_user_response = create_user_response
        self._check_user_response = check_user_response
        self._check_user_with_id_response = check_user_with_id_response
        self._register_user_information_response = register_user_information_response
        self._save_user_physical_address_response = save_user_physical_address_response
        self._request_login_token_response = request_login_token_response
        self._upload_image_response = upload_image_response
        self._login_response = login_response
        self._save_wallet_response = save_wallet_response
        self._create_operation_response = create_operation_response
        self._upload_voucher_response = upload_voucher_response
        self._get_operations_response = get_operations_response
        self._available_currencies_response = available_currencies_response
        self._register_user_bank_account_response = register_user_bank_account_response
        self._get_quotations_response = get_quotations_response
        self._calculate_amount_out_response = calculate_amount_out_response
        self._get_user_limits_response = get_user_limits_response

    def create_user(self, *args):
        return self._create_user_response

    def check_user(self, *args):
        return self._check_user_response

    def check_user_with_id(self, *args):
        return self._check_user_with_id_response

    def register_user_information(self, *args):
        return self._register_user_information_response

    def save_user_physical_address(self, *args):
        return self._save_user_physical_address_response

    def get_currencies(self, *args):
        """"""

    def get_quotations(self, *args):
        return self._get_quotations_response

    def register_user_bank_account(self, *args):
        return self._register_user_bank_account_response

    def upload_image(self, *args):
        return self._upload_image_response

    def get_operations(self, *args, **kwargs):
        return self._get_operations_response

    def save_wallet(self, *args, **kwargs):
        return self._save_wallet_response

    def create_operation(self, *args, **kwargs):
        return self._create_operation_response

    def upload_voucher(self, *args, **kwargs):
        return self._upload_voucher_response

    def send_tx_hash(self, *args):
        """"""

    def request_login_token(self, email):
        return self._request_login_token_response

    def login(self, email, token):
        return self._login_response

    def get_user_limits(self, *args):
        return self._get_user_limits_response

    def available_currencies(self, *args):
        return self._available_currencies_response

    def get_user_banks(self, *args):
        """"""

    def calculate_amount_out(self, *args):
        return self._calculate_amount_out_response


class DefaultKriptonClient(KriptonClient, ApiClient):

    def __init__(self, api_url: str = KRIPTON_API):
        self._header = {'Authorization': KRIPTON_TOKEN}
        super().__init__(api_url)

    def _header_with_auth(self, ac):
        return {**self._header, 'Authorization-Client': f'Bearer {ac}'}

    def get_currencies(self):
        return self._http.get(self._generate_endpoint('public/currencies'))

    def get_quotations(self, quotations_url: str = KRIPTON_QUOTATIONS):
        return self._http.get(quotations_url)

    def check_user(self, email: str = ''):
        data = {"username": email}
        return self._http.post(self._generate_endpoint('hooks/clients/check'), data=data, headers=self._header)

    def check_user_with_id(self, provider_user_id: int):
        return self._http.get(self._generate_endpoint(f'hooks/clients/{provider_user_id}'), headers=self._header)

    def available_currencies(self):
        return self._http.get(self._generate_endpoint('hooks/config/crypto_networks'), headers=self._header)

    def create_user(self, email: str, avoid_password_email=True):
        data = {"username": email, 'avoid_password_email': avoid_password_email}
        return self._http.post(self._generate_endpoint('hooks/clients'), data=data, headers=self._header)

    def register_user_information(self, provider_user_id, raw_data, auth_token):
        valid_fields = ['birthday', 'first_name', 'last_name', 'gender', 'marital_status', 'nationality',
                        'politically_exposed', 'document_type', 'document_number']

        data = {key: value for key, value in raw_data.items() if key in valid_fields}

        return self._http.post(self._generate_endpoint(f'hooks/clients/{provider_user_id}/personal_informations'),
                               data=data,
                               headers=self._header_with_auth(auth_token))

    def register_user_bank_account(self, user_id, data, auth_token):
        return self._http.post(self._generate_endpoint(f'hooks/clients/{user_id}/banks'), data=data,
                               headers=self._header_with_auth(auth_token))

    def upload_image(self, provider_user_id, document_name, image, auth_token):
        try:
            data_type = {'document_type': document_name}
            tmp_file, file = self._generate_tmp_file(image, document_name)

            return self._http.post(self._generate_endpoint(f'hooks/clients/{provider_user_id}/digital_documents'),
                                   data=data_type, files=file, headers=self._header_with_auth(auth_token))
        finally:
            os.remove(tmp_file)

    def get_operations(self, user_id, auth_token, operation_id=None):
        op_id = f'/{operation_id}' if operation_id is not None else ''
        return self._http.get(self._generate_endpoint(f'hooks/clients/{user_id}/operations{op_id}'),
                              headers=self._header_with_auth(auth_token))

    def get_user_banks(self, user_id, auth_token):
        return self._http.get(self._generate_endpoint(f'hooks/clients/{user_id}/banks'),
                              headers=self._header_with_auth(auth_token))

    def create_operation(self, user_id, data, auth_token):
        return self._http.post(self._generate_endpoint(f'hooks/clients/{user_id}/operations'), data=data,
                               headers=self._header_with_auth(auth_token))

    def save_wallet(self, user_id, data, auth_token):
        return self._http.post(self._generate_endpoint(f'hooks/clients/{user_id}/wallets'), data=data,
                               headers=self._header_with_auth(auth_token))

    def upload_voucher(self, user_id, operation_id, data, auth_token):
        try:
            tmp_file, file = self._generate_tmp_file(data['file'])
            return self._http.patch(self._generate_endpoint(f'hooks/clients/{user_id}/cash_ins/{operation_id}'),
                                    data={}, files=file, headers=self._header_with_auth(auth_token))
        finally:
            os.remove(tmp_file)

    def send_tx_hash(self, user_id, operation_id, data):
        return self._http.patch(self._generate_endpoint(f'hooks/clients/{user_id}/cash_outs/{operation_id}'), data=data,
                                headers=self._header)

    def save_user_physical_address(self, provider_user_id, raw_data, auth_token):
        valid_fields = ['postal_code', 'city', 'street_address', 'street_number', 'telephone_number']
        data = {key: value for key, value in raw_data.items() if key in valid_fields}

        return self._http.post(self._generate_endpoint(f'hooks/clients/{provider_user_id}/domiciles'), data=data,
                               headers=self._header_with_auth(auth_token))

    @staticmethod
    def _generate_tmp_file(file, filename=None):
        if filename is None:
            filename = 'voucher'
        tmp_file, content_type, filename = decode_base64(file, filename)
        return tmp_file, {'file': (filename, open(tmp_file, 'rb'), content_type)}

    def request_login_token(self, email) -> requests.Response:
        return self._http.post(self._generate_endpoint('hooks/security/request_token'),
                               data={'username': email},
                               headers=self._header)

    def refresh_token(self, access_token, refresh_token) -> requests.Response:
        access_token = 'Bearer ' + access_token
        refresh_token = 'Bearer ' + refresh_token
        return self._http.post(self._generate_endpoint('hooks/security/refresh'),
                               data={'token': access_token, 'refresh': refresh_token},
                               headers=self._header)

    def login(self, email, token):
        return self._http.post(self._generate_endpoint('hooks/security/login'),
                               data={'username': email, 'token': token},
                               headers=self._header)

    def get_user_limits(self, mode, user_id, currency):
        params = {'kripto_service': mode, 'currency': currency, 'client_id': user_id}
        return self._http.get(self._generate_endpoint('hooks/config/limits'), params=params, headers=self._header)

    def confirm_cash_out_operation(self, user_id, auth_token, operation_id, data):
        return self._http.patch(self._generate_endpoint(f'/hooks/clients/{user_id}/cash_outs/{operation_id}'),
                                data=data, headers=self._header_with_auth(auth_token))

    def calculate_amount_out(self, mode: str, currency_in: str, currency_out: str, network_out: str, amount_in=10000):
        data = {
            "currency_in": currency_in,
            "amount_in": amount_in,
            "currency_out": currency_out,
            "type": mode,
            "network_out": network_out
        }
        return self._http.post(f'{KRIPTON_PUBLIC_URL}/calculate_amount_out', data=data)
