import base64
import tempfile


def decode_base64(data, filename):
        data_split = data.split(",")
        content_type = data_split[0].split(";")[0].replace('data:', '')
        format = content_type.split("/")[1]
        filename = f"{filename}.{format}"

        file = tempfile.NamedTemporaryFile(delete=False)
        with open(file.name, 'w+b') as fi:
            fi.write(base64.b64decode(data_split[1]))

        return file.name, content_type, filename
