from enum import Enum


class ProvidersEnum(Enum):
    kripton = "1"
    paxful = "2"


class CurrencyNetworksEnum(Enum):
    ERC20 = "eth"
    BSC_BEP20 = "bsc"
    RSK = "rsk"
    MATIC = "polygon"
