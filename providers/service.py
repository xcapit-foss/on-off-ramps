from providers.kripton.service import KriptonService
from providers.paxful.service import PaxfulService
from .enums import ProvidersEnum


class GetProviderError(Exception):
    DEFAULT_MESSAGE = 'Error al obtener el proveedor seleccionado'

    def __init__(self, message=''):
        self.message = message or self.DEFAULT_MESSAGE
        super().__init__(self.message)


class ProvidersService:

    def __init__(self):
        self._providers = ProvidersEnum

    def get_provider_client(self, provider_id):
        if provider_id == self._providers.kripton.value:
            return KriptonService()
        elif provider_id == self._providers.paxful.value:
            return PaxfulService()
        else:
            self._raise_get_provider_client_error(provider_id)

    @staticmethod
    def _raise_get_provider_client_error(provider_id: int):
        raise GetProviderError(
            f'{GetProviderError.DEFAULT_MESSAGE}, '
            f'id provider: {provider_id}')
