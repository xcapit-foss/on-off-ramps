import json

from rest_framework import serializers
from providers.enums import ProvidersEnum
from operations.models import OperationsModel
import logging

logger = logging.getLogger(__name__)


class KriptonOperationReturnSerializer(serializers.Serializer):
    operation_id = serializers.CharField(max_length=255, source='id')
    operation_type = serializers.SerializerMethodField()
    status = serializers.CharField(max_length=255)
    currency_in = serializers.CharField(max_length=255)
    amount_in = serializers.FloatField()
    currency_out = serializers.CharField(max_length=255)
    amount_out = serializers.FloatField()
    created_at = serializers.CharField(max_length=255)
    provider = serializers.SerializerMethodField()
    voucher = serializers.SerializerMethodField()
    wallet_address = serializers.SerializerMethodField()
    tx_hash = serializers.CharField(max_length=255, allow_null=True)
    network = serializers.SerializerMethodField()
    fiat_fee = serializers.SerializerMethodField()
    payment_method_id = serializers.CharField(max_length=255)
    external_code = serializers.SerializerMethodField()

    @staticmethod
    def get_provider(obj):
        return ProvidersEnum.kripton.value

    def get_fiat_fee(self, obj):
        json_data = json.loads(self.initial_data.get('support_data'))
        return json_data.get('costs')

    @staticmethod
    def get_operation_type(obj):
        return OperationsModel.objects.get(provider_id="1", operation_id=obj['id']).operation_type

    @staticmethod
    def get_wallet_address(obj):
        return OperationsModel.objects.get(provider_id="1", operation_id=obj['id']).wallet

    def get_voucher(self, obj):
        operation_type = self.get_operation_type(obj)
        if operation_type == 'cash-in':
            return True if self.initial_data['url_voucher_image'] is not None else False
        elif operation_type == 'cash-out':
            return True if self.initial_data['tx_hash'] is not None else False

    @staticmethod
    def _operation_exists(op_id):
        return OperationsModel.objects.filter(provider_id="1", operation_id=op_id).exists()

    @staticmethod
    def get_network(obj):
        return OperationsModel.objects.get(provider_id="1", operation_id=obj['id']).network

    @staticmethod
    def get_external_code(obj):
        return OperationsModel.objects.get(provider_id="1", operation_id=obj['id']).external_code

    def validate(self, attrs):
        self._validate_operation_type(attrs)
        return super().validate(attrs)

    def _validate_operation_type(self, attrs):
        op_id = attrs.get('id')
        if not self._operation_exists(op_id):
            logger.warning(f'La operación con el id {op_id} del proveedor kripton no existe localmente en la BD')
            raise serializers.ValidationError('La operación no existe en la BD')


class PaxfulOperationReturnSerializer(serializers.Serializer):
    operation_id = serializers.CharField(max_length=255)
    operation_type = serializers.CharField(max_length=255)
    status = serializers.CharField(max_length=255)
    currency_in = serializers.CharField(max_length=255)
    amount_in = serializers.FloatField()
    currency_out = serializers.CharField(max_length=255)
    amount_out = serializers.FloatField()
    created_at = serializers.CharField(max_length=255)
    provider = serializers.SerializerMethodField()

    @staticmethod
    def get_provider(obj):
        return ProvidersEnum.paxful.value
