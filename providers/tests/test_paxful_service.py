import pytest
from providers.paxful.service import PaxfulService
from urllib.parse import urlparse, urlsplit, parse_qsl


def test_paxful_get_link():
    paxful_service = PaxfulService()
    response = paxful_service.get_link('asdasda', '130')

    url = response['url']
    query_parameters = dict(parse_qsl(urlsplit(url).query))
    assert paxful_service
    assert query_parameters['track_id'] == '130'
    assert query_parameters['ext_crypto_address'] == 'asdasda'
