from providers.kripton.clients import FakeKriptonClient
from providers.kripton.kripton_user import KriptonUser
from kripton.models import UserModel
import pytest


def test_kripton_user_new():
    assert KriptonUser(UserModel.objects.none())


@pytest.mark.django_db
def test_kripton_user_create_user_with_kyc_approved(kripton_create_user_response,
                                                    kripton_check_user_response_with_kyc,
                                                    kripton_check_user_with_id_with_kyc):
    fake_client = FakeKriptonClient(kripton_create_user_response,
                                    kripton_check_user_response_with_kyc,
                                    kripton_check_user_with_id_with_kyc)
    KriptonUser(a_client=fake_client).create('test@test.com', '1')
    user = UserModel.objects.get(email='test@test.com', provider_user_id=10000408)
    assert user.user_id == '1'
    assert user.registration_status == 'COMPLETE'


@pytest.mark.django_db
def test_kripton_user_create_user_without_kyc_approved_and_images_uploaded(
        kripton_create_user_response,
        kripton_check_user_response_without_kyc,
        kripton_check_user_with_id_without_kyc_and_images_uploaded):
    fake_client = FakeKriptonClient(
        kripton_create_user_response,
        kripton_check_user_response_without_kyc,
        kripton_check_user_with_id_without_kyc_and_images_uploaded)
    KriptonUser(a_client=fake_client).create('test@test.com', '1')
    user = UserModel.objects.get(email='test@test.com', provider_user_id=10000408)
    assert user.user_id == '1'
    assert user.registration_status == 'COMPLETE'


@pytest.mark.django_db
def test_kripton_user_create_user_with_only_user_information(
        kripton_create_user_response,
        kripton_check_user_response_without_kyc,
        kripton_check_user_with_id_with_only_user_information):
    fake_client = FakeKriptonClient(
        kripton_create_user_response,
        kripton_check_user_response_without_kyc,
        kripton_check_user_with_id_with_only_user_information
    )
    KriptonUser(a_client=fake_client).create('test@test.com', '1')
    user = UserModel.objects.get(email='test@test.com', provider_user_id=10000408)
    assert user.user_id == '1'
    assert user.registration_status == 'USER_IMAGES'


@pytest.mark.django_db
def test_kripton_user_create_user_without_user_information(
        kripton_create_user_response,
        kripton_check_user_response_without_kyc,
        kripton_check_user_with_id_without_user_information):
    fake_client = FakeKriptonClient(
        kripton_create_user_response,
        kripton_check_user_response_without_kyc,
        kripton_check_user_with_id_without_user_information
    )
    KriptonUser(a_client=fake_client).create('test@test.com', '1')
    user = UserModel.objects.get(email='test@test.com', provider_user_id=10000408)
    assert user.user_id == '1'
    assert user.registration_status == 'USER_INFORMATION'


@pytest.mark.django_db
def test_kripton_user_create_user_who_wasnt_previously_created_on_provider(
        kripton_create_user_response,
        kripton_check_user_response_user_not_exists_on_provider,
        kripton_check_user_with_id_without_user_information):
    fake_client = FakeKriptonClient(
        kripton_create_user_response,
        kripton_check_user_response_user_not_exists_on_provider,
        kripton_check_user_with_id_without_user_information)
    KriptonUser(a_client=fake_client).create('test@test.com', '1')
    user = UserModel.objects.get(email='test@test.com', provider_user_id=10000)
    assert user.user_id == '1'
    assert user.registration_status == 'USER_INFORMATION'


@pytest.mark.django_db
def test_kripton_user_kyc_approved(create_kripton_user, kripton_check_user_response_with_kyc):
    fake_client = FakeKriptonClient(check_user_response=kripton_check_user_response_with_kyc)
    assert KriptonUser(UserModel.objects.get(email='test@test.com'), fake_client).kyc_approved is True


@pytest.mark.django_db
def test_kripton_user_registration_status(create_kripton_user):
    assert KriptonUser(UserModel.objects.get(email='test@test.com')).registration_status == 'COMPLETE'


@pytest.mark.django_db
def test_kripton_user_id(create_kripton_user):
    assert KriptonUser(UserModel.objects.get(email='test@test.com')).id == 9999
