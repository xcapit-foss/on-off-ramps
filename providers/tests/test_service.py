import pytest
from providers.service import ProvidersService


def test_new():
    provider_service = ProvidersService()

    assert provider_service


@pytest.mark.parametrize('provider_id', ["1", "2"])
def test_get_provider_selector(provider_id):
    providerService = ProvidersService()
    provider_service = providerService.get_provider_client(provider_id)

    assert provider_service
