import pytest
from unittest.mock import Mock

from providers.kripton.clients import FakeKriptonClient
from providers.kripton.service import (KriptonService, CustomError)
from kripton.models import UserModel

KRIPTON_CURRENCIES = {
    "data": [
        {
            "id": "usd",
            "name": "usd",
            "created_at": "2021-02-07T00:04:29.549Z",
            "updated_at": "2021-02-07T00:04:29.549Z",
            "fiat": True
        },
        {
            "id": "dai",
            "name": "dai",
            "created_at": "2021-02-08T00:30:06.773Z",
            "updated_at": "2021-02-08T00:30:06.773Z",
            "fiat": False
        },
        {
            "id": "usdt",
            "name": "usdt",
            "created_at": "2021-02-08T00:30:08.224Z",
            "updated_at": "2021-02-08T00:30:08.224Z",
            "fiat": False
        }
    ]
}

ALL_QUOTATIONS = [
    {
        "currency": "dai",
        "market_price": {
            "ars": "156.00",
            "usd": "1.00"
        },
        "quotation": {
            "ars": {
                "buy": "151.32",
                "sell": "160.68"
            },
            "usd": {
                "buy": "0.97",
                "sell": "1.03"
            }
        }
    },
    {
        "currency": "usdt",
        "market_price": {
            "ars": "156.00",
            "usd": "1.00"
        },
        "quotation": {
            "ars": {
                "buy": "151.32",
                "sell": "160.68"
            },
            "usd": {
                "buy": "0.97",
                "sell": "1.03"
            }
        }
    }
]

KRIPTON_QUOTATIONS = {
    "status": "ok",
    "data": ALL_QUOTATIONS
}

USDT_QUOTATION = [
    {
        "currency": "usdt",
        "market_price": {
            "ars": "156.00",
            "usd": "1.00"
        },
        "quotation": {
            "ars": {
                "buy": "151.32",
                "sell": "160.68"
            },
            "usd": {
                "buy": "0.97",
                "sell": "1.03"
            }
        }
    }
]


@pytest.fixture
def get_currencies_kripton_client_mock():
    def gck(mock_client=Mock(), data=(), status_code=200):
        mock_client.get_currencies.return_value.json.return_value = data
        mock_client.get_currencies.return_value.status_code = status_code

        return mock_client

    return gck


@pytest.fixture
def get_quotations_kripton_client_mock():
    def gqk(mock_client=Mock(), data=(), status_code=200):
        mock_client.get_quotations.return_value.json.return_value = data
        mock_client.get_quotations.return_value.status_code = status_code

        return mock_client

    return gqk


@pytest.fixture
def check_user_kripton_client_mock():
    def guk(mock_client=Mock(), data=(), status_code=200):
        mock_client.check_user.return_value.json.return_value = data
        mock_client.check_user.return_value.status_code = status_code

        return mock_client

    return guk


@pytest.fixture
def get_operations_kripton_client_mock():
    def guk(mock_client=Mock(), data=(), status_code=200):
        mock_client.get_operations.return_value.json.return_value = data
        mock_client.get_operations.return_value.status_code = status_code

        return mock_client

    return guk


@pytest.fixture
def create_operation_kripton_client_mock():
    def guk(mock_client=Mock(), data=(), status_code=200):
        mock_client.create_operation.return_value.json.return_value = data
        mock_client.create_operation.return_value.status_code = status_code

        return mock_client

    return guk


@pytest.fixture
def confirm_operation_kripton_client_mock():
    def guk(mock_client=Mock(), data=(), status_code=200):
        mock_client.confirm_operation.return_value.json.return_value = data
        mock_client.confirm_operation.return_value.status_code = status_code

        return mock_client

    return guk


@pytest.fixture
def create_user_kripton_client_mock():
    def cuk(mock_client=Mock(), data=(), status_code=200):
        mock_client.create_user.return_value.json.return_value = data
        mock_client.create_user.return_value.status_code = status_code

        mock_client.register_user_bank_account.return_value.json.return_value = {'status': 'ok'}
        mock_client.register_user_bank_account.return_value.status_code = status_code

        mock_client.upload_image.return_value.json.return_value = {'status': 'ok'}
        mock_client.upload_image.return_value.status_code = status_code

        return mock_client

    return cuk


@pytest.mark.django_db
@pytest.fixture
def create_user():
    data = {
        'app_id': 1,
        'email': 'test@test.com',
        'provider': 1,
        'provider_user_id': 10,
        'user_id': 34
    }
    return UserModel.create(**data)


def test_kripton_service(get_currencies_kripton_client_mock):
    kripton_service_1 = KriptonService()
    kripton_service_2 = KriptonService(client=get_currencies_kripton_client_mock)

    assert kripton_service_1
    assert kripton_service_2


def test_get_currencies(get_currencies_kripton_client_mock):
    mock_client = get_currencies_kripton_client_mock(data=KRIPTON_CURRENCIES)
    kripton_service = KriptonService(client=mock_client)

    result = kripton_service.get_currencies()

    assert result
    assert result == KRIPTON_CURRENCIES


def test_get_currencies_should_raise_an_exception_when_response_status_code_is_not_200(
        get_currencies_kripton_client_mock):
    mock_client = get_currencies_kripton_client_mock(status_code=400)
    kripton_service = KriptonService(client=mock_client)

    with pytest.raises(CustomError):
        kripton_service.get_currencies()


@pytest.mark.parametrize('currency, expected_result', [
    [None, KRIPTON_QUOTATIONS],
    ['usdt', USDT_QUOTATION]
])
def test_get_quotations(get_quotations_kripton_client_mock, currency, expected_result):
    mock_client = get_quotations_kripton_client_mock(data=KRIPTON_QUOTATIONS)
    kripton_service = KriptonService(mock_client)

    result = kripton_service.get_quotations(currency)

    assert result
    assert result == expected_result


def test_get_quotations_should_raise_an_exception_when_response_status_code_is_not_200(
        get_quotations_kripton_client_mock):
    mock_client = get_quotations_kripton_client_mock(status_code=400)
    kripton_service = KriptonService(client=mock_client)

    with pytest.raises(CustomError):
        kripton_service.get_quotations()


def test_get_quotations_should_raise_an_exception_when_currency_is_not_listed(get_quotations_kripton_client_mock):
    mock_client = get_quotations_kripton_client_mock(data=KRIPTON_QUOTATIONS)
    kripton_service = KriptonService(client=mock_client)

    with pytest.raises(CustomError):
        kripton_service.get_quotations('xxx')


@pytest.mark.django_db
def test_save_user_information_and_physical_address(create_user, raw_user_personal_information):
    provider_user_id = 10
    auth_token = raw_user_personal_information['auth_token']
    kripton_service = KriptonService(
        FakeKriptonClient(
            register_user_information_response=Mock(status_code=200, json=lambda: {}),
            save_user_physical_address_response=Mock(status_code=200, json=lambda: {})
        )
    )

    result = kripton_service.save_user_information(provider_user_id, auth_token, raw_user_personal_information)

    assert result == {'status': 'Ok', 'message': 'Información de usuario guardada con éxito'}


@pytest.mark.django_db
def test_save_user_bank(create_user):
    user_id = 10
    kripton_service = KriptonService(
        FakeKriptonClient(
            register_user_bank_account_response=Mock(status_code=200, json=lambda: {"data": {"id": 101}}),
        )
    )
    data = {
        "country": "COL",
        "currency": "USDC",
        "name": "Test",
        "account_type": "test",
        "type_of_document": "DU",
        "number_of_document": "12345678",
        "account_number": 1234
    }

    result = kripton_service.save_user_bank(user_id, data, 'test')

    assert result == {'status': 'Ok', 'id': 101}


@pytest.mark.django_db
def test_save_user_images(create_kripton_user):
    provider_user_id = 9999
    kripton_client = KriptonService(FakeKriptonClient(upload_image_response=Mock(status_code=200)))
    data = {
        'front_document': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==',
        'back_document': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==',
        'dni_selfie': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==',
    }

    result = kripton_client.upload_user_image(provider_user_id, 'test', data)
    assert result


def test_get_operations_should_raise_an_exception_when_response_status_code_is_not_200(
        get_operations_kripton_client_mock):
    mock_client = get_operations_kripton_client_mock(status_code=400)
    kripton_service = KriptonService(client=mock_client)

    with pytest.raises(CustomError):
        kripton_service.get_operations('1')
