import responses
from providers.kripton.clients import DefaultKriptonClient
import urllib.parse


def test_new():
    kripton_client = DefaultKriptonClient()

    assert kripton_client


@responses.activate
def test_get_currencies():
    api_url = 'http://app.provider.com/'
    url = f'{api_url}public/currencies'
    responses.add(responses.GET, url, json={}, status=200)
    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.get_currencies()

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_get_quotations():
    api_url = 'http://provider-quotations.com/'
    responses.add(responses.GET, api_url, json={}, status=200)
    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.get_quotations(api_url)

    assert result
    assert responses.calls[0].request.url == api_url


@responses.activate
def test_check_user():
    json_response = {"id": 1, "kyc_approved": None}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/check'
    responses.add(responses.POST, url, json=json_response, status=200)

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.check_user('test@test.com')

    assert result
    assert responses.calls[0].request.url == url
    assert result.json()['id'] == 1


@responses.activate
def test_create_user():
    json_response = {'data': {'user': {'username': 'test@test.com', 'id': 1}}}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients'
    responses.add(responses.POST, url, json=json_response, status=200)
    email = 'test@test.com'
    avoid_password_email = True
    data = {
        'email': email,
        'avoid_password_email': avoid_password_email,
    }

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.create_user(**data)

    assert result
    assert responses.calls[0].request.url == url
    assert responses.calls[
               0].request.body == f'username={urllib.parse.quote(email)}&avoid_password_email={avoid_password_email}'
    assert result.json()['data']['user']['id'] == 1
    assert result.json()['data']['user']['username'] == 'test@test.com'


@responses.activate
def test_register_user_information():
    user_id = 1
    json_response = {}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/{user_id}/personal_informations'
    responses.add(responses.POST, url, json=json_response, status=200)

    data = {
        'email': 'test@test.com',
        'provider': 1,
        'nacimiento': '1990/01/01',
        'nombre': 'Test',
        'apellido': 'TEST',
        'genero': 'Hombre',
        'estado_civil': 'single',
        'nacionalidad': 'Argentino',
        'expuesto_politicamente': False,
        'tipo_doc': 'dni',
        'nro_doc': '33333333',
        'codigo_postal': '5000',
        'ciudad': 'Córdoba',
        'direccion_calle': 'Bv. Chacabuco',
        'direccion_nro': '500'
    }

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.register_user_information(user_id, data, 'test')

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_register_user_bank_account():
    user_id = 4
    json_response = {}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/{user_id}/banks'
    responses.add(responses.POST, url, json=json_response, status=200)

    data = {
        "country": "COL",
        "currency": "USDC",
        "name": "Test",
        "account_type": "test",
        "type_of_document": "DU",
        "number_of_document": "12345678",
        "account_number": 123,
    }

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.register_user_bank_account(user_id, data, "test")

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_upload_image():
    provider_user_id = 1
    json_response = {}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/{provider_user_id}/digital_documents'
    responses.add(responses.POST, url, json=json_response, status=200)

    document_name = 'front_document'
    image = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD=='

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.upload_image(provider_user_id, document_name, image, 'test')

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_get_operations():
    json_response = {"data": [
        {
            "id": 1,
            "status": "pending_by_validate",
            "currency_in": "DOC",
            "amount_in": 100,
            "currency_out": "USD",
            "amount_out": 100,
            "comment": None,
            "tx_hash": None,
            "url_voucher_image": None,
            "client_id": 7,
            "merchant_id": 3,
            "created_at": "2021-03-02T19:24:38.468Z",
            "updated_at": "2021-03-02T19:24:38.468Z"
        }
    ]}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/7/operations/1'
    responses.add(responses.GET, url, json=json_response, status=200)

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.get_operations(7, 'test', 1)

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_create_operation():
    json_response = {
        "data": {
            "id": 1,
            "status": "pending_by_validate",
            "currency_in": "DOC",
            "amount_in": 200,
            "currency_out": "USD",
            "amount_out": 150,
            "comment": None,
            "tx_hash": None,
            "url_voucher_image": None,
            "client_id": 7,
            "merchant_id": 3,
            "created_at": "2021-03-08T14:44:18.903Z",
            "updated_at": "2021-03-08T14:44:18.903Z"
        }
    }
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/7/operations'
    responses.add(responses.POST, url, json=json_response, status=200)

    kripton_client = DefaultKriptonClient(api_url=api_url)

    data = {}
    result = kripton_client.create_operation(7, data, 'auth_token_test')

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_upload_voucher():
    user_id = 7
    operation_id = 30
    json_response = {}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/{user_id}/cash_ins/{operation_id}'
    responses.add(responses.PATCH, url, json=json_response, status=200)

    data = {'file': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD=='}

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.upload_voucher(user_id, operation_id, data, 'test')

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_send_hash():
    user_id = 7
    operation_id = 30
    json_response = {}
    api_url = 'http://app.provider.com/'
    url = f'{api_url}hooks/clients/{user_id}/cash_outs/{operation_id}'
    responses.add(responses.PATCH, url, json=json_response, status=200)

    data = {'tx_hash': 'asdasdasdasdasdasda'}

    kripton_client = DefaultKriptonClient(api_url=api_url)

    result = kripton_client.send_tx_hash(user_id, operation_id, data)

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_save_user_physical_address(raw_user_personal_information):
    provider_user_id = 7
    api_url = 'http://app.provider.com/'
    kripton_client = DefaultKriptonClient(api_url=api_url)
    url = f'{api_url}hooks/clients/{provider_user_id}/domiciles'

    responses.add(responses.POST, url, json={}, status=200)

    result = kripton_client.save_user_physical_address(provider_user_id, raw_user_personal_information,
                                                       'auth_token_test')
    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_request_login_token(raw_user_personal_information):
    api_url = 'http://app.provider.com/'
    kripton_client = DefaultKriptonClient(api_url=api_url)
    url = f'{api_url}hooks/security/request_token'
    responses.add(responses.POST, url, json={}, status=200)

    result = kripton_client.request_login_token('test@test.com')

    assert result
    assert responses.calls[0].request.url == url


@responses.activate
def test_login(raw_user_personal_information):
    api_url = 'http://app.provider.com/'
    kripton_client = DefaultKriptonClient(api_url=api_url)
    url = f'{api_url}hooks/security/login'
    responses.add(responses.POST, url, json={}, status=200)

    result = kripton_client.login('test@test.com', '123456')

    assert result
    assert responses.calls[0].request.url == url
