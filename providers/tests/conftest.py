from unittest.mock import Mock

import pytest

from kripton.models import UserModel


@pytest.fixture
def raw_user_personal_information():
    return {
        "email": "test@test.com",
        "provider": 1,
        "nacimiento": "1990/01/01",
        "nombre": "Test",
        "apellido": "TEST",
        "genero": "Hombre",
        "estado_civil": "single",
        "nacionalidad": "Argentino",
        "expuesto_politicamente": False,
        "tipo_doc": "dni",
        "nro_doc": "33333333",
        "codigo_postal": "5000",
        "ciudad": "Córdoba",
        "direccion_calle": "Bv. Chacabuco",
        "direccion_nro": "500",
        "telefono": "3333444444",
        "auth_token": "test"
    }


@pytest.fixture
def kripton_create_user_response():
    return Mock(status_code=200, json=lambda: {"data": {"user": {"id": 10000}}})


@pytest.fixture
def kripton_check_user_response_with_kyc():
    return Mock(status_code=200, json=lambda: {"id": 10000408, "kyc_approved": True})


@pytest.fixture
def kripton_check_user_response_without_kyc():
    return Mock(status_code=200, json=lambda: {"id": 10000408, "kyc_approved": False})


@pytest.fixture
def kripton_check_user_with_id_with_kyc():
    return Mock(status_code=200, json=lambda: {
        "id": 10000408,
        "kyc_approved": True,
        "kyc": {
            "information": "approving",
            "domicile": "approving",
            "bank": "approving"
        },
        "declared_domicile": None,
        "digital_documents": [
            "billing",
            "front_document",
            "back_document"
        ]
    })


@pytest.fixture
def kripton_check_user_with_id_without_kyc_and_images_uploaded():
    return Mock(status_code=200, json=lambda: {
        "id": 10000408,
        "kyc_approved": False,
        "kyc": {
            "information": "approving",
            "domicile": "approving",
            "bank": "approving"
        },
        "declared_domicile": None,
        "digital_documents": [
            "dni_selfie",
            "front_document",
            "back_document"
        ]
    })


@pytest.fixture
def kripton_check_user_with_id_with_only_user_information():
    return Mock(status_code=200, json=lambda: {
        "id": 10000408,
        "kyc_approved": False,
        "kyc": {
            "information": "approving",
            "domicile": "approving",
            "bank": "approving"
        },
        "declared_domicile": None,
        "digital_documents": []
    })


@pytest.fixture
def kripton_check_user_with_id_without_user_information():
    return Mock(status_code=200, json=lambda: {
        "id": 10000408,
        "kyc_approved": False,
        "kyc": {
            "information": "pending",
            "domicile": "pending",
            "bank": "pending"
        },
        "declared_domicile": None,
        "digital_documents": []
    })


@pytest.fixture
def kripton_check_user_response_user_not_exists_on_provider():
    return Mock(status_code=404, json=lambda: {})


@pytest.fixture
def create_kripton_user():
    UserModel.objects.create(app_id='1', provider='1', provider_user_id=9999,
                             email='test@test.com', user_id=47, registration_status='COMPLETE')
