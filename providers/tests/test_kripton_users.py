import pytest

from providers.kripton.clients import FakeKriptonClient
from providers.kripton.kripton_user import KriptonUser
from providers.kripton.kripton_users import KriptonUsers
from kripton.models import UserModel


def test_kripton_users_create():
    assert KriptonUsers(UserModel)


@pytest.mark.django_db
def test_kripton_users_get_or_create_user_when_user_exists(create_kripton_user,
                                                           kripton_create_user_response,
                                                           kripton_check_user_response_with_kyc,
                                                           kripton_check_user_with_id_without_user_information):
    kripton_client = FakeKriptonClient(kripton_create_user_response,
                                       kripton_check_user_response_with_kyc,
                                       kripton_check_user_with_id_without_user_information)
    kripton_users = KriptonUsers(a_client=kripton_client)
    kripton_user = kripton_users.get_or_create_by('test@test.com', '4')
    assert isinstance(kripton_user, KriptonUser)
    assert kripton_user.registration_status == 'COMPLETE'


@pytest.mark.django_db
def test_kripton_users_get_or_create_user_when_user_doesnt_exists(kripton_create_user_response,
                                                                  kripton_check_user_response_with_kyc,
                                                                  kripton_check_user_with_id_without_user_information):
    kripton_client = FakeKriptonClient(kripton_create_user_response,
                                       kripton_check_user_response_with_kyc,
                                       kripton_check_user_with_id_without_user_information)
    kripton_users = KriptonUsers(a_client=kripton_client)
    kripton_user = kripton_users.get_or_create_by('non_existent@test.com', '4123')
    assert isinstance(kripton_user, KriptonUser)
    assert kripton_user.registration_status == 'USER_INFORMATION'
