from django.db import models


class Providers(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    provider = models.CharField(max_length=50, default=None, null=False)
    client = models.CharField(max_length=50, default=None, null=False)
