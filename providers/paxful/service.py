import hmac
from hashlib import sha256
from urllib.parse import urlencode
from on_off_ramps.settings import PAXFUL_API_URL, PAXFUL_API_SECRET, \
    PAXFUL_API_KEY, PAXFUL_KIOSK_ID, \
    PAXFUL_AFFILIATE_CODE
from operations.models import OperationsModel
from providers.enums import ProvidersEnum
from core.exceptions import CustomError
from providers.serializers import PaxfulOperationReturnSerializer


class PaxfulService:

    def __init__(self):
        self._provider_id = ProvidersEnum.paxful.value

    def get_link(self, wallet, track_id):
        payload = urlencode([('r', PAXFUL_AFFILIATE_CODE),
                             ('ext_crypto_address', wallet),
                             ('apikey', PAXFUL_API_KEY),
                             ('kiosk', PAXFUL_KIOSK_ID),
                             ('track_id', track_id)])

        apiseal = hmac.new(PAXFUL_API_SECRET.encode(), payload.encode(), sha256).hexdigest()
        data_with_apiseal = payload + "&apiseal=" + apiseal

        url = PAXFUL_API_URL + data_with_apiseal
        return {"url": url}

    def create_operation(self, user_id, data=()):
        try:
            self._save_operation(data, user_id)
            response = {'status': 'Ok', 'message': 'Operación cargada con exito'}
        except Exception as e:
            self._raise_custom_error('Error al guardar la operación en BD')
            response = {'error': e}
        finally:
            return response

    def get_operations(self, user_id, operation_id=None):
        filter_kwargs = {'user_id': user_id, 'provider_id': self._provider_id}
        if operation_id is not None:
            filter_kwargs['operation_id'] = operation_id
        operations = OperationsModel.objects.filter(**filter_kwargs)
        serialized_operations = self._serialize_output(operations)
        return serialized_operations

    @staticmethod
    def _raise_custom_error(message=''):
        raise CustomError(f'{message}')

    def _save_operation(self, data, user_id):
        op_id = self._get_last_paxful_operation_id()
        operation_data = {
            'operation_id': op_id,
            'operation_type': data['type'],
            'currency_in': data['fiat_currency'],
            'amount_in': data['fiat_amount'],
            'currency_out': data['crypto_currency'],
            'amount_out': self._satoshi_to_btc(data['crypto_amount']),
            'provider_id': self._provider_id,
            'user_id': user_id,
            'status': data['status']
        }
        return OperationsModel.objects.create(**operation_data)

    @staticmethod
    def _get_last_paxful_operation_id():
        last_paxful_id = OperationsModel.objects.filter(provider_id="2").order_by('-operation_id')
        return last_paxful_id[0].operation_id + 1 if last_paxful_id.exists() else 1

    @staticmethod
    def _serialize_output(ops):
        serializer = PaxfulOperationReturnSerializer
        return [serializer(op.__dict__).data for op in ops]

    @staticmethod
    def _satoshi_to_btc(amount_in_satoshi):
        return int(amount_in_satoshi) / 100000000
