import requests

from operations.exceptions import RequestError
from operations.raw_operation import RawOperation
from operations.saved_user_payment_method import SavedUserPaymentMethod
from providers.kripton.clients import KriptonClient
from requests.exceptions import RetryError


class CreateOperationRequest:

    def __init__(self, saved_user_payment_method: SavedUserPaymentMethod, kripton_client: KriptonClient, auth_token):
        self._saved_user_payment_method = saved_user_payment_method
        self._client = kripton_client
        self._auth_token = auth_token

    def response(self) -> requests.Response:
        response = self._client.create_operation(self._saved_user_payment_method.operation().user().id,
                                                 data=self._parsed_data(),
                                                 auth_token=self._auth_token)
        if response.status_code == 401:
            raise RetryError('')
        elif 400 <= response.status_code < 500:
            raise RequestError(response.json())
        elif response.status_code >= 500:
            raise RequestError()
        return response

    def _get_payment_method_id(self):
        return self._saved_user_payment_method.value().get('payment_method_id')

    def _get_network(self):
        return self._saved_user_payment_method.operation().data().get('network')

    def _parsed_data(self):
        valid_fields = ['currency_in', 'amount_in', 'currency_out', 'amount_out', 'type']
        filtered_data = {key: value for key, value in self._saved_user_payment_method.operation().data().items() if
                         key in valid_fields}
        return {**filtered_data,
                'payment_method_id': self._get_payment_method_id(),
                'network_in': self._parsed_network(self._get_network())}

    @staticmethod
    def _parsed_network(network: str):
        networks = {
            'ERC20': 'ETH',
            'MATIC': 'Polygon',
            'BSC_BEP20': 'BSC',
            'RSK': 'RSK'
        }
        return networks[network]

    def operation(self) -> RawOperation:
        return self._saved_user_payment_method.operation()
