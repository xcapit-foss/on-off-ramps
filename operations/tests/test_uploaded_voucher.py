import pytest

from operations.uploaded_voucher import UploadedVoucher
from providers.kripton.clients import FakeKriptonClient
from requests.exceptions import RetryError
from rest_framework import status


@pytest.mark.django_db
def test_uploaded_voucher_new(test_kripton_user, uploaded_voucher_data):
    assert UploadedVoucher(test_kripton_user, uploaded_voucher_data, 10, auth_token='auth_token_test')


@pytest.mark.django_db
def test_uploaded_voucher_value(test_kripton_user, uploaded_voucher_data, request_response):
    response = UploadedVoucher(test_kripton_user,
                               uploaded_voucher_data,
                               10,
                               'auth_token_test',
                               FakeKriptonClient(upload_voucher_response=request_response(None))
                               ).value()
    assert response.status_code == 200


@pytest.mark.django_db
def test_uploaded_voucher_unauthorized(test_kripton_user, uploaded_voucher_data, request_response):
    with pytest.raises(RetryError):
        UploadedVoucher(test_kripton_user,
                        uploaded_voucher_data,
                        10,
                        'auth_token_test',
                        FakeKriptonClient(upload_voucher_response=request_response({}, status.HTTP_401_UNAUTHORIZED))
                        ).value()
