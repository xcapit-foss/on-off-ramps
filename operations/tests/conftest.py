from typing import List
from unittest.mock import Mock

import pytest

from kripton.models import UserModel
from operations.create_operation_request import CreateOperationRequest
from operations.models import OperationsModel
from operations.raw_operation import RawOperation
from operations.saved_user_payment_method import SavedUserWallet
from providers.kripton.clients import FakeKriptonClient
from providers.kripton.kripton_user import KriptonUser


@pytest.fixture
def request_response():
    def rr(json_data={}, status_code=200):
        return Mock(json=lambda: json_data, status_code=status_code)

    return rr


@pytest.fixture
def create_kripton_user():
    def ckug(email='test@test.com', registration_status='COMPLETE'):
        return UserModel.objects.create(app_id='1', provider='1', provider_user_id=9999,
                                        email=email, user_id=47, registration_status=registration_status)

    return ckug


@pytest.mark.django_db
@pytest.fixture
def create_operation():
    data = {
        'wallet': '0xtest_wallet',
        'operation_id': 1,
        'operation_type': 'cash-in',
        'currency_in': 'ars',
        'amount_in': '1',
        'price_in': '1.0',
        'currency_out': 'MATIC',
        'amount_out': '1',
        'price_out': '1.0',
        'provider_id': '1',
        'user_id': '1',
        'status': 'pending_by_validate',
        'voucher': False,
    }
    return {'operation_model': OperationsModel.objects.create(**data), 'kripton_wallet': None}


@pytest.fixture
def create_operation_data():
    return {
        'wallet': '0xtest_wallet',
        'operation_id': 1,
        'operation_type': 'cash-out',
        'currency_out': 'ars',
        'amount_out': '1',
        'price_out': '1.0',
        'currency_in': 'MATIC',
        'amount_in': '1',
        'price_in': '1.0',
        'provider_id': '1',
        'user_id': '1',
        'status': 'pending_by_validate',
        'voucher': False,
        'external_code': 'an_external_code'
    }


@pytest.mark.django_db
@pytest.fixture
def create_cash_out_operation(create_operation_data):
    return {'operation_model': OperationsModel.objects.create(**create_operation_data), 'kripton_wallet': '0xanAddress'}


@pytest.mark.django_db
@pytest.fixture
def create_paxful_operation():
    data = {
        'wallet': '',
        'operation_id': 1,
        'operation_type': 'cash-in',
        'currency_in': 'ARS',
        'amount_in': '471',
        'price_in': None,
        'currency_out': 'BTC',
        'amount_out': '0.00007245',
        'price_out': None,
        'provider_id': '2',
        'user_id': '3',
        'status': 'SUCCESS',
        'voucher': False,
    }
    return OperationsModel.objects.create(**data)


@pytest.mark.django_db
@pytest.fixture
def create_operation_cash_out():
    data = {
        'wallet': '0x_test_wallet',
        'operation_id': 3,
        'operation_type': 'cash-out',
        'currency_in': 'DOC',
        'amount_in': '1',
        'price_in': '1.0',
        'currency_out': 'BTC',
        'amount_out': '1',
        'price_out': '1.0',
        'provider_id': '1',
        'user_id': '1',
        'status': 'pending_to_validate',
        'voucher': False,
    }
    return OperationsModel.objects.create(**data)


@pytest.fixture
def create_operation_response():
    return {
        "data": {
            "id": 450,
            "status": "pending_by_validate",
            "currency_in": "ars",
            "amount_in": 518.0,
            "currency_out": "DAI",
            "amount_out": 1.87,
            "comment": None,
            "tx_hash": None,
            "url_voucher_image": None,
            "created_at": "2022-09-06T17:46:32.460Z",
            "type": "Operations::CashIn"
        },
        "invoice": {
            "url": "https://sandbox.app.kriptonmarket.com/public/invoices/hMGqdzt71I",
            "external_code": "hMGqdzt71I",
            "address": "XXXXXXXXXXXXXXXXXXXXX"
        }
    }


@pytest.fixture
def save_wallet_response():
    return {
        "data": {
            "id": 420,
            "address": "0xtest_wallet",
            "client_id": None,
            "created_at": "2022-07-19T20:45:25.948Z",
            "updated_at": "2022-07-19T20:45:25.948Z",
            "crypto_network_id": 19,
            "user_type": "User",
            "user_id": 9999
        }
    }


@pytest.fixture
def save_bank_response():
    return {
        "data": {
            "id": 108,
            "json_data": [
                {
                    "name_of_param": "country",
                    "value": "Argentina",
                    "label": "Pais"
                },
                {
                    "name_of_param": "currency",
                    "value": "Pesos Argentinos",
                    "label": "Moneda"
                },
                {
                    "name_of_param": "name",
                    "value": "Santander",
                    "label": "Nombre entidad bancaria"
                },
                {
                    "name_of_param": "account_type",
                    "value": "Ahorro",
                    "label": "Tipo de cuenta"
                },
                {
                    "name_of_param": "type_of_document",
                    "value": "DNI",
                    "label": "Tipo de documento"
                },
                {
                    "name_of_param": "number_of_document",
                    "value": "99999999",
                    "label": "Identificacion"
                },
                {
                    "name_of_param": "account_number",
                    "value": "0099090900990",
                    "label": "Numero de cuenta"
                },
                {
                    "name_of_param": "mail",
                    "value": "test@gmail.com",
                    "label": "Mail asociado"
                },
                {
                    "name_of_param": "alias",
                    "value": "test",
                    "label": "Alias"
                },
                {
                    "name_of_param": "cbu_cvu",
                    "value": "test",
                    "label": "CBU/CVU"
                }
            ]
        }
    }


@pytest.fixture
def uploaded_voucher_data():
    return {
        "file": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD==",
        "email": "test@test.com",
        "auth_token": 'test'
    }


@pytest.fixture
def create_operation_input_data(raw_operation_data):
    return {**raw_operation_data, "email": "test@test.com", "auth_token": 'auth_token_test'}


@pytest.fixture
def raw_operation_data():
    return {
        "wallet": "0xsome_test_wallet",
        "currency_in": "ARS",
        "amount_in": "200",
        "price_in": 1.0,
        "currency_out": "MATIC",
        "amount_out": "1",
        "price_out": 150.0,
        "type": "cash-in",
        "network": "MATIC",
    }


@pytest.fixture
def create_cash_out_operation_input_data(raw_operation_data_cash_out):
    return {**raw_operation_data_cash_out, "email": "test@test.com", "auth_token": 'auth_token_test'}


@pytest.fixture
def raw_operation_data_cash_out():
    return {
        "wallet": "0xsome_test_wallet",
        "currency_out": "ARS",
        "amount_out": "200",
        "price_out": 1.0,
        "currency_in": "MATIC",
        "amount_in": "1",
        "price_in": 150.0,
        "type": "cash-out",
        "network": "MATIC",
        "payment_method_id": 23
    }


@pytest.fixture
def test_kripton_user(create_kripton_user):
    return KriptonUser(model_data=create_kripton_user(email='test@test.com'))


@pytest.fixture
def raw_single_operation():
    return {
        "id": 1,
        "status": "pending_by_validate",
        "currency_in": "ars",
        "amount_in": "300.0",
        "currency_out": "MATIC",
        "amount_out": "1.16652378",
        "comment": None,
        "tx_hash": None,
        "url_voucher_image": None,
        "client_id": 4236,
        "merchant_id": 2,
        "created_at": "2022-08-04T12:00:24.649Z",
        "updated_at": "2022-08-04T12:00:24.950Z",
        "metadata": "{\"network_in\":null}",
        "token": "5bCaBx5z9EEAABRdTSFp",
        "earnings": "0.0",
        "support_data": """{\"raw_amount\":1.20151949,\"profit\":\"0.0\",\"earn_merchant\":\"0.03499571\",
        \"amount_out\":\"1.16652378\",\"fee_of_network\":0,\"costs\":0.03499571,\"costs_fiat\":\"0.0\"}""",
        "quotation_in": 5247,
        "quotation_out": 2425,
        "payment_method_type": None,
        "payment_method_id": '10',
        "reference_2": None,
        "pairing_network_id": None,
        "order_id": None
    }


@pytest.fixture
def raw_single_operation_complete():
    return {
        "id": 1,
        "status": "complete",
        "currency_in": "ars",
        "amount_in": "300.0",
        "currency_out": "MATIC",
        "amount_out": "1.16652378",
        "comment": None,
        "tx_hash": '0xthis_is_a_test_tx_hash',
        "url_voucher_image": None,
        "client_id": 4236,
        "merchant_id": 2,
        "created_at": "2022-08-04T12:00:24.649Z",
        "updated_at": "2022-08-04T12:00:24.950Z",
        "metadata": "{\"network_in\":null}",
        "token": "5bCaBx5z9EEAABRdTSFp",
        "earnings": "0.0",
        "support_data": """{\"raw_amount\":1.20151949,\"profit\":\"0.0\",\"earn_merchant\":\"0.03499571\",
        \"amount_out\":\"1.16652378\",\"fee_of_network\":0,\"costs\":0.03499571,\"costs_fiat\":\"0.0\"}""",
        "quotation_in": 5247,
        "quotation_out": 2425,
        "payment_method_type": None,
        "payment_method_id": '10',
        "reference_2": None,
        "pairing_network_id": None,
        "order_id": None
    }


@pytest.fixture
def raw_operations_response(raw_single_operation):
    return {"data": [raw_single_operation]}


@pytest.fixture
def expected_serialized_operations_response():
    return [{
        'operation_id': '1',
        'operation_type': 'cash-in',
        'status': 'complete',
        'currency_in': 'ars',
        'amount_in': 300.0,
        'currency_out': 'MATIC',
        'amount_out': 1.16652378,
        'created_at': '2022-08-04T12:00:24.649Z',
        'provider': '1',
        'voucher': False,
        'wallet_address': '0xtest_wallet',
        'tx_hash': '0xthis_is_a_test_tx_hash',
        'network': 'MATIC',
        'fiat_fee': 0.03499571,
        'payment_method_id': '10',
        'external_code': None
    }]


@pytest.fixture
def expected_serialized_cash_out_operations_response():
    return [{
        'operation_id': '1',
        'operation_type': 'cash-out',
        'status': 'complete',
        'currency_in': 'ars',
        'amount_in': 300.0,
        'currency_out': 'MATIC',
        'amount_out': 1.16652378,
        'created_at': '2022-08-04T12:00:24.649Z',
        'provider': '1',
        'voucher': True,
        'wallet_address': '0xtest_wallet',
        'tx_hash': '0xthis_is_a_test_tx_hash',
        'network': 'MATIC',
        'fiat_fee': 0.03499571,
        'payment_method_id': '10',
        'external_code': 'an_external_code'
    }]


@pytest.fixture
def expected_list_operations_response():
    return [{
        'operation_id': '1',
        'operation_type': 'cash-in',
        'status': 'pending_by_validate',
        'currency_in': 'ars',
        'amount_in': 300.0,
        'currency_out': 'MATIC',
        'amount_out': 1.16652378,
        'created_at': '2022-08-04T12:00:24.649Z',
        'provider': '1',
        'voucher': False,
        'wallet_address': '0xtest_wallet',
        'tx_hash': None,
        'network': 'MATIC',
        'fiat_fee': 0.03499571,
        'payment_method_id': '10',
        'external_code': None
    }]


@pytest.mark.django_db
@pytest.fixture
def create_multiple_operation_with_networks():
    def cmown(currencies: List[str]):
        operation_id = 1
        for currency in currencies:
            data = {
                'wallet': '0xtest_wallet',
                'operation_id': operation_id,
                'operation_type': 'cash-in',
                'currency_in': 'ars',
                'amount_in': '1',
                'price_in': '1.0',
                'currency_out': currency,
                'amount_out': '1',
                'price_out': '1.0',
                'provider_id': '1',
                'user_id': '1',
                'status': 'pending_by_validate',
                'voucher': False,
                'network': 'MATIC'
            }
            operation_id += 1
            return OperationsModel.objects.create(**data)

    return cmown


@pytest.fixture
def create_operation_with_fee():
    def cowf(fee: float, operation_id: int):
        data = {
            'wallet': '0xtest_wallet',
            'operation_id': operation_id,
            'operation_type': 'cash-in',
            'currency_in': 'ars',
            'amount_in': '1',
            'price_in': '1.0',
            'currency_out': 'USDC',
            'amount_out': '1',
            'price_out': '1.0',
            'provider_id': '1',
            'user_id': '1',
            'status': 'pending_by_validate',
            'voucher': False,
            'network': 'MATIC',
            'fee': fee
        }
        return OperationsModel.objects.create(**data)

    return cowf


@pytest.fixture
def test_operation(test_kripton_user, raw_operation_data):
    return RawOperation(test_kripton_user, raw_operation_data)


@pytest.fixture
def test_operation_cash_out(test_kripton_user, raw_operation_data_cash_out):
    return RawOperation(test_kripton_user, raw_operation_data_cash_out)


@pytest.fixture
def test_saved_user_wallet(test_operation):
    def tsuw(**kwargs):
        return SavedUserWallet(test_operation, FakeKriptonClient(**kwargs), auth_token='auth_token_test')

    return tsuw


@pytest.fixture
def test_create_operation_request(test_saved_user_wallet):
    def tcor(**kwargs):
        saved_user_wallet = test_saved_user_wallet(**kwargs)
        return CreateOperationRequest(saved_user_wallet, FakeKriptonClient(**kwargs), auth_token='auth_token_test')

    return tcor


@pytest.fixture
def expected_create_operation_view_response():
    return {
        'id': 1,
        'operation_type': 'cash-in',
        'currency_in': 'ars',
        'amount_in': '1',
        'price_in': '1.0',
        'currency_out': 'MATIC',
        'amount_out': '1',
        'price_out': '1.0',
        'provider_id': '1',
        'user_id': '1',
        'status': 'pending_by_validate',
        'voucher': False,
        'network': 'MATIC',
        'fee': 0.0,
        'external_code': None,
        'kripton_wallet': None
    }


@pytest.fixture
def expected_create_cash_out_operation_view_response():
    return {
        'id': 1,
        'operation_type': 'cash-out',
        'currency_out': 'ars',
        'amount_out': '1',
        'price_out': '1.0',
        'currency_in': 'MATIC',
        'amount_in': '1',
        'price_in': '1.0',
        'provider_id': '1',
        'user_id': '1',
        'status': 'pending_by_validate',
        'voucher': False,
        'network': 'MATIC',
        'fee': 0.0,
        'external_code': 'an_external_code',
        'kripton_wallet': '0xanAddress'
    }


@pytest.fixture
def get_single_operation_response():
    return {
        "id": 450,
        "status": "received",
        "currency_in": "ars",
        "amount_in": "2913.6",
        "currency_out": "USDC",
        "amount_out": "19.4",
        "comment": None,
        "tx_hash": None,
        "url_voucher_image": "operations/734/voucher-1672779374.png",
        "client_id": 10003072,
        "merchant_id": 2,
        "created_at": "2023-01-03T19:48:41.242Z",
        "updated_at": "2023-01-03T20:56:14.378Z",
        "metadata": "{\"network_in\":null}",
        "token": "w8xsjQXxzYuJwdEeziE4",
        "earnings": "0.0",
        "support_data": """{\"raw_amount\":20.0,\"profit\":\"0.0\",\"earn_merchant\":\"0.3\",\"amount_out\":19.4,
        \"fee_of_network\":0,\"costs\":0.6,\"costs_fiat\":\"0.0\",\"commissions\":{\"percentage\":1.5,\"amount\":0.3},
        \"taxes\":{\"percentage\":1.5,\"amount\":0.3}}""",
        "quotation_in": 12,
        "quotation_out": 33,
        "payment_method_type": None,
        "payment_method_id": None,
        "reference_2": None,
        "pairing_network_id": None,
        "order_id": None,
        "usd_amount": "19.4",
        "crypto_network_id": None
    }


@pytest.fixture
def confirm_operation_data():
    return {'auth_token': 'test',
            "tx_hash": "mv6q2q5wx1c2vt495s0q8dm8t3yjoi2j",
            'email': 'test@test.com'}


@pytest.fixture
def error_response():
    def er(error_code: int = 22, message: str = 'The amount_in is greater than the maximum limit'):
        return {
            'error_code': error_code,
            'message': message
        }

    return er
