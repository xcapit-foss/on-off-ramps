import pytest
from operations.create_operation_request import CreateOperationRequest
from providers.kripton.clients import FakeKriptonClient
from requests.exceptions import RetryError
from rest_framework import status


@pytest.mark.django_db
def test_create_operation_request_new(test_saved_user_wallet):
    assert CreateOperationRequest(test_saved_user_wallet, FakeKriptonClient(), 'auth_token_test')


@pytest.mark.django_db
def test_create_operation_request_operation(test_saved_user_wallet, test_operation, request_response):
    saved_user_wallet = test_saved_user_wallet(save_wallet_response=request_response({"data": {"id": 1}}))
    assert CreateOperationRequest(saved_user_wallet,
                                  FakeKriptonClient(), 'auth_token_test').operation().data() == test_operation.data()


@pytest.mark.django_db
def test_create_operation_request_response(test_saved_user_wallet, create_operation_response, request_response):
    saved_user_wallet = test_saved_user_wallet(save_wallet_response=request_response({"data": {"id": 1}}))
    fake_client = FakeKriptonClient(create_operation_response=request_response(create_operation_response))
    response = CreateOperationRequest(saved_user_wallet, fake_client, 'auth_token_test').response()

    assert response.json() == create_operation_response
    assert response.status_code == 200


@pytest.mark.django_db
def test_saved_user_wallet_retry_error(test_saved_user_wallet, request_response):
    with pytest.raises(RetryError):
        saved_user_wallet = test_saved_user_wallet(save_wallet_response=request_response({"data": {"id": 1}}))
        fake_client = FakeKriptonClient(create_operation_response=request_response({}, status.HTTP_401_UNAUTHORIZED))
        CreateOperationRequest(saved_user_wallet, fake_client, 'auth_token_test').response()
