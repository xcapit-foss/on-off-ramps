import pytest
from io import StringIO
from django.core.management import call_command

from operations.models import OperationsModel


@pytest.mark.django_db
def test_command_set_operations_network(create_multiple_operation_with_networks):
    expected_network_of = {'MATIC': 'MATIC', 'DAI': 'MATIC', 'USDC': 'MATIC', 'ETH': 'ERC20',
                           'USDT': 'ERC20', 'RBTC': 'RSK', 'RIF': 'RSK'}
    currencies = ['MATIC', 'RIF', 'RBTC', 'DAI', 'USDT', 'USDC', 'ETH']
    create_multiple_operation_with_networks(['matic', 'RIF', 'RBTC', 'DAI', 'USDT', 'USDC', 'ETH'])
    out = StringIO()
    call_command('set_operations_network', stdout=out)
    assert 'Success' in out.getvalue()
    for currency in currencies:
        assert not OperationsModel.objects.filter(provider_id=1, currency_out=currency).exclude(
            network=expected_network_of[currency]).exists()
