from unittest.mock import patch
from requests.exceptions import RetryError
import pytest
from django.urls import reverse


@pytest.mark.django_db
@patch('operations.uploaded_voucher.UploadedVoucher.value')
def test_confirm_operation_view(mock_uploaded_voucher, request_response, create_kripton_user, uploaded_voucher_data,
                                client, create_operation):
    create_kripton_user()
    mock_uploaded_voucher.return_value = request_response(None)

    params = {
        'provider_id': 1,
        'operation_id': 1,
        'user_id': 47
    }

    response = client.post(reverse('operations:confirm-operation', kwargs=params), data=uploaded_voucher_data)

    assert response.status_code == 200
    assert response.json() == {}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.upload_voucher')
def test_confirm_operation_view_error_unauthorized(mock_upload_voucher, request_response, create_kripton_user,
                                                   uploaded_voucher_data, client, create_operation):
    create_kripton_user()
    mock_upload_voucher.side_effect = RetryError

    params = {
        'provider_id': 1,
        'operation_id': 1,
        'user_id': 47
    }

    response = client.post(reverse('operations:confirm-operation', kwargs=params), data=uploaded_voucher_data)
    assert response.status_code == 401
    assert response.json() == {'error': 'Error al confirmar una operacion, token de acceso invalido',
                               "error_code": "on_off_ramps.operations.single_operation.tokenNotValid"}
