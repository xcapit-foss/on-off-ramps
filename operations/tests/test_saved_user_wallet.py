from operations.saved_user_payment_method import SavedUserWallet
from providers.kripton.clients import FakeKriptonClient
import pytest
from rest_framework import status
from requests.exceptions import RetryError


@pytest.mark.django_db
def test_saved_user_wallet_new(test_operation):
    assert SavedUserWallet(test_operation, FakeKriptonClient(), 'auth_token_test')


@pytest.mark.django_db
def test_saved_user_wallet_value(test_operation, save_wallet_response,
                                 request_response):
    result = SavedUserWallet(test_operation, FakeKriptonClient(
        save_wallet_response=request_response(save_wallet_response)), 'auth_token_test').value()
    assert result.get('user_id') == 9999
    assert result.get('payment_method_id') == 420


@pytest.mark.django_db
def test_saved_user_wallet_operation(test_operation):
    assert SavedUserWallet(test_operation, FakeKriptonClient(),
                           'auth_token_test').operation().data() == test_operation.data()


@pytest.mark.django_db
def test_saved_user_wallet_retry_error(test_operation, request_response, ):
    with pytest.raises(RetryError):
        SavedUserWallet(test_operation, FakeKriptonClient(
            save_wallet_response=request_response({}, status.HTTP_401_UNAUTHORIZED)), 'auth_token_test').value()
