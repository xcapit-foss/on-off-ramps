import pytest
from unittest.mock import patch
from django.urls import reverse
from requests.exceptions import RetryError

@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_list_operations_view(mock_get_operations, client, create_kripton_user, raw_operations_response,
                              create_operation, expected_list_operations_response, request_response):
    create_kripton_user()
    mock_get_operations.return_value = request_response(raw_operations_response)
    response = client.post(reverse('operations:get-user-operations', kwargs={'user_id': 1}),
                           data={'email': 'test@test.com', 'auth_token': 'test'})
    assert response.status_code == 200
    assert response.json() == expected_list_operations_response


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_list_operations_view_invalid(mock_get_operations, client, create_kripton_user, request_response):
    create_kripton_user()
    mock_get_operations.return_value = request_response({}, 400)
    response = client.post(reverse('operations:get-user-operations', kwargs={'user_id': 1}),
                           data={'email': 'test@test.com', 'auth_token': 'test'})
    assert response.status_code == 400
    assert response.json() == {"error": 'Error al obtener las operaciones desde proveedor',
                               'error_code': 'on_off_ramps.operations.list.errorProvider'}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_list_operations_view_unauthorized(mock_get_operations, client, create_kripton_user, request_response):
    create_kripton_user()

    mock_get_operations.side_effect = RetryError
    response = client.post(reverse('operations:get-user-operations', kwargs={'user_id': 1}),
                           data={'email': 'test@test.com', 'auth_token': 'unauthorized_token'})
    assert response.status_code == 401
    assert response.json() == {"error": 'Error al obtener las operaciones desde proveedor, token de acceso vencido',
                               'error_code': 'on_off_ramps.operations.list.errorProvider'}