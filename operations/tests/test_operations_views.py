import pytest
from unittest.mock import patch, Mock
from kripton.models import UserModel
from rest_framework import status
from django.urls import reverse
from operations.models import OperationsModel

operations_data = {"data": [
    {
        "id": 1,
        "status": "pending_by_validate",
        "currency_in": "DOC",
        "amount_in": 100,
        "currency_out": "USD",
        "amount_out": 100,
        "comment": None,
        "tx_hash": None,
        "url_voucher_image": None,
        "client_id": 7,
        "merchant_id": 3,
        "created_at": "2021-03-02T19:24:38.468Z",
        "updated_at": "2021-03-02T19:24:38.468Z"
    },
    {
        "id": 2,
        "status": "pending_by_validate",
        "currency_in": "DOC",
        "amount_in": 200,
        "currency_out": "USD",
        "amount_out": 150,
        "comment": None,
        "tx_hash": None,
        "url_voucher_image": None,
        "client_id": 7,
        "merchant_id": 3,
        "created_at": "2021-03-03T11:56:38.436Z",
        "updated_at": "2021-03-03T11:56:38.436Z"
    }
]}

single_operation = {"data": [
    {
        "id": 1,
        "status": "pending_by_validate",
        "currency_in": "DOC",
        "amount_in": 100,
        "currency_out": "USD",
        "amount_out": 100,
        "comment": None,
        "tx_hash": None,
        "url_voucher_image": None,
        "client_id": 7,
        "merchant_id": 3,
        "created_at": "2021-03-02T19:24:38.468Z",
        "updated_at": "2021-03-02T19:24:38.468Z"
    }
]}

create_operation_data = {"data":
    {
        "id": 1,
        "status": "pending_by_validate",
        "currency_in": "DOC",
        "amount_in": 100,
        "currency_out": "USD",
        "amount_out": 100,
        "comment": None,
        "tx_hash": None,
        "url_voucher_image": None,
        "client_id": 7,
        "merchant_id": 3,
        "created_at": "2021-03-02T19:24:38.468Z",
        "updated_at": "2021-03-02T19:24:38.468Z"
    }
}


@pytest.fixture
def expected_formatted_get_user_operations_response():
    return [
        {'operation_id': '1', 'operation_type': 'cash-in', 'status': 'pending_by_validate', 'currency_in': 'DOC',
         'amount_in': 100.0, 'currency_out': 'USD', 'amount_out': 100.0, 'created_at': '2021-03-02T19:24:38.468Z',
         'provider': '1', 'voucher': False, 'wallet_address': '0xtest_wallet'}
    ]


@pytest.fixture
def get_request(client):
    def p(url, reverse_kargs=(), params=(), auth_header=''):
        return client.get(f'{reverse(url, kwargs=reverse_kargs)}?{urlencode(params)}', header=auth_header)

    return p


@pytest.fixture
def post_request(client):
    def p(url, data, reverse_kargs=(), auth_header=''):
        return client.post(f'{reverse(url, kwargs=reverse_kargs)}', data=data, header=auth_header)

    return p


@pytest.fixture
def get_all_operations():
    response = Mock()
    response.json.return_value = operations_data
    response.status_code = 200
    return response


@pytest.fixture
def get_single_operation():
    response = Mock()
    response.json.return_value = single_operation
    response.status_code = 200
    return response


@pytest.fixture
def create_operation_mock():
    response = Mock()
    response.json.return_value = create_operation_data
    response.status_code = 200
    return response


@pytest.fixture
def confirm_operation_mock():
    response = Mock()
    response.status_code = 200
    return response


@pytest.mark.django_db
@pytest.fixture
def create_user():
    data = {
        'app_id': 1,
        'email': 'juan@topo.com',
        'provider': 1,
        'provider_user_id': 7,
        'user_id': 34
    }
    return UserModel.create(**data)


@pytest.mark.django_db
@pytest.mark.parametrize('user_id, expected_response', [
    ['1', True],
    ['33', False],
])
def test_user_has_operations(user_id, expected_response, client, create_operation):
    params = {
        'user_id': user_id
    }
    response = client.get(reverse('operations:user-has-operations', kwargs=params))
    data = response.json()
    assert data['user_has_operations'] == expected_response


@patch('operations.tasks.update_kripton_purchases_status.apply_async')
def test_update_operation_status(task_mock, client):
    response = client.get(reverse('operations:update-operation-status'))
    assert 200 == response.status_code
    task_mock.assert_called()
