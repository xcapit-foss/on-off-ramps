from operations.saved_user_payment_method import SavedUserBank
import pytest


@pytest.mark.django_db
def test_saved_user_bank_new(test_operation_cash_out):
    assert SavedUserBank(test_operation_cash_out)


@pytest.mark.django_db
def test_saved_user_bank_value(test_operation_cash_out):
    result = SavedUserBank(test_operation_cash_out).value()
    assert result.get('user_id') == 9999
    assert result.get('payment_method_id') == 23


@pytest.mark.django_db
def test_saved_user_bank_operation(test_operation_cash_out):
    assert SavedUserBank(test_operation_cash_out).operation().data() == test_operation_cash_out.data()
