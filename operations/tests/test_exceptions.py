from operations.exceptions import RequestError

def test_request_error():
    assert RequestError({})


def test_request_error_code():
    assert RequestError({'error': {'error_code': 22, 'message': 'Kripton operation error'}}).code() == 22


def test_request_error_message():
    assert RequestError({'error': {'error_code': 22, 'message': 'Kripton operation error'}}).message() == 'Kripton operation error'


def test_request_error_code_default():
    assert RequestError({}).code() == 500


def test_request_error_message_default():
    assert RequestError({}).message() == 'Server error'
