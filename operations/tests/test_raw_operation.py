import pytest
from operations.raw_operation import RawOperation
from providers.kripton.clients import FakeKriptonClient
from providers.kripton.kripton_user import KriptonUser


def test_raw_operation_new(raw_operation_data):
    test_kripton_user = KriptonUser(a_client=FakeKriptonClient())
    assert RawOperation(kripton_user=test_kripton_user, raw_operation_data=raw_operation_data)


@pytest.mark.django_db
def test_raw_operation_user(raw_operation_data, test_kripton_user):
    assert RawOperation(kripton_user=test_kripton_user,
                        raw_operation_data=raw_operation_data).user().id == test_kripton_user.id


@pytest.mark.django_db
def test_raw_operation_data(raw_operation_data, test_kripton_user):
    assert RawOperation(kripton_user=test_kripton_user,
                        raw_operation_data=raw_operation_data).data() == raw_operation_data
