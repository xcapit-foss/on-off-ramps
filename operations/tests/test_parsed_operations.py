import pytest

from operations.parsed_operations import ParsedOperations


def test_parsed_operations_new():
    assert ParsedOperations(operations=[])


@pytest.mark.django_db
def test_parsed_operations_value(
        raw_single_operation_complete,
        expected_serialized_cash_out_operations_response,
        create_cash_out_operation):
    serialized_operations = ParsedOperations(operations=raw_single_operation_complete).value()
    assert serialized_operations == expected_serialized_cash_out_operations_response


@pytest.mark.django_db
def test_parsed_operations_value_cash_in(
        raw_single_operation_complete,
        expected_serialized_operations_response,
        create_operation):
    serialized_operations = ParsedOperations(operations=raw_single_operation_complete).value()
    assert serialized_operations == expected_serialized_operations_response
