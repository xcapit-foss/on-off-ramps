import pytest

from operations.created_operation import CreatedOperation
from operations.models import OperationsModel
from providers.kripton.clients import FakeKriptonClient
from requests.exceptions import RetryError
from rest_framework import status


@pytest.mark.django_db
def test_created_kripton_operation_new(test_create_operation_request):
    assert CreatedOperation(test_create_operation_request, FakeKriptonClient(), 'auth_token')


@pytest.mark.django_db
def test_created_kripton_operation_value(test_create_operation_request, create_operation_response, request_response,
                                         get_single_operation_response):
    create_operation_request = test_create_operation_request(
        save_wallet_response=request_response({"data": {"id": 1}}),
        create_operation_response=request_response(create_operation_response))
    CreatedOperation(create_operation_request,
                     FakeKriptonClient(
                         get_operations_response=request_response({"data": get_single_operation_response})),
                     'auth_token').value()
    operation = OperationsModel.objects.get(operation_id=450, network='MATIC')
    assert operation
    assert float(operation.fee) == 0.600000000000000
    assert operation.external_code == 'hMGqdzt71I'


@pytest.mark.django_db
def test_created_kripton_operation_retry_error(test_create_operation_request, create_operation_response,
                                               request_response):
    with pytest.raises(RetryError):
        create_operation_request = test_create_operation_request(
            save_wallet_response=request_response({"data": {"id": 1}}),
            create_operation_response=request_response(create_operation_response))
        CreatedOperation(create_operation_request,
                         FakeKriptonClient(
                             get_operations_response=request_response({}, status.HTTP_401_UNAUTHORIZED)),
                         'auth_token').value()
