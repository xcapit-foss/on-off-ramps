import pytest
from operations.services import OperationsService


def test_operation_service():
    operation_service = OperationsService()
    assert operation_service


@pytest.mark.django_db
@pytest.mark.parametrize('operation_id, expected_result', [
    ['1', True],
    ['2', False],
])
def test_operation_exists(create_operation, operation_id, expected_result):
    operation_service = OperationsService()
    result = operation_service.operation_exists(operation_id, '1')
    assert result == expected_result


@pytest.mark.django_db
@pytest.mark.parametrize('operation_id, expected_result', [
    ['1', 'cash-in'],
    ['2', None],
])
def test_get_operation_type(create_operation, operation_id, expected_result):
    operation_service = OperationsService()
    result = operation_service.get_operation_type(operation_id, '1')
    assert result == expected_result
