from unittest.mock import patch
from operations.tasks import update_kripton_purchases_status
import pytest


@pytest.mark.django_db
@patch('operations.operation.Operation.update')
def test_update_kripton_purchases_status_task(operation_update_mock, create_operation):
    operation_update_mock.return_value = None
    update_kripton_purchases_status.apply()
    operation_update_mock.assert_called()
