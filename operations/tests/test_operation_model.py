import pytest
from operations.operation import Operation
from operations.models import OperationsModel
from unittest.mock import patch
from core.exceptions import CustomError

get_operations_response = [
    {
        "operation_id": "57",
        "operation_type": "cash-in",
        "status": "wait",
        "currency_in": "ARS",
        "amount_in": 200.0,
        "currency_out": "DAI",
        "amount_out": 207.99,
        "created_at": "2021-05-11T20:19:40.334Z",
        "provider": "1",
        "voucher": False
    }
]


@pytest.mark.django_db
def test_operation_model_create(create_operation_data):
    operation_id = OperationsModel.objects.create(**create_operation_data).id
    assert OperationsModel.objects.get(id=operation_id).user_id == 1


@pytest.mark.django_db
@patch('providers.kripton.service.KriptonService.get_operations')
def test_operation_update(mock_get, create_operation):
    mock_get.return_value = get_operations_response

    op = OperationsModel.objects.get(id=1)
    Operation.create(op).update()
    assert OperationsModel.objects.get(id=1).status == 'wait'


@pytest.mark.django_db
@patch('providers.kripton.service.KriptonService.get_operations')
def test_operation_update_should_not_update_anything_when_error_is_raised(mock_get, create_operation):
    mock_get.side_effect = CustomError('some_error')

    op = OperationsModel.objects.get(id=1)
    Operation.create(op).update()
    assert OperationsModel.objects.get(id=1).status == 'pending_by_validate'
