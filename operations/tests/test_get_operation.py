import pytest
from unittest.mock import patch
from requests.exceptions import RetryError
from django.urls import reverse


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_get_operation_success(mock_get_operations, client, create_operation, create_kripton_user, request_response,
                               raw_single_operation_complete, expected_serialized_operations_response):
    create_kripton_user()
    mock_get_operations.return_value = request_response({'data': raw_single_operation_complete})
    params = {'provider_id': '1', 'user_id': 1, 'operation_id': 1}

    response = client.post(reverse('operations:get-operation', kwargs=params), data={'email': 'test@test.com', 'auth_token': 'test'})
    assert response.status_code == 200
    assert response.json() == expected_serialized_operations_response


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_get_operation_error(mock_get_operations, client, create_operation, create_kripton_user, request_response,
                             expected_serialized_operations_response):
    create_kripton_user()
    mock_get_operations.return_value = request_response({}, 400)
    params = {'provider_id': '1', 'user_id': 1, 'operation_id': 1}

    response = client.post(reverse('operations:get-operation', kwargs=params), data={'email': 'test@test.com', 'auth_token': 'test'})
    assert response.status_code == 400
    assert response.json() == {'error': 'Error al obtener la operacion desde proveedor',
                               'error_code': 'on_off_ramps.operations.single_operation.errorProvider'}


@pytest.mark.django_db
def test_get_operation_error_operation_doesnt_exist(client, create_kripton_user, request_response):
    create_kripton_user()
    params = {'provider_id': '1', 'user_id': 1, 'operation_id': 1}

    response = client.post(reverse('operations:get-operation', kwargs=params), data={'email': 'test@test.com', 'auth_token': 'test'})
    assert response.status_code == 404
    assert response.json() == {'error': 'operation does not exist',
                               'error_code': 'on_off_ramps.operations.single_operation.doesNotExist'}

@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_get_operation_unauthorized(mock_get_operations, client, create_kripton_user, request_response, create_operation):
    create_kripton_user()
    params = {'provider_id': '1', 'user_id': 1, 'operation_id': 1}
    mock_get_operations.side_effect = RetryError
    response = client.post(reverse('operations:get-operation', kwargs=params),
                           data={'email': 'test@test.com', 'auth_token': 'unauthorized_token'})
    assert response.status_code == 401
    assert response.json() == {"error": 'Error al obtener la operacione desde proveedor, token de acceso vencido',
                     'error_code': 'on_off_ramps.operations.single_operation.errorProvider'}
