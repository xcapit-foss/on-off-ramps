import pytest
from io import StringIO
from django.core.management import call_command
from unittest.mock import patch
from operations.models import OperationsModel


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_command_set_operations_fee(mock_get_operations, create_operation_with_fee, request_response,
                                    get_single_operation_response):
    mock_get_operations.return_value = request_response({"data": get_single_operation_response})
    create_operation_with_fee(0.0, 1)
    create_operation_with_fee(0.678, 2)
    create_operation_with_fee(0.0, 3)
    out = StringIO()
    call_command('set_operations_fee', stdout=out)
    assert 'Success' in out.getvalue()
    assert not OperationsModel.objects.filter(fee=0.0).exists()
    assert len(OperationsModel.objects.filter(fee__gt=0.0)) == 3
