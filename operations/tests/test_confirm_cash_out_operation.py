import pytest
from unittest.mock import patch
from django.urls import reverse
from requests.exceptions import RetryError

@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.confirm_cash_out_operation')
def test_confirm_cash_out_operation_success(
        mock_confirm_cash_out_operation, request_response, client,
        create_kripton_user, confirm_operation_data, create_operation):
    mock_confirm_cash_out_operation.return_value = request_response({})
    create_kripton_user()
    params = {
        'provider_id': 1,
        'operation_id': 1,
        'user_id': 47
    }
    response = client.post(reverse('operations:confirm-cash-out', kwargs=params), data=confirm_operation_data)
    assert response.json() == {}
    assert response.status_code == 200

@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.confirm_cash_out_operation')
def test_confirm_cash_out_operation_retryerror(
        mock_confirm_cash_out_operation, client,
        create_kripton_user,confirm_operation_data, create_operation):
    mock_confirm_cash_out_operation.side_effect = RetryError
    create_kripton_user()
    params = {
        'provider_id': 1,
        'operation_id': 1,
        'user_id': 47
    }
    response = client.post(reverse('operations:confirm-cash-out', kwargs=params), data=confirm_operation_data)
    assert response.json() == {'error': 'Error al confirmar una operacion, token de acceso invalido',
                        "error_code": "on_off_ramps.operations.single_operation.tokenNotValid"}
    assert response.status_code == 401

@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.confirm_cash_out_operation')
def test_confirm_cash_out_operation_unauthorized(
        mock_confirm_cash_out_operation, request_response, client,
        create_kripton_user, confirm_operation_data, create_operation):
    mock_confirm_cash_out_operation.return_value = request_response({}, status_code=401)
    create_kripton_user()
    params = {
        'provider_id': 1,
        'operation_id': 1,
        'user_id': 47
    }
    response = client.post(reverse('operations:confirm-cash-out', kwargs=params), data=confirm_operation_data)
    assert response.json() == {'error': 'Error al confirmar una operacion, token de acceso invalido',
                        "error_code": "on_off_ramps.operations.single_operation.tokenNotValid"}
    assert response.status_code == 401

@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.confirm_cash_out_operation')
def test_confirm_cash_out_operation_not_exist(
    mock_confirm_cash_out_operation, request_response, client,
    create_kripton_user, confirm_operation_data):
    mock_confirm_cash_out_operation.return_value = request_response({}, status_code=401)
    create_kripton_user()
    params = {
        'provider_id': 1,
        'operation_id': 1,
        'user_id': 47
    }
    response = client.post(reverse('operations:confirm-cash-out', kwargs=params), data=confirm_operation_data)
    assert response.json() == {'error': 'operation does not exist',
                                "error_code": "on_off_ramps.operations.single_operation.doesNotExist"}
    assert response.status_code == 404