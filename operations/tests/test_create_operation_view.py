from unittest.mock import patch
from django.urls import reverse
from rest_framework import status
import pytest
from operations.models import OperationsRequestErrorModel
from providers.kripton.clients import FakeKriptonClient


@pytest.mark.django_db
@patch('operations.saved_user_payment_method.SavedUserWallet.value')
@patch('operations.create_operation_request.CreateOperationRequest.response')
@patch('operations.created_operation.CreatedOperation.value')
def test_create_operation_ok(
        mock_created_kripton_operation,
        mock_create_operation_request,
        mock_saved_user_wallet,
        client,
        request_response,
        create_kripton_user,
        create_operation_input_data,
        create_operation,
        expected_create_operation_view_response
):
    mock_created_kripton_operation.return_value = create_operation
    mock_create_operation_request.return_value = request_response()
    mock_saved_user_wallet.return_value = request_response()
    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}
    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)

    assert response.status_code == status.HTTP_200_OK
    json_response = response.json()
    assert json_response['created_at']
    del json_response['created_at']
    assert json_response == expected_create_operation_view_response


@pytest.mark.django_db
@patch('operations.create_operation_request.CreateOperationRequest.response')
@patch('operations.created_operation.CreatedOperation.value')
def test_create_operation_cash_out_ok(mock_created_kripton_operation, mock_create_operation_request,
                                      client, request_response, create_kripton_user,
                                      create_cash_out_operation_input_data,
                                      create_cash_out_operation, expected_create_cash_out_operation_view_response):
    mock_created_kripton_operation.return_value = create_cash_out_operation
    mock_create_operation_request.return_value = request_response()
    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}
    response = client.post(reverse('operations:create-operation', kwargs=params),
                           data=create_cash_out_operation_input_data)

    assert response.status_code == status.HTTP_200_OK
    json_response = response.json()
    assert json_response['created_at']
    del json_response['created_at']
    assert json_response == expected_create_cash_out_operation_view_response


@pytest.mark.django_db
@patch('operations.views.DefaultKriptonClient')
def test_create_operation_error_log(
        mock_default_kripton_client,
        client,
        request_response,
        create_kripton_user,
        create_operation_input_data,
        error_response
):
    mock_default_kripton_client.return_value = FakeKriptonClient(
        save_wallet_response=request_response({'error': error_response()}, status.HTTP_400_BAD_REQUEST))
    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)
    error_db = OperationsRequestErrorModel.objects.first()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'error': f'Error al intentar crear la operación en el proveedor: {error_response().get("message")}',
        'error_code': 'on_off_ramps.operations.create.errorProvider',
        'error_id': error_db.id}
    assert error_db.error_code == error_response().get("error_code")
    assert error_db.message == error_response().get("message")


@pytest.mark.django_db
@patch('operations.views.DefaultKriptonClient')
def test_create_operation_server_error_on_save_wallet_log(
        mock_default_kripton_client,
        client,
        create_kripton_user,
        create_operation_input_data,
        request_response,
        error_response
):
    server_error_response = error_response(500, 'Server error')
    mock_default_kripton_client.return_value = FakeKriptonClient(
        save_wallet_response=request_response({}, status.HTTP_500_INTERNAL_SERVER_ERROR))

    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)

    error_db = OperationsRequestErrorModel.objects.first()

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'error': f'Error al intentar crear la operación en el proveedor: {server_error_response.get("message")}',
        'error_code': 'on_off_ramps.operations.create.errorProvider',
        'error_id': error_db.id}
    assert error_db.error_code == server_error_response.get("error_code")
    assert error_db.message == server_error_response.get("message")


@pytest.mark.django_db
@patch('operations.views.DefaultKriptonClient')
def test_create_operation_error_operation_log(
        mock_default_kripton_client,
        client,
        create_kripton_user,
        create_operation_input_data,
        request_response,
        error_response
):
    mock_default_kripton_client.return_value = FakeKriptonClient(
        save_wallet_response=request_response({'data': {'user_id': 34, 'id': 1}}, status.HTTP_200_OK),
        create_operation_response=request_response({'error': error_response()}, status.HTTP_400_BAD_REQUEST)
    )

    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)
    error_db = OperationsRequestErrorModel.objects.first()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'error': f'Error al intentar crear la operación en el proveedor: {error_response().get("message")}',
        'error_code': 'on_off_ramps.operations.create.errorProvider',
        'error_id': error_db.id}
    assert error_db.error_code == error_response().get("error_code")
    assert error_db.message == error_response().get("message")


@pytest.mark.django_db
@patch('operations.views.DefaultKriptonClient')
def test_create_operation_server_error_log(
        mock_default_kripton_client,
        client,
        create_kripton_user,
        create_operation_input_data,
        request_response,
        error_response
):
    server_error_response = error_response(500, 'Server error')

    mock_default_kripton_client.return_value = FakeKriptonClient(
        save_wallet_response=request_response({'data': {'user_id': 34, 'id': 1}}, status.HTTP_200_OK),
        create_operation_response=request_response(
            {'error': server_error_response}, status.HTTP_500_INTERNAL_SERVER_ERROR)
    )

    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)
    error_db = OperationsRequestErrorModel.objects.first()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'error': f'Error al intentar crear la operación en el proveedor: {server_error_response.get("message")}',
        'error_code': 'on_off_ramps.operations.create.errorProvider',
        'error_id': error_db.id}
    assert error_db.error_code == server_error_response.get("error_code")
    assert error_db.message == server_error_response.get("message")


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.save_wallet')
@patch('providers.kripton.clients.DefaultKriptonClient.create_operation')
def test_create_operation_unauthorized_on_create_operation(mock_create_operation, mock_save_wallet, client,
                                                           request_response,
                                                           create_kripton_user,
                                                           create_operation_input_data, create_operation,
                                                           expected_create_operation_view_response):
    mock_save_wallet.return_value = request_response({'data': {'id': 1}})
    mock_create_operation.return_value = request_response({}, status.HTTP_401_UNAUTHORIZED)
    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {'error': 'Error al intentar crear la operación en el proveedor, token de acceso vencido',
                               'error_code': 'on_off_ramps.operations.create.errorProvider'}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.save_wallet')
def test_create_operation_unauthorized_on_save_wallet(mock_kripton_client, client, request_response,
                                                      create_kripton_user, create_operation_input_data,
                                                      create_operation, expected_create_operation_view_response):
    mock_kripton_client.return_value = request_response({}, status.HTTP_401_UNAUTHORIZED)
    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {
        'error': 'Error al intentar crear la operación en el proveedor, token de acceso vencido',
        'error_code': 'on_off_ramps.operations.create.errorProvider'}


@pytest.mark.django_db
@patch('providers.kripton.clients.DefaultKriptonClient.save_wallet')
@patch('providers.kripton.clients.DefaultKriptonClient.create_operation')
@patch('providers.kripton.clients.DefaultKriptonClient.get_operations')
def test_create_operation_unauthorized_on_created_operation(mock_get_operations, mock_create_operation,
                                                            mock_save_wallet,
                                                            client, request_response, create_kripton_user,
                                                            create_operation_input_data, create_operation,
                                                            expected_create_operation_view_response,
                                                            create_operation_response):
    mock_save_wallet.return_value = request_response({'data': {'id': 1}})
    mock_create_operation.return_value = request_response(create_operation_response)
    mock_get_operations.return_value = request_response({}, status.HTTP_401_UNAUTHORIZED)
    create_kripton_user()

    params = {'provider_id': '1', 'user_id': '34'}

    response = client.post(reverse('operations:create-operation', kwargs=params), data=create_operation_input_data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {'error': 'Error al intentar crear la operación en el proveedor, token de acceso vencido',
                               'error_code': 'on_off_ramps.operations.create.errorProvider'}
