from providers.serializers import KriptonOperationReturnSerializer


class ParsedOperations:
    def __init__(self, operations):
        self._operations = operations
        self._serializer_class = KriptonOperationReturnSerializer

    def value(self):
        serialized_output = []
        _ops = [self._operations] if not isinstance(self._operations, list) else self._operations
        for op in _ops:
            op['operation_id'] = op['id']
            serializer = self._serializer_class(data=op)
            if serializer.is_valid(raise_exception=False):
                serialized_output.append(serializer.data)
        return serialized_output
