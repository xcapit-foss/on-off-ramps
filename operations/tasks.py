from on_off_ramps.celery import app
from operations.models import OperationsModel
from django.db.models import Q
from operations.operation import Operation


@app.task
def update_kripton_purchases_status():
    unfinished_operations = OperationsModel.objects.filter(Q(provider_id="1"),
                                                           ~Q(status__in=['complete', 'cancel']))
    for op in unfinished_operations:
        Operation.create(op).update()
