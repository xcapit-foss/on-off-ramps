import requests
from rest_framework.views import APIView
from requests.exceptions import RetryError
from providers.kripton.clients import DefaultKriptonClient, KriptonClient
from providers.kripton.kripton_user import KriptonUser
from providers.kripton.kripton_users import KriptonUsers
from providers.serializers import KriptonOperationReturnSerializer
from providers.service import ProvidersService
from rest_framework.response import Response
from rest_framework import status
from kripton.users.services import UsersService
from .create_operation_request import CreateOperationRequest
from .created_operation import CreatedOperation
from .exceptions import RequestError
from .models import OperationsModel, OperationsRequestErrorModel
from .parsed_operations import ParsedOperations
from .raw_operation import RawOperation
from .services import OperationsService
from operations.tasks import update_kripton_purchases_status
from operations.saved_user_payment_method import SavedUserWallet, SavedUserBank
from .uploaded_voucher import UploadedVoucher


class ListOperationsAPIView(APIView):
    kripton_client = DefaultKriptonClient()

    def post(self, request, user_id, *args, **kwargs):
        email = request.data.get('email')
        auth_token = request.data.get('auth_token')
        user = KriptonUsers().get_or_create_by(email, user_id)
        try:
            operations_response = self.kripton_client.get_operations(user.id, auth_token=auth_token)
            if operations_response.status_code != 200:
                response = Response({"error": 'Error al obtener las operaciones desde proveedor',
                                     'error_code': 'on_off_ramps.operations.list.errorProvider'},
                                    status.HTTP_400_BAD_REQUEST)
            else:
                serialized_operations = ParsedOperations(operations_response.json().get("data")).value()
                response = Response(serialized_operations, status=status.HTTP_200_OK)
        except RetryError:
            response = Response({"error": 'Error al obtener las operaciones desde proveedor, token de acceso vencido',
                                 'error_code': 'on_off_ramps.operations.list.errorProvider'},
                                status.HTTP_401_UNAUTHORIZED)
        return response


class GetOperationAPIView(APIView):
    kripton_client = DefaultKriptonClient()
    operations_service = OperationsService()

    def post(self, request, user_id, provider_id, operation_id, *args, **kwargs):
        if not self.operations_service.operation_exists(operation_id=operation_id, provider_id=provider_id):
            response = Response({'error': 'operation does not exist',
                                 'error_code': 'on_off_ramps.operations.single_operation.doesNotExist'},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            try:
                email = request.data.get('email')
                auth_token = request.data.get('auth_token')
                user = KriptonUsers().get_or_create_by(email, user_id)

                operation_response = self.kripton_client.get_operations(user.id, auth_token, operation_id)
                if operation_response.status_code != 200:
                    response = Response({'error': 'Error al obtener la operacion desde proveedor',
                                         'error_code': 'on_off_ramps.operations.single_operation.errorProvider'},
                                        status.HTTP_400_BAD_REQUEST)
                else:
                    serialized_operation = ParsedOperations(operation_response.json().get("data")).value()
                    response = Response(serialized_operation, status=status.HTTP_200_OK)
            except RetryError:
                response = Response(
                    {"error": 'Error al obtener la operacione desde proveedor, token de acceso vencido',
                     'error_code': 'on_off_ramps.operations.single_operation.errorProvider'},
                    status.HTTP_401_UNAUTHORIZED)
        return response


class CreateOperationAPIView(APIView):
    users_service = UsersService()
    provider_service = ProvidersService()
    serializer_class = KriptonOperationReturnSerializer

    def post(self, request, provider_id, user_id, *args, **kwargs):
        kripton_client = DefaultKriptonClient()
        data = request.data.copy()
        kripton_user = KriptonUsers().get_or_create_by(data.get('email'), user_id)
        auth_token = data.get('auth_token')
        data.pop('email')
        data.pop('auth_token')

        try:
            created_operation = CreatedOperation(
                CreateOperationRequest(
                    self._saved_user_payment_method(kripton_user, data, kripton_client, auth_token),
                    kripton_client,
                    auth_token
                ),
                kripton_client,
                auth_token
            ).value()
            response = Response(
                self.parse_operation_response(created_operation),
                status=status.HTTP_200_OK
            )
        except RequestError as error:
            response = Response(
                {'error': f'Error al intentar crear la operación en el proveedor: {error.message()}',
                 'error_code': 'on_off_ramps.operations.create.errorProvider',
                 "error_id": self._operation_error(data, error, provider_id, user_id).id},
                status.HTTP_400_BAD_REQUEST
            )

        except RetryError:
            response = Response(
                {'error': 'Error al intentar crear la operación en el proveedor, token de acceso vencido',
                 'error_code': 'on_off_ramps.operations.create.errorProvider'},
                status.HTTP_401_UNAUTHORIZED
            )

        return response

    def _saved_user_payment_method(
            self,
            _kripton_user: KriptonUser,
            _data: any,
            _kripton_client: KriptonClient,
            auth_token: str
    ):
        if _data.get('type') == 'cash-in':
            result = SavedUserWallet(
                RawOperation(_kripton_user, _data),
                _kripton_client,
                auth_token
            )
        else:
            result = SavedUserBank(RawOperation(_kripton_user, _data))

        return result

    def _operation_error(self, data: any, error: RequestError, provider_id: int, user_id: int):
        return OperationsRequestErrorModel.objects.create(
            error_code=error.code(),
            message=error.message(),
            wallet=data.get('wallet'),
            operation_type=data.get('type'),
            currency_in=data.get('currency_in'),
            amount_in=data.get('amount_in'),
            currency_out=data.get('currency_out'),
            amount_out=data.get('amount_out'),
            provider_id=provider_id,
            user_id=user_id,
            network=data.get('network'),
        )

    def parse_operation_response(self, created_operation):
        operation_dict = {key: value for key, value in created_operation.get('operation_model').__dict__.items() if
                          key in OperationsModel.fields() and key != 'wallet'}
        operation_dict['id'] = operation_dict.pop('operation_id')
        operation_dict['kripton_wallet'] = created_operation.get('kripton_wallet')
        return operation_dict


class ConfirmCashOutView(APIView):
    operations_service = OperationsService()
    kripton_client = DefaultKriptonClient()

    def post(self, request, provider_id, user_id, operation_id, *args, **kwargs):
        data = request.data.copy()
        auth_token = data.get('auth_token')
        data.pop('auth_token')
        kripton_user = KriptonUsers().get_or_create_by(data.get('email'), user_id)

        if not self.operations_service.operation_exists(operation_id, provider_id):
            return Response({'error': 'operation does not exist',
                             "error_code": "on_off_ramps.operations.single_operation.doesNotExist"},
                            status=status.HTTP_404_NOT_FOUND)
        try:
            data.pop('email')
            confirm_operation_response = self.kripton_client.confirm_cash_out_operation(kripton_user.id, auth_token,
                                                                                        operation_id, data)
            if confirm_operation_response.status_code == 401:
                raise RetryError
            else:
                response = Response({}, confirm_operation_response.status_code)
        except RetryError:
            response = Response({'error': 'Error al confirmar una operacion, token de acceso invalido',
                                 "error_code": "on_off_ramps.operations.single_operation.tokenNotValid"},
                                status=status.HTTP_401_UNAUTHORIZED)
        return response


class ConfirmOperationAPIView(APIView):
    operations_service = OperationsService()

    def post(self, request, provider_id, user_id, operation_id, *args, **kwargs):

        data = request.data.copy()
        auth_token = data.get('auth_token')
        data.pop('auth_token')
        kripton_user = KriptonUsers().get_or_create_by(data.get('email'), user_id)

        if not self.operations_service.operation_exists(operation_id, provider_id):
            return Response({'error': 'operation does not exist',
                             "error_code": "on_off_ramps.operations.single_operation.doesNotExist"},
                            status=status.HTTP_404_NOT_FOUND)

        try:
            uploaded_voucher_response = UploadedVoucher(kripton_user, data, operation_id, auth_token).value()

            if uploaded_voucher_response.status_code != 200:
                response = Response(self._setErrorMessage(uploaded_voucher_response), status.HTTP_400_BAD_REQUEST)
            else:
                response = Response({}, status=status.HTTP_200_OK)
        except RetryError:
            response = Response({'error': 'Error al confirmar una operacion, token de acceso invalido',
                                 "error_code": "on_off_ramps.operations.single_operation.tokenNotValid"},
                                status=status.HTTP_401_UNAUTHORIZED)
        return response

    @staticmethod
    def _setErrorMessage(error_response: requests.Response):
        if error_response.json()['message'] == 'Operation was not validate by the user':
            content = {"error": 'Debe confirmar la operación creada. Por favor, verifique su correo electrónico.',
                       "error_code": 'on_off_ramps.operations.confirm.errorConfirmation'}
        else:
            content = {"error": 'Error al intentar guardar el comprobante en el proveedor',
                       "error_code": 'on_off_ramps.operations.confirm.errorProvider'}
        return content


class UserHasOperationsView(APIView):
    operations_service = OperationsService()
    users_service = UsersService()

    def get(self, request, user_id, *args, **kwargs):
        all_user_ids = self.users_service.get_all_user_ids_from_active_providers(user_id=user_id)
        result = self.operations_service.user_has_any_operation_created(all_user_ids)
        return Response({'user_has_operations': result}, status.HTTP_200_OK)


class UpdateOperationStatusView(APIView):

    def get(self, request, *args, **kwargs):
        update_kripton_purchases_status.apply_async()
        return Response(status=status.HTTP_200_OK)
