class RequestError(Exception):
    def __init__(self, error=None):
        self._error = error if error is not None else {}

    def code(self):
        return self._error.get("error", {'error_code': 500}).get("error_code")

    def message(self):
        return self._error.get("error", {'message': 'Server error'}).get("message")
