from django.core.management import BaseCommand

from operations.models import OperationsModel


class Command(BaseCommand):
    help = 'Set network for existing Krypton operations'
    _currency_to_network = {'MATIC': 'MATIC',
                            'DAI': 'MATIC',
                            'USDC': 'MATIC',
                            'ETH': 'ERC20',
                            'USDT': 'ERC20',
                            'RBTC': 'RSK',
                            'RIF': 'RSK'}

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('🚀 Launching 🚀'))
        for operation in OperationsModel.objects.filter(provider_id=1):
            operation.network = self._currency_to_network[operation.currency_out.upper()]
        self.stdout.write(self.style.SUCCESS('💯 Success 💯'))
