from django.core.management import BaseCommand
from providers.kripton.clients import DefaultKriptonClient
from operations.models import OperationsModel
import json


class Command(BaseCommand):
    help = 'Change the fee of the operations that were already created for the one that was actually charged'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('🚀 Launching 🚀'))

        for operation in OperationsModel.objects.filter(fee=0.0):
            operation_response = DefaultKriptonClient().get_operations(operation.user_id,
                                                                       'auth_token',
                                                                       operation_id=operation.operation_id)
            fee = json.loads(operation_response.json()['data']['support_data']).get('costs')
            operation.fee = fee
            operation.save()

        self.stdout.write(self.style.SUCCESS('💯 Success 💯'))
