from django.db import models


class OperationsModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    wallet = models.CharField(max_length=100)
    operation_id = models.IntegerField()
    operation_type = models.CharField(max_length=100)
    currency_in = models.CharField(max_length=100)
    amount_in = models.DecimalField(max_digits=30, decimal_places=15)
    price_in = models.DecimalField(max_digits=30, decimal_places=15, null=True)
    currency_out = models.CharField(max_length=100)
    amount_out = models.DecimalField(max_digits=30, decimal_places=15)
    price_out = models.DecimalField(max_digits=30, decimal_places=15, null=True)
    provider_id = models.IntegerField()
    user_id = models.IntegerField()
    status = models.CharField(max_length=200)
    voucher = models.BooleanField(default=False)
    network = models.CharField(max_length=150, default='MATIC')
    fee = models.DecimalField(max_digits=30, decimal_places=15, default=0.0)
    external_code = models.CharField(max_length=150, null=True)

    @classmethod
    def fields(cls):
        return [field.get_attname() for field in OperationsModel._meta.get_fields()]


class OperationsRequestErrorModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    wallet = models.CharField(max_length=100)
    error_code = models.IntegerField()
    message = models.CharField(max_length=255)
    operation_type = models.CharField(max_length=100)
    currency_in = models.CharField(max_length=100)
    amount_in = models.DecimalField(max_digits=30, decimal_places=15)
    currency_out = models.CharField(max_length=100)
    amount_out = models.DecimalField(max_digits=30, decimal_places=15)
    provider_id = models.IntegerField()
    user_id = models.IntegerField()
    network = models.CharField(max_length=150)
