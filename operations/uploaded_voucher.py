from providers.kripton.clients import KriptonClient, DefaultKriptonClient
from providers.kripton.kripton_user import KriptonUser
from requests.exceptions import RetryError


class UploadedVoucher:
    def __init__(self, kripton_user: KriptonUser, voucher_data, operation_id, auth_token,
                 kripton_client: KriptonClient = DefaultKriptonClient()):
        self._user = kripton_user
        self._data = voucher_data
        self._client = kripton_client
        self._auth_token = auth_token
        self._operation_id = operation_id

    def value(self):
        self._data.pop('email')
        response = self._client.upload_voucher(self._user.id, self._operation_id, self._data, self._auth_token)
        if response.status_code == 401:
            raise RetryError('')
        return response
