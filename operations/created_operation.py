import json

import requests
from operations.create_operation_request import CreateOperationRequest
from operations.models import OperationsModel
from providers.kripton.clients import KriptonClient
from requests.exceptions import RetryError


class CreatedOperation:
    def __init__(self, create_operation_request: CreateOperationRequest, kripton_client: KriptonClient,
                 auth_token: str):
        self._request = create_operation_request
        self._provider_id = '1'
        self._kripton_client = kripton_client
        self._auth_token = auth_token

    def value(self):
        json_request = self._request.response().json()
        invoice = json_request.get('invoice', None)
        return {
            'operation_model': OperationsModel.objects.create(
                external_code=invoice['external_code'] if invoice else None,
                **self._parsed_operation(json_request.get('data'))
            ),
            'kripton_wallet': invoice['address'] if invoice else None
        }

    def _parsed_operation(self, create_operation_response: requests.Response):
        data = self._request.operation().data()
        operation_response = self._get_operation(create_operation_response['id'])
        fee = json.loads(operation_response.json()['data']['support_data']).get('costs')
        return {
            'wallet': data['wallet'],
            'operation_id': create_operation_response['id'],
            'operation_type': data['type'],
            'currency_in': data['currency_in'],
            'amount_in': data['amount_in'],
            'price_in': data['price_in'],
            'currency_out': data['currency_out'],
            'amount_out': data['amount_out'],
            'price_out': data['price_out'],
            'provider_id': self._provider_id,
            'user_id': self._request.operation().user().id,
            'status': create_operation_response['status'],
            'voucher': False,
            'network': data['network'].upper(),
            'fee': fee,
        }

    def _get_operation(self, operation_id):
        response = self._kripton_client.get_operations(self._request.operation().user().id, self._auth_token,
                                                       operation_id)
        if response.status_code == 401:
            raise RetryError('')

        return response
