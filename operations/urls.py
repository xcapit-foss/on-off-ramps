from django.urls import path
from operations.views import ListOperationsAPIView, GetOperationAPIView, CreateOperationAPIView, \
                             ConfirmOperationAPIView, UserHasOperationsView, UpdateOperationStatusView, \
                             ConfirmCashOutView

app_name = "operations"

urlpatterns = [
    path('provider/get_user_operations/<user_id>', ListOperationsAPIView.as_view(), name='get-user-operations'),
    path('provider/<provider_id>/get_user_operations/<user_id>/<operation_id>', GetOperationAPIView.as_view(), name='get-operation'),
    path('provider/<provider_id>/create_operation/<user_id>', CreateOperationAPIView.as_view(), name='create-operation'),
    path('provider/<provider_id>/confirm_operation/cash-in/<user_id>/<operation_id>', ConfirmOperationAPIView.as_view(), name='confirm-operation'),
    path('provider/<provider_id>/confirm_operation/cash-out/<user_id>/<operation_id>', ConfirmCashOutView.as_view(), name='confirm-cash-out'),
    path('user_has_operations/<user_id>', UserHasOperationsView.as_view(), name='user-has-operations'),
    path('update_operation_status', UpdateOperationStatusView.as_view(), name='update-operation-status')

]
