from operations.models import OperationsModel
from providers.service import ProvidersService
from core.exceptions import CustomError


class Operation:
    def __init__(self, instance: OperationsModel, provider_service: ProvidersService):
        self._instance = instance
        self._provider_service = provider_service

    @classmethod
    def create(
            cls,
            instance: OperationsModel,
    ):
        return cls(
            instance,
            ProvidersService(),
        )

    def update(self):
        last_status = self._last_status
        if last_status and self._instance.status != last_status:
            self._instance.status = last_status
            self._instance.save()

    @property
    def _provider_client(self):
        return self._provider_service.get_provider_client(provider_id=str(self._instance.provider_id))

    @property
    def _last_status(self):
        try:
            response = self._provider_client.get_operations(self._instance.user_id, self._instance.operation_id)
        except CustomError:
            response = None
        finally:
            return response[0]['status'] if response else None
