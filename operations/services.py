from .models import OperationsModel
from django.db.models import Q


class OperationsService:

    def __init__(self):
        self._model = OperationsModel

    def user_has_any_operation_created(self, user_ids_array):
        filter_params = None
        for id in user_ids_array:
            filter_params = filter_params | Q(user_id=id) if filter_params is not None else Q(user_id=id)
        return self._model.objects.filter(filter_params).exists() if filter_params is not None else False

    def operation_exists(self, operation_id, provider_id):
        return self._model.objects.filter(operation_id=operation_id, provider_id=provider_id).exists()

    def get_operation_type(self, operation_id, provider_id):
        return self._model.objects.get(operation_id=operation_id, provider_id=provider_id).operation_type if self.operation_exists(
            operation_id, provider_id) else None
