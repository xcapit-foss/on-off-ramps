from providers.kripton.kripton_user import KriptonUser


class RawOperation:
    def __init__(self, kripton_user: KriptonUser, raw_operation_data):
        self._kripton_user = kripton_user
        self._raw_data = raw_operation_data

    def user(self) -> KriptonUser:
        return self._kripton_user

    def data(self) -> dict:
        return self._raw_data
