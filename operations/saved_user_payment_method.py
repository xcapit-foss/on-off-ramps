import requests

from operations.exceptions import RequestError
from operations.raw_operation import RawOperation
from providers.enums import CurrencyNetworksEnum
from providers.kripton.clients import KriptonClient
from requests.exceptions import RetryError
from abc import ABC, abstractmethod


class SavedUserPaymentMethod(ABC):
    @abstractmethod
    def value(self) -> dict:
        pass

    def operation(self) -> RawOperation:
        return self._operation


class SavedUserBank(SavedUserPaymentMethod):
    def __init__(self, operation: RawOperation):
        self._operation = operation

    def value(self) -> dict:
        return {'user_id': self._operation.user().id,
                'payment_method_id': self._operation.data()['payment_method_id']}


class SavedUserWallet(SavedUserPaymentMethod):
    def __init__(self, operation: RawOperation, kripton_client: KriptonClient, auth_token):
        self._operation = operation
        self._client = kripton_client
        self._auth_token = auth_token

    def value(self) -> dict:
        response = self._client.save_wallet(self._operation.user().id, data=self._save_wallet_body(),
                                            auth_token=self._auth_token)
        if response.status_code == 401:
            raise RetryError('')
        elif 400 <= response.status_code < 500:
            raise RequestError(response.json())
        elif response.status_code >= 500:
            raise RequestError()

        return {'user_id': response.json().get('data').get('user_id'),
                'payment_method_id': response.json().get('data').get('id')}

    def _save_wallet_body(self):
        currency = self._operation.data()['currency_out'] if self._operation.data()['type'] == 'cash-in' else \
            self._operation.data()['currency_in']
        return {
            "address": self._operation.data()['wallet'],
            "currency": currency,
            "crypto_network": CurrencyNetworksEnum[self._operation.data()['network']].value
        }
